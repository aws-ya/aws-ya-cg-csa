terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.0"
        }
    }
}

provider "aws" {
    region = "us-east-1"
}

resource "aws_iam_user" "IAMUser" {
    path = "/"
    name = "phpapp"
    tags = {
        AKIAZGPOQSOLMDRBRRRK = "phpapp"
    }
}

resource "aws_iam_user" "IAMUser2" {
    path = "/"
    name = "tatiana_vodka"
    tags = {
        Name = "t-vodka"
    }
}

resource "aws_iam_user" "IAMUser3" {
    path = "/"
    name = "yakov_kolyada"
    tags = {
        Department = "Engineering"
        Name = "y-kolyada"
    }
}

resource "aws_iam_group" "IAMGroup" {
    path = "/"
    name = "Admin"
}

resource "aws_iam_role" "IAMRole" {
    path = "/"
    name = "DemoRoleForEC2"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"ec2.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole2" {
    path = "/"
    name = "ecsTaskExecutionRole"
    assume_role_policy = "{\"Version\":\"2008-10-17\",\"Statement\":[{\"Sid\":\"\",\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"ecs-tasks.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole3" {
    path = "/service-role/"
    name = "lambda_basic_execution"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"lambda.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole4" {
    path = "/"
    name = "rds-monitoring-role"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Sid\":\"\",\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"monitoring.rds.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole5" {
    path = "/service-role/"
    name = "ya-lambda-function-role-boc3va9m"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"lambda.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole6" {
    path = "/"
    name = "yaCloudWatchAgentPolicy"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"ec2.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole7" {
    path = "/"
    name = "yaImageBuilderRole"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"ec2.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole" {
    aws_service_name = "autoscaling.amazonaws.com"
    description = "Default Service-Linked Role enables access to AWS Services and Resources used or managed by Auto Scaling"
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole2" {
    aws_service_name = "ecs.amazonaws.com"
    description = "Role to enable Amazon ECS to manage your cluster."
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole3" {
    aws_service_name = "imagebuilder.amazonaws.com"
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole4" {
    aws_service_name = "elasticloadbalancing.amazonaws.com"
    description = "Allows ELB to call AWS services on your behalf."
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole5" {
    aws_service_name = "globalaccelerator.amazonaws.com"
    description = "Allows Global Accelerator to call AWS services on customer's behalf"
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole6" {
    aws_service_name = "rds.amazonaws.com"
    description = "Allows Amazon RDS to manage AWS resources on your behalf"
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole7" {
    aws_service_name = "resource-explorer-2.amazonaws.com"
}

resource "aws_iam_policy" "IAMManagedPolicy" {
    name = "AWSLambdaBasicExecutionRole-aa8e1ba1-178c-42f8-99c8-871df9bdb907"
    path = "/service-role/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:eu-central-1:632397271958:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:eu-central-1:632397271958:log-group:/aws/lambda/hello-world-python:*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_policy" "IAMManagedPolicy2" {
    name = "policy_scenario1_t-vodka"
    path = "/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:GetLifecycleConfiguration",
                "s3:GetBucketTagging",
                "s3:GetInventoryConfiguration",
                "s3:GetAccessPointPolicyForObjectLambda",
                "s3:ListBucketVersions",
                "s3:GetBucketLogging",
                "s3:ListBucket",
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketPolicy",
                "s3:GetStorageLensConfigurationTagging",
                "s3:GetEncryptionConfiguration",
                "s3:GetBucketObjectLockConfiguration",
                "s3:GetIntelligentTieringConfiguration",
                "s3:DeleteAccessPointPolicyForObjectLambda",
                "s3:GetBucketRequestPayment",
                "s3:GetAccessPointPolicyStatus",
                "s3:PutBucketAcl",
                "s3:GetMetricsConfiguration",
                "s3:GetBucketOwnershipControls",
                "s3:GetBucketPublicAccessBlock",
                "s3:PutAccessPointPolicyForObjectLambda",
                "s3:GetBucketPolicyStatus",
                "s3:ListBucketMultipartUploads",
                "s3:PutBucketPublicAccessBlock",
                "s3:GetBucketWebsite",
                "s3:GetJobTagging",
                "s3:GetAccessPointPolicyStatusForObjectLambda",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetAccessPointConfigurationForObjectLambda",
                "s3:GetBucketNotification",
                "s3:DeleteBucketPolicy",
                "s3:DescribeMultiRegionAccessPointOperation",
                "s3:GetReplicationConfiguration",
                "s3:GetStorageLensConfiguration",
                "s3:DescribeJob",
                "cloudwatch:DescribeAlarms",
                "s3:GetBucketCORS",
                "s3:GetAnalyticsConfiguration",
                "s3:PutBucketPolicy",
                "s3:DeleteAccessPointPolicy",
                "s3:PutAccessPointPolicy",
                "s3:GetBucketLocation",
                "s3:GetAccessPointPolicy",
                "s3:GetAccessPointForObjectLambda",
                "s3:GetStorageLensDashboard"
            ],
            "Resource": [
                "arn:aws:cloudwatch:*:632397271958:alarm:*",
                "arn:aws:s3:::*",
                "arn:aws:s3-object-lambda:*:632397271958:accesspoint/*",
                "arn:aws:s3:*:632397271958:storage-lens/*",
                "arn:aws:s3:*:632397271958:job/*",
                "arn:aws:s3:us-west-2:632397271958:async-request/mrap/*/*",
                "arn:aws:s3:*:632397271958:accesspoint/*"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:GetMultiRegionAccessPointPolicyStatus",
                "s3:GetObjectRetention",
                "s3:GetMultiRegionAccessPointPolicy",
                "s3:PutMultiRegionAccessPointPolicy",
                "s3:GetObjectVersionTagging",
                "s3:GetMultiRegionAccessPoint",
                "s3:GetObjectAttributes",
                "s3:GetObjectLegalHold",
                "s3:GetObjectVersionAttributes",
                "s3:BypassGovernanceRetention",
                "s3:ListMultipartUploadParts",
                "s3:GetObjectVersionTorrent",
                "s3:GetObjectAcl",
                "s3:GetObject",
                "s3:ObjectOwnerOverrideToBucketOwner",
                "s3:GetObjectTorrent",
                "s3:GetMultiRegionAccessPointRoutes",
                "s3:PutObjectVersionAcl",
                "s3:GetObjectVersionAcl",
                "s3:GetObjectTagging",
                "s3:GetObjectVersionForReplication",
                "s3:PutObjectAcl",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:::*/*",
                "arn:aws:s3::632397271958:accesspoint/*"
            ]
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": [
                "s3:ListAccessPointsForObjectLambda",
                "s3:GetAccessPoint",
                "ec2:DescribeInstances",
                "s3:PutAccountPublicAccessBlock",
                "s3:ListAccessPoints",
                "s3:ListJobs",
                "ec2:DescribeSnapshotTierStatus",
                "s3:ListMultiRegionAccessPoints",
                "ec2:ReportInstanceStatus",
                "s3:ListStorageLensConfigurations",
                "ec2:DescribeVolumeStatus",
                "s3:GetAccountPublicAccessBlock",
                "ec2:GetSerialConsoleAccessStatus",
                "s3:ListAllMyBuckets",
                "cloudwatch:DescribeAlarmsForMetric",
                "s3:PutAccessPointPublicAccessBlock",
                "ec2:DescribeInstanceStatus"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "IAMManagedPolicy3" {
    name = "AWSLambdaBasicExecutionRole-d9424a61-be9d-42d9-8fdc-bb26a6ccaebf"
    path = "/service-role/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:eu-central-1:632397271958:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:eu-central-1:632397271958:log-group:/aws/lambda/ya-lambda-function:*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "IAMInstanceProfile" {
    path = "/"
    name = "${aws_iam_role.IAMRole.name}"
    roles = [
        "${aws_iam_role.IAMRole.name}"
    ]
}

resource "aws_iam_instance_profile" "IAMInstanceProfile2" {
    path = "/"
    name = "${aws_iam_role.IAMRole6.name}"
    roles = [
        "${aws_iam_role.IAMRole6.name}"
    ]
}

resource "aws_iam_instance_profile" "IAMInstanceProfile3" {
    path = "/"
    name = "${aws_iam_role.IAMRole7.name}"
    roles = [
        "${aws_iam_role.IAMRole7.name}"
    ]
}

resource "aws_iam_access_key" "IAMAccessKey" {
    status = "Active"
    user = "phpapp"
}

resource "aws_iam_access_key" "IAMAccessKey2" {
    status = "Active"
    user = "tatiana_vodka"
}

resource "aws_iam_access_key" "IAMAccessKey3" {
    status = "Active"
    user = "yakov_kolyada"
}

resource "aws_route53_zone" "Route53HostedZone" {
    name = "kolyada.link."
}

resource "aws_route53_record" "Route53RecordSet" {
    name = "kolyada.link."
    type = "A"
    alias {
        name = "s3-website.eu-central-1.amazonaws.com."
        zone_id = "Z21DNDUVLTQW6Q"
        evaluate_target_health = false
    }
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet2" {
    name = "kolyada.link."
    type = "NS"
    ttl = 172800
    records = [
        "ns-1645.awsdns-13.co.uk.",
        "ns-233.awsdns-29.com.",
        "ns-976.awsdns-58.net.",
        "ns-1499.awsdns-59.org."
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet3" {
    name = "kolyada.link."
    type = "SOA"
    ttl = 900
    records = [
        "ns-1645.awsdns-13.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet4" {
    name = "aaj24le4orparuue337x54d6oz4svfuq._domainkey.kolyada.link."
    type = "CNAME"
    ttl = 1800
    records = [
        "aaj24le4orparuue337x54d6oz4svfuq.dkim.amazonses.com"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet5" {
    name = "bhrvejb4jsmtifh3h452qh2ll42ivwxn._domainkey.kolyada.link."
    type = "CNAME"
    ttl = 1800
    records = [
        "bhrvejb4jsmtifh3h452qh2ll42ivwxn.dkim.amazonses.com"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet6" {
    name = "bvvvce25rkeyhl7yfcripncdrtua374a._domainkey.kolyada.link."
    type = "CNAME"
    ttl = 1800
    records = [
        "bvvvce25rkeyhl7yfcripncdrtua374a.dkim.amazonses.com"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet7" {
    name = "admin\\100.kolyada.link."
    type = "TXT"
    ttl = 300
    records = [
        "\"amazonses:\""
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet8" {
    name = "_amazonses.admin\\100.kolyada.link."
    type = "TXT"
    ttl = 300
    records = [
        "\"\""
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet9" {
    name = "app.kolyada.link."
    type = "A"
    alias {
        name = "prodelb-1976402710.eu-central-1.elb.amazonaws.com."
        zone_id = "Z215JYRZR1TBD5"
        evaluate_target_health = false
    }
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet10" {
    name = "_af02d14b8bfec02463b67087594aac26.app.kolyada.link."
    type = "CNAME"
    ttl = 300
    records = [
        "_f618dc15385f56f2bc44dfccfb4c301d.myvvytqgqf.acm-validations.aws."
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet11" {
    name = "mx1.kolyada.link."
    type = "MX"
    ttl = 300
    records = [
        "10 inbound-smtp.eu-central-1.amazonaws.com"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet12" {
    name = "www.kolyada.link."
    type = "A"
    alias {
        name = "kolyada.link."
        zone_id = "Z0159622Q42LBH5B3J59"
        evaluate_target_health = false
    }
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_opsworks_user_profile" "OpsWorksUserProfile" {
    allow_self_management = false
    user_arn = "arn:aws:iam::632397271958:user/phpapp"
    ssh_username = "phpapp"
}

resource "aws_cloudwatch_dashboard" "CloudWatchDashboard" {
    dashboard_name = "my-production-system"
    dashboard_body = "{\"widgets\":[{\"type\":\"metric\",\"x\":0,\"y\":0,\"width\":6,\"height\":6,\"properties\":{\"view\":\"timeSeries\",\"stacked\":false,\"metrics\":[[\"AWS/EC2\",\"CPUUtilization\",\"InstanceId\",\"i-0eb3596eafefd5862\"]],\"region\":\"eu-central-1\"}},{\"type\":\"metric\",\"x\":6,\"y\":0,\"width\":6,\"height\":6,\"properties\":{\"view\":\"timeSeries\",\"stacked\":false,\"metrics\":[[\"AWS/EC2\",\"NetworkPacketsIn\",\"InstanceId\",\"i-0eb3596eafefd5862\"]],\"region\":\"eu-central-1\"}},{\"type\":\"metric\",\"x\":12,\"y\":0,\"width\":6,\"height\":6,\"properties\":{\"view\":\"timeSeries\",\"stacked\":false,\"metrics\":[[\"AWS/EC2\",\"NetworkPacketsOut\",\"InstanceId\",\"i-0eb3596eafefd5862\"]],\"region\":\"eu-central-1\"}}]}"
}

resource "aws_cloudtrail" "CloudTrailTrail" {
    name = "management-events"
    s3_bucket_name = "aws-cloudtrail-logs-632397271958-99b8a708"
    include_global_service_events = true
    is_multi_region_trail = true
    enable_log_file_validation = false
    enable_logging = true
}

resource "aws_budgets_budget" "BudgetsBudget" {
    limit_amount = "1.0"
    limit_unit = "USD"
    time_unit = "MONTHLY"
    cost_filters {}
    name = "My Zero-Spend Budget"
    cost_types {
        include_support = true
        include_other_subscription = true
        include_tax = true
        include_subscription = true
        use_blended = false
        include_upfront = true
        include_discount = true
        include_credit = false
        include_recurring = true
        use_amortized = false
        include_refund = false
    }
    budget_type = "COST"
}
