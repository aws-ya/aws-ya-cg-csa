# AWS How To

## Out of scope

- [How can I purge/clean the build queue?](https://docs.cloudbees.com/docs/cloudbees-ci-kb/latest/client-and-managed-controllers/how-can-i-purge-or-clean-the-build-queue)
- [How to create jenkins credentials via the REST API?](https://stackoverflow.com/questions/29616660/how-to-create-jenkins-credentials-via-the-rest-api)
- [How to manage Credentials via the REST API](https://docs.cloudbees.com/docs/cloudbees-ci-kb/latest/client-and-managed-controllers/how-to-manage-credentials-via-the-rest-api)
- [credentials-plugin](https://github.com/jenkinsci/credentials-plugin/blob/master/docs/user.adoc#rest-api)
- [How to add Jenkins credentials with curl or Ansible](https://tutorials.releaseworksacademy.com/learn/how-to-add-jenkins-credentials-with-curl-or-ansible)
- [https://copyprogramming.com/howto/how-to-make-a-curl-request-with-json-in-jenkins-pipeline-groovy-script](https://copyprogramming.com/howto/how-to-make-a-curl-request-with-json-in-jenkins-pipeline-groovy-script)

## Tags

- [Getting Started with Resource Tags](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/guides/resource-tagging)
- [How to have multiple tags with terraform](https://discuss.hashicorp.com/t/how-to-have-multiple-tags-with-terraform/43935)
- [Terraform: Is there a concise syntax for specifying multiple tags for a resource?](https://devops.stackexchange.com/questions/2115/terraform-is-there-a-concise-syntax-for-specifying-multiple-tags-for-a-resource)

## Variables and Data Sources

- [Input Variables](https://developer.hashicorp.com/terraform/language/values/variables)
- [Terraform .tfvars files: Variables Management with Examples](https://spacelift.io/blog/terraform-tfvars)
- [Terraform Data Sources – How They Are Utilized](https://spacelift.io/blog/terraform-data-sources-how-they-are-utilised)

## Credentials

- [CLI Configuration File](https://developer.hashicorp.com/terraform/cli/config/config-file)
- [3 Ways to Configure Terraform to use your AWS Account](https://banhawy.medium.com/3-ways-to-configure-terraform-to-use-your-aws-account-fb00a08ded5)

## Outputs

- [Command: output](https://developer.hashicorp.com/terraform/cli/commands/output)
- [Terraform Output Values : Complete Guide & Examples](https://spacelift.io/blog/terraform-output)
- [jq: print key and value for each entry in an object](https://stackoverflow.com/questions/34226370/jq-print-key-and-value-for-each-entry-in-an-object)

## Modules

- [Modules](https://developer.hashicorp.com/terraform/language/modules)
- [Что такое модули Terraform и как они работают?](https://habr.com/ru/articles/553576/)

## Remote Stafe

- [Store terraform state file on Terraform cloud Remote](https://www.middlewareinventory.com/blog/store-terraform-state-terraform-cloud/)
- [Remote State](https://developer.hashicorp.com/terraform/language/state/remote)
- [https://medium.com/@109manojsaini/store-terraform-state-file-on-terraform-cloud-remote-d5bd85064dd](https://medium.com/@109manojsaini/store-terraform-state-file-on-terraform-cloud-remote-d5bd85064dd)