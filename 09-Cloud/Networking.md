# Networking

## TOC

### AWS Docs

- [AWS Quick Start](https://github.com/aws-quickstart)
- [Automate deployments to the AWS Cloud](https://aws.amazon.com/quickstart/?solutions-all.sort-by=item.additionalFields.sortDate&solutions-all.sort-order=desc&awsf.filter-content-type=*all&awsf.filter-tech-category=tech-category%23app-integration&awsf.filter-industry=*all&awsm.page-solutions-all=2)
- [aws-vpc.template.yaml](https://github.com/aws-ia/cfn-ps-aws-vpc/blob/main/templates/aws-vpc.template.yaml)
- [TaskCat CI/CD Pipeline for AWS CloudFormation](https://aws.amazon.com/solutions/implementations/taskcat-ci/)
- [TaskCat CI/CD Pipeline for AWS CloudFormation on AWS](https://aws-ia.github.io/cfn-ps-taskcat-ci/#_configuring_your_github_repository)
- [Run commands on your Linux instance at launch](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html)
- [Install LAMP on Amazon Linux 2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html)

- [CloudFormation: AWS resource and property types reference](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html)
- [CloudFormation: Template snippets](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/CHAP_TemplateQuickRef.html)
- [AWS CloudFormation Sample Templates - AWS GovCloud (US) region](https://aws.amazon.com/cloudformation/resources/templates/govcloud-us/)
- [AWS CloudFormation Resources](https://aws.amazon.com/cloudformation/resources/)
- [GitHub: AWS CloudFormation](https://github.com/aws-cloudformation)

- [Export AWS configuration as CloudFormation template](https://stackoverflow.com/questions/38057526/export-aws-configuration-as-cloudformation-template)
- [An easy way to generate Terraform code for existing AWS resources](https://awstip.com/an-easy-way-to-generate-terraform-code-for-existing-aws-resources-cc5ddd20cbd9)
- [Former2 - allows you to generate Infrastructure-as-Code outputs from your existing resources within your AWS account](https://former2.com/)
- [Creating a stack from existing resources](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/resource-import-new-stack.html)
- [Resources that support import and drift detection operations](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/resource-import-supported-resources.html)
- [CloudFormation Exports and Imports](https://thomasstep.com/blog/cloudformation-exports-and-imports)
- [jq - How to convert a json response into yaml in bash](https://stackoverflow.com/questions/53315791/how-to-convert-a-json-response-into-yaml-in-bash)
- [yq - a lightweight and portable command-line YAML processor](https://mikefarah.gitbook.io/yq/)
- [yq - GitHub](https://github.com/mikefarah/yq/#install)

- [Amazon Virtual Private Cloud - User Guide](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-eips.html)
- [Amazon Elastic Compute Cloud - User Guide for Linux Instances](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html)

- [Getting Started With AWS > Amazon VPC Setup > Create VPC](https://beginaws.awsstudygroup.com/2-vpc-setup/1-create-vpc/)
- [beginaws.awsstudygroup.com](https://beginaws.awsstudygroup.com/)
- [** Deploy AWS Cloudformation Template Using AWS CLI | Create Private Subnet,Nat Gateway, Elastic Ip, Private Route Table & Associate](https://dheeraj3choudhary.com/deploy-aws-cloudformation-template-using-aws-cli-or-create-private-subnetnat-gateway-elastic-ip-private-route-table-and-associate)

### MySQL

- [How to Install MySQL on Amazon Linux 2023](https://muleif.medium.com/how-to-install-mysql-on-amazon-linux-2023-5d39afa5bf11)
- [Securing the Initial MySQL Account](https://dev.mysql.com/doc/refman/8.0/en/default-privileges.html)
- [Configuring a default root password for MySQL/MariaDB](https://www.ibm.com/docs/en/spectrum-lsf-rtm/10.2.0?topic=ssl-configuring-default-root-password-mysqlmariadb)
- [Reset a MySQL root password](https://docs.rackspace.com/docs/reset-a-mysql-root-password)
- [How to Reset the Root Password](https://dev.mysql.com/doc/refman/8.0/en/resetting-permissions.html)
- [MYSQL - WHAT IS THE DEFAULT USERNAME AND PASSWORD](https://dbschema.com/2020/04/21/mysql/default-username-password/)
- [How To Allow Remote Access to MySQL](https://www.digitalocean.com/community/tutorials/how-to-allow-remote-access-to-mysql)
- [MySQL List Users: View All Users in MySQL](https://blog.udemy.com/mysql-list-users/?utm_source=adwords&utm_medium=udemyads&utm_campaign=DSA_Catchall_la.EN_cc.ROW&utm_content=deal4584&utm_term=_._ag_88010211481_._ad_535397282064_._kw__._de_c_._dm__._pl__._ti_dsa-393987629421_._li_1012868_._pd__._&matchtype=&gad_source=1&gclid=Cj0KCQiA7aSsBhCiARIsALFvovzww_NiN1bARyVTFRttmeGbYdRvR6E8BM3chDf0Gok9akkAiju8OqEaAjb_EALw_wcB)

### Bash

- [How to Use sed Command to Delete a Line](https://phoenixnap.com/kb/sed-delete-line)
- [Set environment variables from file of key/value pairs](https://stackoverflow.com/questions/19331497/set-environment-variables-from-file-of-key-value-pairs)
- [Redirect all output to file in Bash](https://stackoverflow.com/questions/6674327/redirect-all-output-to-file-in-bash)
- [Reversing the List of Words in a Bash String](https://www.baeldung.com/linux/bash-reverse-string-word-order)
 - [How to find your IP address in Linux](https://opensource.com/article/18/5/how-find-ip-address-linux)

### Architecture

- [Architecture Diagrams](https://aws.amazon.com/architecture/icons/)

### Solution

- [Deploy an Auto Scaling Group behind an Application Load Balancer and NAT Gateway using AWS CloudFormation](https://medium.com/@dahmearjohnson/deploy-an-auto-scaling-group-behind-an-application-load-balancer-and-nat-gateway-using-aws-fcc76c438f86)
- [NAT gateway use cases](https://docs.aws.amazon.com/vpc/latest/userguide/nat-gateway-scenarios.html)
- [Tutorial: Integrate a NAT gateway with a public load balancer using the Azure portal](https://learn.microsoft.com/en-us/azure/nat-gateway/tutorial-nat-gateway-load-balancer-public-portal)
- [Using the NAT gateway and Gateway Load Balancer with EC2 instances for centralized egress](https://docs.aws.amazon.com/whitepapers/latest/building-scalable-secure-multi-vpc-network-infrastructure/using-nat-gateway-and-gwlb-with-ec2.html)
- [Introducing AWS Gateway Load Balancer: Supported architecture patterns](https://aws.amazon.com/blogs/networking-and-content-delivery/introducing-aws-gateway-load-balancer-supported-architecture-patterns/)
- [Load balancer subnets and routing](https://docs.aws.amazon.com/prescriptive-guidance/latest/load-balancer-stickiness/subnets-routing.html)


## VPC

```bash
curl ifconfig.me
curl ipinfo.io/ip
curl ident.me
curl checkip.dyndns.org
```

1. Create a `myVPC` VPC, use the CIDR "10.0.0.0/16", use the `Default` Tenancy, use tag Name as `myVPC`

    1. Create a `myPrivateSubnet` Private Subnet for the `myVPC` VPC, use the `eu-central-1a` Availability Zone, use the CIDR "10.0.1.0/24", use tag Name as `myPrivateSubnet`
    2. Create a `myPublicSubnet` Public Subnet for the `myVPC` VPC, use the `eu-central-1b` Availability Zone, use the CIDR "10.0.2.0/24", use tag Name as `myPublicSubnet`, enable Auto-assign public IP address settings
    3. Create a `myIGW` Internet Gateway and attach it to `myVPC`
    4. Create a `myPublicRoute` Route Table, attach it to `myVPC`, add `0.0.0.0/0` Route throuth the `myIGW` Internet Gateway, then add subnet associations with the `myPublicSubnet` Subnet

2. Create three EC2 instances and configure Security Groups for them

    1. Create a `myPublicInstance` EC2 Instance, use Amazon Linux 2, Instance Type is `t2.micro`, Key pair name is `aws`, VPC is `myVPC` and choose `myPublicSubnet`, Auto-assing public IP is `Enabled`, create `myWebDMZ` Security Group, Security group rule is `ssh from Anyware` 
    2. Create a `myPrivateInstance` EC2 Instance, use Amazon Linux 2, Instance Type is `t2.micro`, Key pair name is `aws`, VPC is `myVPC` and choose `myPrivateSubnet`, Auto-assing public IP is `Disabled`, create `myPrivateSG` Security Group, remove all Security group rules from the `myPrivateSG`
    3. Create a `myBastionHost` EC2 Instance, use Amazon Linux 2, Instance Type is `t2.micro`, Key pair name is `aws`, VPC is `myVPC` and choose `myPublicSubnet`, Auto-assing public IP is `Enabled`, create `myBastionSG` Security Group, Security group rule is `ssh from Anyware` 
    4. Add Inbound rule for the `myPrivateSG` to have SSH access from the `myBastionSG` Security Group
    5. Create a `myNATGateway` NAT Gateway, choose the `myPublicSubnet` Public Subnet, Connectivity Type is `public`, choose the allocate an Elastic IP
    6. Create a `myPrivateRoute` Route Table, attach it to `myVPC`, add `0.0.0.0/0` Route throuth `myNATGateway` NAT Gateway, then add subnet associations with the `myPrivateSubnet` Subnet

```yaml
  # https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/quickref-ec2-vpc.html
  # aws ec2 describe-vpcs --output yaml > vpcs.yaml
  # aws ec2 describe-route-tables --output yaml > route-tables.yaml

  MyVPC:
    Type: "AWS::EC2::VPC"
    Properties:
      CidrBlock: "10.0.0.0/16"
      InstanceTenancy: default
      Tags:
        - Key: Name
          Value: MyVPC
  
  # MyDhcpOptions:
  #   Type: "AWS::EC2::DHCPOptions"
  #   Properties:
  #     DomainName: "example.com"
  #     DomainNameServers:
  #       - "10.0.0.2"
  #       - "10.0.0.3"
  #     NtpServers:
  #       - "10.0.0.4"
  #     NetbiosNameServers:
  #       - "10.0.0.5"
  #       - "10.0.0.6"
  #     NetbiosNodeType: 2

  # MyVPCDhcpOptionsAssociation:
  #   Type: "AWS::EC2::VPCDHCPOptionsAssociation"
  #   Properties:
  #     DhcpOptionsId: !Ref MyDhcpOptions
  #     VpcId: !Ref MyVPC
```

```bash
aws iam list-policies --query "Policies[?contains(PolicyName, 'ReadOnlyAccess')].{PolicyName:PolicyName,Arn:Arn}"
aws iam list-policies --query "Policies[?contains(PolicyName, 'ReadOnlyAccess')].{PolicyName:PolicyName,Arn:Arn}" | xargs -n 2
```

## NAT gateway

- [NAT gateways](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html)

## CLI

- [jmespath.org](https://jmespath.org/)
- [JMESPath Tutorial](https://jmespath.org/tutorial.html)
- [AWS CLI JMESPath cheatsheet](https://gist.github.com/magnetikonline/6a382a4c4412bbb68e33e137b9a74168)
- [Filter AWS CLI output](https://docs.aws.amazon.com/cli/latest/userguide/cli-usage-filter.html)
- [Quotation marks with strings in the AWS CLI](https://docs.aws.amazon.com/cli/v1/userguide/cli-usage-parameters-quoting-strings.html)

```bash
aws cloudformation list-stacks --output json

aws cloudformation list-stacks --generate-cli-skeleton

aws cloudformation list-stacks --output text --query StackSummaries[].[StackName,StackStatus,DeletionTime] 

HOSTED_ZONE_ID=Z0159622Q42LBH5B3J59

aws route53 list-resource-record-sets   --hosted-zone-id $HOSTED_ZONE_ID   --output text   --query "ResourceRecordSets[].[join(': ',[Name,Type])]"

aws route53 list-resource-record-sets   --hosted-zone-id $HOSTED_ZONE_ID   --output text   --query "ResourceRecordSets[?Type=='A'].[join(': ',[Name,Type])]"

aws cloudformation list-stacks --output text --query StackSummaries[].[StackName,StackStatus,DeletionTime] --stack-status-filter UPDATE_COMPLETE

aws route53 list-resource-record-sets \
  --hosted-zone-id $HOSTED_ZONE_ID \
  --output text \
  --query "ResourceRecordSets[].[join(': ',[Name,Type])]"

aws ec2 describe-volumes --query 'Volumes[?Size < `20`].VolumeId'

aws ec2 describe-volumes --query 'Volumes[?Size < `20`].VolumeId' --output text | xargs -n 1

aws ec2 describe-volumes --query 'Volumes[?Size < `20`].[join(`:`,[VolumeId,to_string(Size)])]' --output text | xargs -n 1

aws ec2 describe-volumes --query 'Volumes[*].Attachments[].InstanceId | [0]'

aws ec2 describe-volumes --query 'Volumes[].[VolumeId, VolumeType]' --output text

aws ec2 describe-volumes --query 'Volumes[].[VolumeId, VolumeType, Attachments[].[InstanceId, State]]' --output text
aws ec2 describe-volumes --query 'Volumes[].[VolumeId, VolumeType, Attachments[].[InstanceId, State][]][]' --output text

aws ec2 describe-volumes --query 'Volumes[].{VolumeType: VolumeType}'

aws ec2 describe-volumes \
  --query 'sort_by(Volumes, &VolumeId)[].{VolumeId: VolumeId, VolumeType: VolumeType, InstanceId: Attachments[0].InstanceId, State: Attachments[0].State}'

 aws ec2 describe-volumes \
  --query 'sort_by(Volumes, &VolumeId)[].{VolumeId: VolumeId, VolumeType: VolumeType, InstanceId: Attachments[0].InstanceId, State: Attachments[0].State} | [].[State,VolumeId]'

aws ec2 describe-volumes \
  --query 'sort_by(Volumes, &VolumeId)[].{VolumeId: VolumeId, VolumeType: VolumeType, InstanceId: Attachments[0].InstanceId, State: Attachments[0].State} | [].[join(`: `,[State,VolumeId])]'

aws ec2 describe-volumes \
  --query 'Volumes.Tags[?Value == `test`]'

# aws ec2 describe-volumes \
#   --query 'Volumes[!not_null(Tags[?Value == `test`].Value)]'

aws ec2 describe-images \
  --owners amazon \
  --filters "Name=name,Values=amzn*gp2" "Name=virtualization-type,Values=hvm" "Name=root-device-type,Values=ebs" \
  --query "sort_by(Images, &CreationDate)[-1].ImageId" \
  --output text  
```
