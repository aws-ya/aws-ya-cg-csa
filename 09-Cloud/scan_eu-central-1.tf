terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.0"
        }
    }
}

provider "aws" {
    region = "eu-central-1"
}

resource "aws_iam_user" "IAMUser" {
    path = "/"
    name = "phpapp"
    tags = {
        AKIAZGPOQSOLMDRBRRRK = "phpapp"
    }
}

resource "aws_iam_user" "IAMUser2" {
    path = "/"
    name = "tatiana_vodka"
    tags = {
        Name = "t-vodka"
    }
}

resource "aws_iam_user" "IAMUser3" {
    path = "/"
    name = "yakov_kolyada"
    tags = {
        Department = "Engineering"
        Name = "y-kolyada"
    }
}

resource "aws_iam_group" "IAMGroup" {
    path = "/"
    name = "Admin"
}

resource "aws_iam_role" "IAMRole" {
    path = "/"
    name = "DemoRoleForEC2"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"ec2.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole2" {
    path = "/"
    name = "ecsTaskExecutionRole"
    assume_role_policy = "{\"Version\":\"2008-10-17\",\"Statement\":[{\"Sid\":\"\",\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"ecs-tasks.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole3" {
    path = "/service-role/"
    name = "lambda_basic_execution"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"lambda.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole4" {
    path = "/service-role/"
    name = "ya-lambda-function-role-boc3va9m"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"lambda.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole5" {
    path = "/"
    name = "rds-monitoring-role"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Sid\":\"\",\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"monitoring.rds.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole6" {
    path = "/"
    name = "yaImageBuilderRole"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"ec2.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_role" "IAMRole7" {
    path = "/"
    name = "yaCloudWatchAgentPolicy"
    assume_role_policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"ec2.amazonaws.com\"},\"Action\":\"sts:AssumeRole\"}]}"
    max_session_duration = 3600
    tags = {}
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole" {
    aws_service_name = "autoscaling.amazonaws.com"
    description = "Default Service-Linked Role enables access to AWS Services and Resources used or managed by Auto Scaling"
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole2" {
    aws_service_name = "ecs.amazonaws.com"
    description = "Role to enable Amazon ECS to manage your cluster."
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole3" {
    aws_service_name = "elasticloadbalancing.amazonaws.com"
    description = "Allows ELB to call AWS services on your behalf."
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole4" {
    aws_service_name = "imagebuilder.amazonaws.com"
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole5" {
    aws_service_name = "globalaccelerator.amazonaws.com"
    description = "Allows Global Accelerator to call AWS services on customer's behalf"
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole6" {
    aws_service_name = "rds.amazonaws.com"
    description = "Allows Amazon RDS to manage AWS resources on your behalf"
}

resource "aws_iam_service_linked_role" "IAMServiceLinkedRole7" {
    aws_service_name = "resource-explorer-2.amazonaws.com"
}

resource "aws_iam_policy" "IAMManagedPolicy" {
    name = "policy_scenario1_t-vodka"
    path = "/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:GetLifecycleConfiguration",
                "s3:GetBucketTagging",
                "s3:GetInventoryConfiguration",
                "s3:GetAccessPointPolicyForObjectLambda",
                "s3:ListBucketVersions",
                "s3:GetBucketLogging",
                "s3:ListBucket",
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketPolicy",
                "s3:GetStorageLensConfigurationTagging",
                "s3:GetEncryptionConfiguration",
                "s3:GetBucketObjectLockConfiguration",
                "s3:GetIntelligentTieringConfiguration",
                "s3:DeleteAccessPointPolicyForObjectLambda",
                "s3:GetBucketRequestPayment",
                "s3:GetAccessPointPolicyStatus",
                "s3:PutBucketAcl",
                "s3:GetMetricsConfiguration",
                "s3:GetBucketOwnershipControls",
                "s3:GetBucketPublicAccessBlock",
                "s3:PutAccessPointPolicyForObjectLambda",
                "s3:GetBucketPolicyStatus",
                "s3:ListBucketMultipartUploads",
                "s3:PutBucketPublicAccessBlock",
                "s3:GetBucketWebsite",
                "s3:GetJobTagging",
                "s3:GetAccessPointPolicyStatusForObjectLambda",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetAccessPointConfigurationForObjectLambda",
                "s3:GetBucketNotification",
                "s3:DeleteBucketPolicy",
                "s3:DescribeMultiRegionAccessPointOperation",
                "s3:GetReplicationConfiguration",
                "s3:GetStorageLensConfiguration",
                "s3:DescribeJob",
                "cloudwatch:DescribeAlarms",
                "s3:GetBucketCORS",
                "s3:GetAnalyticsConfiguration",
                "s3:PutBucketPolicy",
                "s3:DeleteAccessPointPolicy",
                "s3:PutAccessPointPolicy",
                "s3:GetBucketLocation",
                "s3:GetAccessPointPolicy",
                "s3:GetAccessPointForObjectLambda",
                "s3:GetStorageLensDashboard"
            ],
            "Resource": [
                "arn:aws:cloudwatch:*:632397271958:alarm:*",
                "arn:aws:s3:::*",
                "arn:aws:s3-object-lambda:*:632397271958:accesspoint/*",
                "arn:aws:s3:*:632397271958:storage-lens/*",
                "arn:aws:s3:*:632397271958:job/*",
                "arn:aws:s3:us-west-2:632397271958:async-request/mrap/*/*",
                "arn:aws:s3:*:632397271958:accesspoint/*"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:GetMultiRegionAccessPointPolicyStatus",
                "s3:GetObjectRetention",
                "s3:GetMultiRegionAccessPointPolicy",
                "s3:PutMultiRegionAccessPointPolicy",
                "s3:GetObjectVersionTagging",
                "s3:GetMultiRegionAccessPoint",
                "s3:GetObjectAttributes",
                "s3:GetObjectLegalHold",
                "s3:GetObjectVersionAttributes",
                "s3:BypassGovernanceRetention",
                "s3:ListMultipartUploadParts",
                "s3:GetObjectVersionTorrent",
                "s3:GetObjectAcl",
                "s3:GetObject",
                "s3:ObjectOwnerOverrideToBucketOwner",
                "s3:GetObjectTorrent",
                "s3:GetMultiRegionAccessPointRoutes",
                "s3:PutObjectVersionAcl",
                "s3:GetObjectVersionAcl",
                "s3:GetObjectTagging",
                "s3:GetObjectVersionForReplication",
                "s3:PutObjectAcl",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:::*/*",
                "arn:aws:s3::632397271958:accesspoint/*"
            ]
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": [
                "s3:ListAccessPointsForObjectLambda",
                "s3:GetAccessPoint",
                "ec2:DescribeInstances",
                "s3:PutAccountPublicAccessBlock",
                "s3:ListAccessPoints",
                "s3:ListJobs",
                "ec2:DescribeSnapshotTierStatus",
                "s3:ListMultiRegionAccessPoints",
                "ec2:ReportInstanceStatus",
                "s3:ListStorageLensConfigurations",
                "ec2:DescribeVolumeStatus",
                "s3:GetAccountPublicAccessBlock",
                "ec2:GetSerialConsoleAccessStatus",
                "s3:ListAllMyBuckets",
                "cloudwatch:DescribeAlarmsForMetric",
                "s3:PutAccessPointPublicAccessBlock",
                "ec2:DescribeInstanceStatus"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "IAMManagedPolicy2" {
    name = "AWSLambdaBasicExecutionRole-aa8e1ba1-178c-42f8-99c8-871df9bdb907"
    path = "/service-role/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:eu-central-1:632397271958:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:eu-central-1:632397271958:log-group:/aws/lambda/hello-world-python:*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_policy" "IAMManagedPolicy3" {
    name = "AWSLambdaBasicExecutionRole-d9424a61-be9d-42d9-8fdc-bb26a6ccaebf"
    path = "/service-role/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:eu-central-1:632397271958:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:eu-central-1:632397271958:log-group:/aws/lambda/ya-lambda-function:*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "IAMInstanceProfile" {
    path = "/"
    name = "${aws_iam_role.IAMRole.name}"
    roles = [
        "${aws_iam_role.IAMRole.name}"
    ]
}

resource "aws_iam_instance_profile" "IAMInstanceProfile2" {
    path = "/"
    name = "${aws_iam_role.IAMRole7.name}"
    roles = [
        "${aws_iam_role.IAMRole7.name}"
    ]
}

resource "aws_iam_instance_profile" "IAMInstanceProfile3" {
    path = "/"
    name = "${aws_iam_role.IAMRole6.name}"
    roles = [
        "${aws_iam_role.IAMRole6.name}"
    ]
}

resource "aws_iam_access_key" "IAMAccessKey" {
    status = "Active"
    user = "phpapp"
}

resource "aws_iam_access_key" "IAMAccessKey2" {
    status = "Active"
    user = "yakov_kolyada"
}

resource "aws_iam_access_key" "IAMAccessKey3" {
    status = "Active"
    user = "tatiana_vodka"
}

resource "aws_vpc" "EC2VPC" {
    cidr_block = "10.10.0.0/16"
    enable_dns_support = true
    enable_dns_hostnames = true
    instance_tenancy = "default"
    tags = {
        Name = "prodVPC"
    }
}

resource "aws_subnet" "EC2Subnet" {
    availability_zone = "eu-central-1b"
    cidr_block = "10.10.20.0/24"
    vpc_id = "${aws_vpc.EC2VPC.id}"
    map_public_ip_on_launch = true
}

resource "aws_subnet" "EC2Subnet2" {
    availability_zone = "eu-central-1b"
    cidr_block = "10.10.2.0/24"
    vpc_id = "${aws_vpc.EC2VPC.id}"
    map_public_ip_on_launch = false
}

resource "aws_subnet" "EC2Subnet3" {
    availability_zone = "eu-central-1a"
    cidr_block = "10.10.10.0/24"
    vpc_id = "${aws_vpc.EC2VPC.id}"
    map_public_ip_on_launch = true
}

resource "aws_subnet" "EC2Subnet4" {
    availability_zone = "eu-central-1a"
    cidr_block = "10.10.1.0/24"
    vpc_id = "${aws_vpc.EC2VPC.id}"
    map_public_ip_on_launch = false
}

resource "aws_internet_gateway" "EC2InternetGateway" {
    tags = {
        Name = "prodInternetGateway"
    }
    vpc_id = "${aws_vpc.EC2VPC.id}"
}

resource "aws_eip" "EC2EIP" {
    vpc = true
}

resource "aws_eip_association" "EC2EIPAssociation" {
    allocation_id = "eipalloc-0418d1c6363f132eb"
    network_interface_id = "eni-059d0b94c7770a234"
    private_ip_address = "10.10.10.110"
}

resource "aws_vpc_dhcp_options" "EC2DHCPOptions" {
    domain_name = "eu-central-1.compute.internal"
    tags = {}
}

resource "aws_vpc_dhcp_options_association" "EC2VPCDHCPOptionsAssociation" {
    dhcp_options_id = "dopt-00c662e3e6b78869c"
    vpc_id = "${aws_vpc.EC2VPC.id}"
}

resource "aws_route_table" "EC2RouteTable" {
    vpc_id = "${aws_vpc.EC2VPC.id}"
    tags = {
        Name = "prodPrivateRouteTable"
    }
}

resource "aws_route_table" "EC2RouteTable2" {
    vpc_id = "${aws_vpc.EC2VPC.id}"
    tags = {
        Name = "prodPublicRouteTable"
    }
}

resource "aws_route_table" "EC2RouteTable3" {
    vpc_id = "${aws_vpc.EC2VPC.id}"
    tags = {}
}

resource "aws_route" "EC2Route" {
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id = "nat-0cb43891a35af2685"
    route_table_id = "rtb-03ab98212fc8ac576"
}

resource "aws_route" "EC2Route2" {
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "igw-0b0f759e67b2cf01d"
    route_table_id = "rtb-022f9bb04d69bf640"
}

resource "aws_nat_gateway" "EC2NatGateway" {
    subnet_id = "subnet-0a93e109ec34454e8"
    tags = {
        Name = "prodNatGateway"
    }
    allocation_id = "eipalloc-00b93cc51fef34626"
}

resource "aws_nat_gateway" "EC2NatGateway2" {
    subnet_id = "subnet-0c9a74482ef54c66c"
    tags = {
        Name = "prodNatGateway"
    }
    allocation_id = "eipalloc-0418d1c6363f132eb"
}

resource "aws_nat_gateway" "EC2NatGateway3" {
    subnet_id = "subnet-05a9dd42574e1cf44"
    tags = {
        Name = "prodNatGateway"
    }
    allocation_id = "eipalloc-059d02a725085c6fc"
}

resource "aws_route_table_association" "EC2SubnetRouteTableAssociation" {
    route_table_id = "rtb-03ab98212fc8ac576"
    subnet_id = "subnet-01bfbea3638066304"
}

resource "aws_route_table_association" "EC2SubnetRouteTableAssociation2" {
    route_table_id = "rtb-03ab98212fc8ac576"
    subnet_id = "subnet-0d78267ed6b30f68b"
}

resource "aws_route_table_association" "EC2SubnetRouteTableAssociation3" {
    route_table_id = "rtb-022f9bb04d69bf640"
    subnet_id = "subnet-0c9a74482ef54c66c"
}

resource "aws_route_table_association" "EC2SubnetRouteTableAssociation4" {
    route_table_id = "rtb-022f9bb04d69bf640"
    subnet_id = "subnet-0002439b41edf1d9c"
}

resource "aws_route53_zone" "Route53HostedZone" {
    name = "kolyada.link."
}

resource "aws_route53_record" "Route53RecordSet" {
    name = "kolyada.link."
    type = "A"
    alias {
        name = "s3-website.eu-central-1.amazonaws.com."
        zone_id = "Z21DNDUVLTQW6Q"
        evaluate_target_health = false
    }
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet2" {
    name = "kolyada.link."
    type = "NS"
    ttl = 172800
    records = [
        "ns-1645.awsdns-13.co.uk.",
        "ns-233.awsdns-29.com.",
        "ns-976.awsdns-58.net.",
        "ns-1499.awsdns-59.org."
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet3" {
    name = "kolyada.link."
    type = "SOA"
    ttl = 900
    records = [
        "ns-1645.awsdns-13.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet4" {
    name = "aaj24le4orparuue337x54d6oz4svfuq._domainkey.kolyada.link."
    type = "CNAME"
    ttl = 1800
    records = [
        "aaj24le4orparuue337x54d6oz4svfuq.dkim.amazonses.com"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet5" {
    name = "bhrvejb4jsmtifh3h452qh2ll42ivwxn._domainkey.kolyada.link."
    type = "CNAME"
    ttl = 1800
    records = [
        "bhrvejb4jsmtifh3h452qh2ll42ivwxn.dkim.amazonses.com"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet6" {
    name = "bvvvce25rkeyhl7yfcripncdrtua374a._domainkey.kolyada.link."
    type = "CNAME"
    ttl = 1800
    records = [
        "bvvvce25rkeyhl7yfcripncdrtua374a.dkim.amazonses.com"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet7" {
    name = "admin\\100.kolyada.link."
    type = "TXT"
    ttl = 300
    records = [
        "\"amazonses:\""
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet8" {
    name = "_amazonses.admin\\100.kolyada.link."
    type = "TXT"
    ttl = 300
    records = [
        "\"\""
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet9" {
    name = "app.kolyada.link."
    type = "A"
    alias {
        name = "prodelb-1976402710.eu-central-1.elb.amazonaws.com."
        zone_id = "Z215JYRZR1TBD5"
        evaluate_target_health = false
    }
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet10" {
    name = "_af02d14b8bfec02463b67087594aac26.app.kolyada.link."
    type = "CNAME"
    ttl = 300
    records = [
        "_f618dc15385f56f2bc44dfccfb4c301d.myvvytqgqf.acm-validations.aws."
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet11" {
    name = "mx1.kolyada.link."
    type = "MX"
    ttl = 300
    records = [
        "10 inbound-smtp.eu-central-1.amazonaws.com"
    ]
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_route53_record" "Route53RecordSet12" {
    name = "www.kolyada.link."
    type = "A"
    alias {
        name = "kolyada.link."
        zone_id = "Z0159622Q42LBH5B3J59"
        evaluate_target_health = false
    }
    zone_id = "Z0159622Q42LBH5B3J59"
}

resource "aws_instance" "EC2Instance" {
    ami = "ami-0ef03f2a1f5df573c"
    instance_type = "t2.micro"
    key_name = "aws-web"
    availability_zone = "eu-central-1a"
    tenancy = "default"
    ebs_optimized = false
    user_data = "IyEvYmluL2Jhc2gNCmRuZiB1cGRhdGUgLXkNCmRuZiBpbnN0YWxsIC15IG1hcmlhZGIxMDUNCg=="
    tags = {
        Name = "AppBastion"
    }
}

resource "aws_instance" "EC2Instance2" {
    ami = "ami-0ef03f2a1f5df573c"
    instance_type = "t2.micro"
    key_name = "aws-web"
    availability_zone = "eu-central-1a"
    tenancy = "default"
    ebs_optimized = false
    user_data = "IyEvYmluL2Jhc2gNCmRuZiB1cGRhdGUgLXkNCmRuZiBpbnN0YWxsIC15IG1hcmlhZGIxMDUNCg=="
    tags = {
        Name = "AppBastion"
    }
}

resource "aws_instance" "EC2Instance3" {
    ami = "ami-0ef03f2a1f5df573c"
    instance_type = "t2.micro"
    key_name = "aws-web"
    availability_zone = "eu-central-1a"
    tenancy = "default"
    ebs_optimized = false
    user_data = "IyEvYmluL2Jhc2gNCnNldCArZQ0KZGF0ZQ0KZG5mIHVwZGF0ZSAteQ0KDQojIEluc3RhbGwgQVdTIENMSQ0KZG5mIGluc3RhbGwgLXkgYXdzLWNsaQ0KQVBQPXBocGFwcA0KQUNDRVNTX0tFWT1BS0lBWkdQT1FTT0xNRFJCUlJSSw0KU0VDUkVUX0tFWT1yeHJaQW1kaVB1M09iek5FSFV5dkRFMFQ1MjdWSHBDcUlhd1l0Y0RCDQpSRUdJT049ZXUtY2VudHJhbC0xDQphd3MgY29uZmlndXJlIHNldCBhd3NfYWNjZXNzX2tleV9pZCAkQUNDRVNTX0tFWQ0KYXdzIGNvbmZpZ3VyZSBzZXQgYXdzX3NlY3JldF9hY2Nlc3Nfa2V5ICRTRUNSRVRfS0VZDQphd3MgY29uZmlndXJlIHNldCByZWdpb24gJFJFR0lPTg0KYXdzIGNvbmZpZ3VyZSBzZXQgb3V0cHV0IGpzb24NCmF3cyBjb25maWd1cmUgbGlzdA0KYXdzIHN0cyBnZXQtY2FsbGVyLWlkZW50aXR5DQoNCmVjaG8gIkFQUDogJEFQUCINCmF3cyBzMyBscw0KYXdzIHMzIGNwIHMzOi8veWEtY2YtdGVtcGxhdGVzL3BocGFwcC8uZW52IH4vDQoNCmV4cG9ydCAkKGdyZXAgLXYgJ14jJyB+Ly5lbnYgfCB4YXJncykNCkRCX0hPU1Q9JChpcCAtYnIgYSB8IGdyZXAgLXYgbG8gfCB4YXJncyB8IGN1dCAtZCcgJyAtZjMgfCBjdXQgLWQnLycgLWYxKQ0Kc2VkIC1pICcvREJfSE9TVC9kJyB+Ly5lbnYNCmVjaG8gIkRCX0hPU1Q9JERCX0hPU1QiID4+IH4vLmVudg0KYXdzIHMzIGNwIH4vLmVudiBzMzovL3lhLWNmLXRlbXBsYXRlcy9waHBhcHAvDQpybSB+Ly5lbnYNCg0KZWNobyAiREJfTkFNRTogJERCX05BTUUiDQplY2hvICJEQl9ST09UX1BBU1NXT1JEOiAkREJfUk9PVF9QQVNTV09SRCINCmVjaG8gIkRCX0hPU1Q6ICREQl9IT1NUIg0KDQp3Z2V0IGh0dHBzOi8vZGV2Lm15c3FsLmNvbS9nZXQvbXlzcWw4MC1jb21tdW5pdHktcmVsZWFzZS1lbDktNS5ub2FyY2gucnBtDQpkbmYgaW5zdGFsbCBteXNxbDgwLWNvbW11bml0eS1yZWxlYXNlLWVsOS01Lm5vYXJjaC5ycG0gLXkNCmRuZiBpbnN0YWxsIG15c3FsLWNvbW11bml0eS1zZXJ2ZXIgLXkNCg0Kc3lzdGVtY3RsIGVuYWJsZSAtLW5vdyBteXNxbGQNCnN5c3RlbWN0bCBpcy1lbmFibGVkIGh0dHBkICYmIGVjaG8gIm15c3FsZCBpcyBlbmFibGVkIiB8fCBlY2hvICJteXNxbGQgaXMgbm90IGRpc2FibGVkIg0Kc3lzdGVtY3RsIHN0YXR1cyBteXNxbGQNCnN5c3RlbWN0bCBzdG9wIG15c3FsZA0Kc3lzdGVtY3RsIHN0YXR1cyBteXNxbGQNCg0Kc3VkbyAtdSBteXNxbCAvdXNyL3NiaW4vbXlzcWxkIC0tc2tpcC1ncmFudC10YWJsZXMgLS1za2lwX25ldHdvcmtpbmcgJg0Kc2xlZXAgNQ0KbXlzcWwgLXUgcm9vdCAtLXNraXAtcGFzc3dvcmQgLWUgIkZMVVNIIFBSSVZJTEVHRVM7IEFMVEVSIFVTRVIgJ3Jvb3QnQCdsb2NhbGhvc3QnIElERU5USUZJRUQgQlkgJyREQl9ST09UX1BBU1NXT1JEJzsiDQpraWxsICQocHMgeGEgfCBncmVwICJzdWRvIC11IG15c3FsIiB8IGhlYWQgLW4gMSB8IHhhcmdzIHwgY3V0IC1kJyAnIC1mMSkNCnNsZWVwIDEwDQpzeXN0ZW1jdGwgc3RhcnQgbXlzcWxkDQpzeXN0ZW1jdGwgaXMtYWN0aXZlIG15c3FsZCAmJiBlY2hvICJteXNxbGQgaXMgYWN0aXZlIiB8fCBlY2hvICJteXNxbGQgaXMgbm90IGFjdGl2ZSINCm15c3FsIC11IHJvb3QgLXAkREJfUk9PVF9QQVNTV09SRCAtZSAiQ1JFQVRFIFVTRVIgJ3Jvb3QnQCclJyBJREVOVElGSUVEIEJZICckREJfUk9PVF9QQVNTV09SRCc7IEZMVVNIIFBSSVZJTEVHRVM7Ig0KbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIC1lICJHUkFOVCBBTEwgT04gKi4qIFRPICdyb290J0AnJScgV0lUSCBHUkFOVCBPUFRJT047IEZMVVNIIFBSSVZJTEVHRVM7Ig0KbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIC1EIG15c3FsIC1lICJTRUxFQ1QgSG9zdCwgVXNlciBGUk9NIHVzZXIgV0hFUkUgdXNlciA9ICdyb290JzsiID4gfi9kYl91c2Vycw0KY2F0IH4vZGJfdXNlcnMNCg0KYXdzIHMzIGNwIHMzOi8veWEtY2YtdGVtcGxhdGVzL3BocGFwcC9pbml0LnNxbCB+Lw0KbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIDwgfi9pbml0LnNxbA0KZGF0ZQ0KDQpybSAtZiB+Ly5hd3MvDQpoaXN0b3J5IC1jDQo="
    tags = {
        Name = "prodAppDB"
    }
}

resource "aws_instance" "EC2Instance4" {
    ami = "ami-0ef03f2a1f5df573c"
    instance_type = "t2.micro"
    key_name = "aws-web"
    availability_zone = "eu-central-1a"
    tenancy = "default"
    ebs_optimized = false
    user_data = "IyEvYmluL2Jhc2gNCnNldCArZQ0KZGF0ZQ0KZG5mIHVwZGF0ZSAteQ0KDQojIEluc3RhbGwgQVdTIENMSQ0KZG5mIGluc3RhbGwgLXkgYXdzLWNsaQ0KQVBQPXBocGFwcA0KQUNDRVNTX0tFWT1BS0lBWkdQT1FTT0xNRFJCUlJSSw0KU0VDUkVUX0tFWT1yeHJaQW1kaVB1M09iek5FSFV5dkRFMFQ1MjdWSHBDcUlhd1l0Y0RCDQpSRUdJT049ZXUtY2VudHJhbC0xDQphd3MgY29uZmlndXJlIHNldCBhd3NfYWNjZXNzX2tleV9pZCAkQUNDRVNTX0tFWQ0KYXdzIGNvbmZpZ3VyZSBzZXQgYXdzX3NlY3JldF9hY2Nlc3Nfa2V5ICRTRUNSRVRfS0VZDQphd3MgY29uZmlndXJlIHNldCByZWdpb24gJFJFR0lPTg0KYXdzIGNvbmZpZ3VyZSBzZXQgb3V0cHV0IGpzb24NCmF3cyBjb25maWd1cmUgbGlzdA0KYXdzIHN0cyBnZXQtY2FsbGVyLWlkZW50aXR5DQoNCmVjaG8gIkFQUDogJEFQUCINCmF3cyBzMyBscw0KYXdzIHMzIGNwIHMzOi8veWEtY2YtdGVtcGxhdGVzL3BocGFwcC8uZW52IH4vDQoNCmV4cG9ydCAkKGdyZXAgLXYgJ14jJyB+Ly5lbnYgfCB4YXJncykNCkRCX0hPU1Q9JChpcCAtYnIgYSB8IGdyZXAgLXYgbG8gfCB4YXJncyB8IGN1dCAtZCcgJyAtZjMgfCBjdXQgLWQnLycgLWYxKQ0Kc2VkIC1pICcvREJfSE9TVC9kJyB+Ly5lbnYNCmVjaG8gIkRCX0hPU1Q9JERCX0hPU1QiID4+IH4vLmVudg0KYXdzIHMzIGNwIH4vLmVudiBzMzovL3lhLWNmLXRlbXBsYXRlcy9waHBhcHAvDQpybSB+Ly5lbnYNCg0KZWNobyAiREJfTkFNRTogJERCX05BTUUiDQplY2hvICJEQl9ST09UX1BBU1NXT1JEOiAkREJfUk9PVF9QQVNTV09SRCINCmVjaG8gIkRCX0hPU1Q6ICREQl9IT1NUIg0KDQp3Z2V0IGh0dHBzOi8vZGV2Lm15c3FsLmNvbS9nZXQvbXlzcWw4MC1jb21tdW5pdHktcmVsZWFzZS1lbDktNS5ub2FyY2gucnBtDQpkbmYgaW5zdGFsbCBteXNxbDgwLWNvbW11bml0eS1yZWxlYXNlLWVsOS01Lm5vYXJjaC5ycG0gLXkNCmRuZiBpbnN0YWxsIG15c3FsLWNvbW11bml0eS1zZXJ2ZXIgLXkNCg0Kc3lzdGVtY3RsIGVuYWJsZSAtLW5vdyBteXNxbGQNCnN5c3RlbWN0bCBpcy1lbmFibGVkIGh0dHBkICYmIGVjaG8gIm15c3FsZCBpcyBlbmFibGVkIiB8fCBlY2hvICJteXNxbGQgaXMgbm90IGRpc2FibGVkIg0Kc3lzdGVtY3RsIHN0YXR1cyBteXNxbGQNCnN5c3RlbWN0bCBzdG9wIG15c3FsZA0Kc3lzdGVtY3RsIHN0YXR1cyBteXNxbGQNCg0Kc3VkbyAtdSBteXNxbCAvdXNyL3NiaW4vbXlzcWxkIC0tc2tpcC1ncmFudC10YWJsZXMgLS1za2lwX25ldHdvcmtpbmcgJg0Kc2xlZXAgNQ0KbXlzcWwgLXUgcm9vdCAtLXNraXAtcGFzc3dvcmQgLWUgIkZMVVNIIFBSSVZJTEVHRVM7IEFMVEVSIFVTRVIgJ3Jvb3QnQCdsb2NhbGhvc3QnIElERU5USUZJRUQgQlkgJyREQl9ST09UX1BBU1NXT1JEJzsiDQpraWxsICQocHMgeGEgfCBncmVwICJzdWRvIC11IG15c3FsIiB8IGhlYWQgLW4gMSB8IHhhcmdzIHwgY3V0IC1kJyAnIC1mMSkNCnNsZWVwIDEwDQpzeXN0ZW1jdGwgc3RhcnQgbXlzcWxkDQpzeXN0ZW1jdGwgaXMtYWN0aXZlIG15c3FsZCAmJiBlY2hvICJteXNxbGQgaXMgYWN0aXZlIiB8fCBlY2hvICJteXNxbGQgaXMgbm90IGFjdGl2ZSINCm15c3FsIC11IHJvb3QgLXAkREJfUk9PVF9QQVNTV09SRCAtZSAiQ1JFQVRFIFVTRVIgJ3Jvb3QnQCclJyBJREVOVElGSUVEIEJZICckREJfUk9PVF9QQVNTV09SRCc7IEZMVVNIIFBSSVZJTEVHRVM7Ig0KbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIC1lICJHUkFOVCBBTEwgT04gKi4qIFRPICdyb290J0AnJScgV0lUSCBHUkFOVCBPUFRJT047IEZMVVNIIFBSSVZJTEVHRVM7Ig0KbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIC1EIG15c3FsIC1lICJTRUxFQ1QgSG9zdCwgVXNlciBGUk9NIHVzZXIgV0hFUkUgdXNlciA9ICdyb290JzsiID4gfi9kYl91c2Vycw0KY2F0IH4vZGJfdXNlcnMNCg0KYXdzIHMzIGNwIHMzOi8veWEtY2YtdGVtcGxhdGVzL3BocGFwcC9pbml0LnNxbCB+Lw0KbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIDwgfi9pbml0LnNxbA0KZGF0ZQ0KDQpybSAtZiB+Ly5hd3MvDQpoaXN0b3J5IC1jDQo="
    tags = {
        Name = "prodAppDB"
    }
}

resource "aws_instance" "EC2Instance5" {
    ami = "ami-024f768332f080c5e"
    instance_type = "t2.micro"
    key_name = "aws-web"
    availability_zone = "eu-central-1a"
    tenancy = "default"
    subnet_id = "subnet-0d78267ed6b30f68b"
    ebs_optimized = false
    vpc_security_group_ids = [
        "${aws_security_group.EC2SecurityGroup.id}",
        "${aws_security_group.EC2SecurityGroup6.id}"
    ]
    source_dest_check = true
    root_block_device {
        volume_size = 8
        volume_type = "gp3"
        delete_on_termination = true
    }
    user_data = "IyEvYmluL2Jhc2gKIyBjYXQgL3Zhci9sb2cvY2xvdWQtaW5pdC1vdXRwdXQubG9nCiMgYXdzIGVjMiBkZXNjcmliZS1pbnN0YW5jZS1hdHRyaWJ1dGUgLS1pbnN0YW5jZS1pZCBpLTA5Mjk1NDI5OWE5YzdkMTc4IC0tYXR0cmlidXRlIHVzZXJEYXRhIC0tb3V0cHV0IHRleHQgLS1xdWVyeSAiVXNlckRhdGEuVmFsdWUiIHwgYmFzZTY0IC0tZGVjb2RlCnNldCArZQpkYXRlCmRuZiB1cGRhdGUgLXkKCiMgSW5zdGFsbCBBV1MgQ0xJCmRuZiBpbnN0YWxsIC15IGF3cy1jbGkKQVBQPXBocGFwcApBQ0NFU1NfS0VZPUFLSUFaR1BPUVNPTE1EUkJSUlJLClNFQ1JFVF9LRVk9cnhyWkFtZGlQdTNPYnpORUhVeXZERTBUNTI3VkhwQ3FJYXdZdGNEQgpSRUdJT049ZXUtY2VudHJhbC0xCmF3cyBjb25maWd1cmUgc2V0IGF3c19hY2Nlc3Nfa2V5X2lkICRBQ0NFU1NfS0VZCmF3cyBjb25maWd1cmUgc2V0IGF3c19zZWNyZXRfYWNjZXNzX2tleSAkU0VDUkVUX0tFWQphd3MgY29uZmlndXJlIHNldCByZWdpb24gJFJFR0lPTgphd3MgY29uZmlndXJlIHNldCBvdXRwdXQganNvbgphd3MgY29uZmlndXJlIGxpc3QKYXdzIHN0cyBnZXQtY2FsbGVyLWlkZW50aXR5CgplY2hvICJBUFA6ICRBUFAiCmF3cyBzMyBscwphd3MgczMgY3AgczM6Ly95YS1jZi10ZW1wbGF0ZXMvcGhwYXBwLy5lbnYgfi8KCiMgRXhwb3J0IGVudiB2YXJzCmV4cG9ydCAkKGdyZXAgLXYgJ14jJyB+Ly5lbnYgfCB4YXJncykKREJfSE9TVD0kKGlwIC1iciBhIHwgZ3JlcCAtdiBsbyB8IHhhcmdzIHwgY3V0IC1kJyAnIC1mMyB8IGN1dCAtZCcvJyAtZjEpCnNlZCAtaSAnL0RCX0hPU1QvZCcgfi8uZW52CmVjaG8gIkRCX0hPU1Q9JERCX0hPU1QiID4+IH4vLmVudgphd3MgczMgY3Agfi8uZW52IHMzOi8veWEtY2YtdGVtcGxhdGVzL3BocGFwcC8Kcm0gfi8uZW52CgojIERCX05BTUU9JChhd3MgY2xvdWRmb3JtYXRpb24gZGVzY3JpYmUtc3RhY2tzIC0tc3RhY2stbmFtZSAkQVBQIC0tcXVlcnkgIlN0YWNrc1swXS5PdXRwdXRzWz9PdXRwdXRLZXk9PSdEYXRhYmFzZU5hbWUnXS5PdXRwdXRWYWx1ZSIgLS1vdXRwdXQgdGV4dCkKIyBEQl9ST09UX1BBU1NXT1JEPSQoYXdzIGNsb3VkZm9ybWF0aW9uIGRlc2NyaWJlLXN0YWNrcyAtLXN0YWNrLW5hbWUgJEFQUCAtLXF1ZXJ5ICJTdGFja3NbMF0uT3V0cHV0c1s/T3V0cHV0S2V5PT0nREJSb290UGFzc3dvcmQnXS5PdXRwdXRWYWx1ZSIgLS1vdXRwdXQgdGV4dCkKIyBEQl9VU0VSPSQoYXdzIGNsb3VkZm9ybWF0aW9uIGRlc2NyaWJlLXN0YWNrcyAtLXN0YWNrLW5hbWUgJEFQUCAtLXF1ZXJ5ICJTdGFja3NbMF0uT3V0cHV0c1s/T3V0cHV0S2V5PT0nREJVc2VybmFtZSddLk91dHB1dFZhbHVlIiAtLW91dHB1dCB0ZXh0KQojIERCX1BBU1NXT1JEPSQoYXdzIGNsb3VkZm9ybWF0aW9uIGRlc2NyaWJlLXN0YWNrcyAtLXN0YWNrLW5hbWUgJEFQUCAtLXF1ZXJ5ICJTdGFja3NbMF0uT3V0cHV0c1s/T3V0cHV0S2V5PT0nREJQYXNzd29yZCddLk91dHB1dFZhbHVlIiAtLW91dHB1dCB0ZXh0KQoKZWNobyAiREJfTkFNRTogJERCX05BTUUiCmVjaG8gIkRCX1JPT1RfUEFTU1dPUkQ6ICREQl9ST09UX1BBU1NXT1JEIgplY2hvICJEQl9IT1NUOiAkREJfSE9TVCIKCiMgSW5zdGFsbCBNeVNRTAp3Z2V0IGh0dHBzOi8vZGV2Lm15c3FsLmNvbS9nZXQvbXlzcWw4MC1jb21tdW5pdHktcmVsZWFzZS1lbDktNS5ub2FyY2gucnBtCmRuZiBpbnN0YWxsIG15c3FsODAtY29tbXVuaXR5LXJlbGVhc2UtZWw5LTUubm9hcmNoLnJwbSAteQpkbmYgaW5zdGFsbCBteXNxbC1jb21tdW5pdHktc2VydmVyIC15CgojIFNldCBNeVNRTCBlbnZpcm9ubWVudCB2YXJpYWJsZXMKIyBNWVNRTF9ST09UX1BBU1NXT1JEPSREQl9ST09UX1BBU1NXT1JECiMgTVlTUUxfREFUQUJBU0U9JERCX05BTUUKIyBNWVNRTF9VU0VSPSREQl9VU0VSCiMgTVlTUUxfUEFTU1dPUkQ9JERCX1BBU1NXT1JECgojIFN0YXJ0IE15U1FMIHNlcnZpY2UKc3lzdGVtY3RsIGVuYWJsZSAtLW5vdyBteXNxbGQKc3lzdGVtY3RsIGlzLWVuYWJsZWQgaHR0cGQgJiYgZWNobyAibXlzcWxkIGlzIGVuYWJsZWQiIHx8IGVjaG8gIm15c3FsZCBpcyBub3QgZGlzYWJsZWQiCnN5c3RlbWN0bCBzdGF0dXMgbXlzcWxkCnN5c3RlbWN0bCBzdG9wIG15c3FsZApzeXN0ZW1jdGwgc3RhdHVzIG15c3FsZAojIFNldCBtYXN0ZXIgcGFzc3dvcmQKc3VkbyAtdSBteXNxbCAvdXNyL3NiaW4vbXlzcWxkIC0tc2tpcC1ncmFudC10YWJsZXMgLS1za2lwX25ldHdvcmtpbmcgJgpzbGVlcCA1Cm15c3FsIC11IHJvb3QgLS1za2lwLXBhc3N3b3JkIC1lICJGTFVTSCBQUklWSUxFR0VTOyBBTFRFUiBVU0VSICdyb290J0AnbG9jYWxob3N0JyBJREVOVElGSUVEIEJZICckREJfUk9PVF9QQVNTV09SRCc7IgpraWxsICQocHMgeGEgfCBncmVwICJzdWRvIC11IG15c3FsIiB8IGhlYWQgLW4gMSB8IHhhcmdzIHwgY3V0IC1kJyAnIC1mMSkKc2xlZXAgMTAKc3lzdGVtY3RsIHN0YXJ0IG15c3FsZApzeXN0ZW1jdGwgaXMtYWN0aXZlIG15c3FsZCAmJiBlY2hvICJteXNxbGQgaXMgYWN0aXZlIiB8fCBlY2hvICJteXNxbGQgaXMgbm90IGFjdGl2ZSIKIyBERUZBVUxUX01ZU1FMX1JPT1RfUEFTU1dPUkQ9JChncmVwICJ0ZW1wb3JhcnkiIC92YXIvbG9nL215c3FsZC5sb2cgfCB4YXJncyAtbiAxIHwgdGFjIHwgeGFyZ3MgfCBjdXQgLWQnICcgLWYxKQojIG15c3FsIC11IHJvb3QgLXAkREVGQVVMVF9NWVNRTF9ST09UX1BBU1NXT1JEIC0tY29ubmVjdC1leHBpcmVkLXBhc3N3b3JkIFwKIyAgIC1lICJBTFRFUiBVU0VSICdyb290J0AnbG9jYWxob3N0JyBJREVOVElGSUVEIEJZICckREJfUk9PVF9QQVNTV09SRCc7IEZMVVNIIFBSSVZJTEVHRVM7IgpteXNxbCAtdSByb290IC1wJERCX1JPT1RfUEFTU1dPUkQgLWUgIkNSRUFURSBVU0VSICdyb290J0AnJScgSURFTlRJRklFRCBCWSAnJERCX1JPT1RfUEFTU1dPUkQnOyBGTFVTSCBQUklWSUxFR0VTOyIKbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIC1lICJHUkFOVCBBTEwgT04gKi4qIFRPICdyb290J0AnJScgV0lUSCBHUkFOVCBPUFRJT047IEZMVVNIIFBSSVZJTEVHRVM7IgpteXNxbCAtdSByb290IC1wJERCX1JPT1RfUEFTU1dPUkQgLUQgbXlzcWwgLWUgIlNFTEVDVCBIb3N0LCBVc2VyIEZST00gdXNlciBXSEVSRSB1c2VyID0gJ3Jvb3QnOyIgPiB+L2RiX3VzZXJzCmNhdCB+L2RiX3VzZXJzCgojIENyZWF0ZSBkYXRhYmFzZSBhbmQgdXNlcgojIG15c3FsIC11IHJvb3QgLXAkREJfUk9PVF9QQVNTV09SRCAtZSAiQ1JFQVRFIERBVEFCQVNFICREQl9OQU1FIENIQVJBQ1RFUiBTRVQgdXRmOG1iNCBDT0xMQVRFIHV0ZjhtYjRfdW5pY29kZV9jaTsiCiMgbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIC1lICJDUkVBVEUgVVNFUiAnJERCX1VTRVInQCclJyBJREVOVElGSUVEIEJZICckREJfUEFTU1dPUkQnOyIKIyBteXNxbCAtdSByb290IC1wJERCX1JPT1RfUEFTU1dPUkQgLWUgIkdSQU5UIEFMTCBQUklWSUxFR0VTIE9OICREQl9OQU1FLiogVE8gJyREQl9VU0VSJ0AnJSc7IgojIG15c3FsIC11IHJvb3QgLXAkREJfUk9PVF9QQVNTV09SRCAtZSAiRkxVU0ggUFJJVklMRUdFUzsiCmF3cyBzMyBjcCBzMzovL3lhLWNmLXRlbXBsYXRlcy9waHBhcHAvaW5pdC5zcWwgfi8KbXlzcWwgLXUgcm9vdCAtcCREQl9ST09UX1BBU1NXT1JEIDwgfi9pbml0LnNxbApkYXRlCgojIFJlbW92ZSBpbml0IGNvbmZpZ3VyYXRpb24KIyBybSAtZiB+Ly5hd3MvKgojIGhpc3RvcnkgLWMK"
    tags = {
        Name = "prodAppDB"
    }
}

resource "aws_instance" "EC2Instance6" {
    ami = "ami-024f768332f080c5e"
    instance_type = "t2.micro"
    key_name = "aws-web"
    availability_zone = "eu-central-1b"
    tenancy = "default"
    subnet_id = "subnet-01bfbea3638066304"
    ebs_optimized = false
    vpc_security_group_ids = [
        "${aws_security_group.EC2SecurityGroup3.id}",
        "${aws_security_group.EC2SecurityGroup.id}"
    ]
    source_dest_check = true
    root_block_device {
        volume_size = 8
        volume_type = "gp3"
        delete_on_termination = true
    }
    user_data = "IyEvYmluL2Jhc2gKc2V0ICtlCmRuZiB1cGRhdGUgLXkKZG5mIC15IGluc3RhbGwgLXkgaHR0cGQgbW9kX3NzbCBwaHA4LjIgcGhwOC4yLW15c3FsbmQgbWFyaWFkYjEwNQpzeXN0ZW1jdGwgZW5hYmxlIC0tbm93IGh0dHBkCmNkIC92YXIvd3d3L2h0bWwKCmVjaG8gIjxoMT5IZWxsbyBmcm9tIFNlcnZlcjwvaDE+IiA+IGluZGV4Lmh0bWwKIyBlY2hvICI8aDE+SGVsbG8gZnJvbSBTZXJ2ZXI8L2gxPiIgPiBpbmRleC5waHAKIyBlY2hvICI8P3BocCAKIyAgIFwkcHVibGljSVAxID0gZ2V0SG9zdEJ5TmFtZShcJF9TRVJWRVJbJ1NFUlZFUl9BRERSJ10pOwojICAgXCRwdWJsaWNJUDIgPSBnZXRIb3N0QnlOYW1lKFwkX1NFUlZFUlsnU0VSVkVSX05BTUUnXSk7CiMgICBlY2hvICc8cD4nIC4gXCRwdWJsaWNJUDEgLiAnPC9wPic7IAojICAgZWNobyAnPHA+JyAuIFwkcHVibGljSVAyIC4gJzwvcD4nOyAKIyA/PiIgPj4gaW5kZXgucGhwCgojIENyZWF0ZSBTU0wgY2VydGlmaWNhdGUKbWtkaXIgL2V0Yy9odHRwZC9zc2wgICAgICAgICAgICAgIApvcGVuc3NsIHJlcSAteDUwOSAtbm9kZXMgLWRheXMgMzY1IC1uZXdrZXkgcnNhOjIwNDggXAogIC1rZXlvdXQgL2V0Yy9odHRwZC9zc2wvYXBhY2hlLmtleSAtb3V0IC9ldGMvaHR0cGQvc3NsL2FwYWNoZS5jcnQgXAogIC1zdWJqICIvQz1VUy9TVD1TdGF0ZS9MPUNpdHkvTz1Pcmdhbml6YXRpb24vT1U9VW5pdC9DTj1hcHAua29seWFkYS5saW5rIgoKIyBDb25maWd1cmUgU1NMIGZvciBodHRwZApjZCAvZXRjL2h0dHBkL2NvbmYuZC8KY3Agc3NsLmNvbmYgc3NsLmNvbmYub3JpZwpzZWQgLWkgJy9MaXN0ZW4vZCcgc3NsLmNvbmYKc2VkIC1pICcvU1NMRW5naW5lL2QnIHNzbC5jb25mCnNlZCAtaSAnL1NTTENlcnRpZmljYXRlRmlsZS9kJyBzc2wuY29uZgpzZWQgLWkgJy9TU0xDZXJ0aWZpY2F0ZUtleUZpbGUvZCcgc3NsLmNvbmYKc2VkIC1pICcxIGlcTGlzdGVuIDQ0MyBodHRwcycgc3NsLmNvbmYKc2VkIC1pICcvPFZpcnR1YWxIb3N0IF9kZWZhdWx0Xzo0NDM+L2EgXApTU0xFbmdpbmUgb24gXApTU0xDZXJ0aWZpY2F0ZUZpbGUgL2V0Yy9odHRwZC9zc2wvYXBhY2hlLmNydCBcClNTTENlcnRpZmljYXRlS2V5RmlsZSAvZXRjL2h0dHBkL3NzbC9hcGFjaGUua2V5JyBzc2wuY29uZgpzeXN0ZW1jdGwgcmVzdGFydCBodHRwZAoKIyBJbnN0YWxsIEFXUyBDTEkgICAgICAgICAgICAgIApkbmYgaW5zdGFsbCAteSBhd3MtY2xpCmNkIC92YXIvd3d3L2h0bWwKQVBQPXBocGFwcApBQ0NFU1NfS0VZPUFLSUFaR1BPUVNPTE1EUkJSUlJLClNFQ1JFVF9LRVk9cnhyWkFtZGlQdTNPYnpORUhVeXZERTBUNTI3VkhwQ3FJYXdZdGNEQgpSRUdJT049ZXUtY2VudHJhbC0xCmF3cyBjb25maWd1cmUgc2V0IGF3c19hY2Nlc3Nfa2V5X2lkICRBQ0NFU1NfS0VZCmF3cyBjb25maWd1cmUgc2V0IGF3c19zZWNyZXRfYWNjZXNzX2tleSAkU0VDUkVUX0tFWQphd3MgY29uZmlndXJlIHNldCByZWdpb24gJFJFR0lPTgphd3MgY29uZmlndXJlIHNldCBvdXRwdXQganNvbgoKIyAgVXNlIHRoZSBFbmRwb2ludCBmb3IgdGhlIG15c3FsIGNvbm5lY3Rpb24KIyBEQl9IT1NUPSQoYXdzIGNsb3VkZm9ybWF0aW9uIGRlc2NyaWJlLXN0YWNrcyAtLXN0YWNrLW5hbWUgJEFQUCAtLXF1ZXJ5ICJTdGFja3NbMF0uT3V0cHV0c1s/T3V0cHV0S2V5PT0nRGF0YWJhc2VFbmRwb2ludCddLk91dHB1dFZhbHVlIiAtLW91dHB1dCB0ZXh0KQphd3MgczMgY3AgczM6Ly95YS1jZi10ZW1wbGF0ZXMvcGhwYXBwLy5lbnYgfi8KZXhwb3J0ICQoZ3JlcCBEQl9IT1NUIH4vLmVudiB8IHhhcmdzKQplY2hvICJEQl9IT1NUOiAkREJfSE9TVCIKcm0gfi8uZW52CgojIENvcHkgQXBwbGljYXRpb24KY2QgL3Zhci93d3cvaHRtbAphd3MgczMgY3AgczM6Ly95YS1jZi10ZW1wbGF0ZXMvcGhwYXBwLyAuIC0tcmVjdXJzaXZlIC0tZXhjbHVkZSAqLnNxbApzZWQgLWkgInMvZGF0YWJhc2UuY2l1aHMzaHNmc3FqLmV1LWNlbnRyYWwtMS5yZHMuYW1hem9uYXdzLmNvbS8kREJfSE9TVC9nIiAuL2RiLnBocAoKIyBSZW1vdmUgaW5pdCBjb25maWd1cmF0aW9uCiMgcm0gLWYgfi8uYXdzLyoKIyBoaXN0b3J5IC1jCg=="
    tags = {
        Name = "prodApp"
    }
}

resource "aws_instance" "EC2Instance7" {
    ami = "ami-01cf633da373ae6f3"
    instance_type = "t2.micro"
    key_name = "aws-web"
    availability_zone = "eu-central-1a"
    tenancy = "default"
    subnet_id = "subnet-01308adc595b1ccb8"
    ebs_optimized = false
    vpc_security_group_ids = [
        "${aws_security_group.EC2SecurityGroup4.id}"
    ]
    source_dest_check = true
    root_block_device {
        volume_size = 10
        volume_type = "gp2"
        delete_on_termination = true
    }
    tags = {
        Name = "prod-devopskb"
    }
}

resource "aws_instance" "EC2Instance8" {
    ami = "ami-024f768332f080c5e"
    instance_type = "t2.micro"
    key_name = "aws-web"
    availability_zone = "eu-central-1a"
    tenancy = "default"
    subnet_id = "subnet-0c9a74482ef54c66c"
    ebs_optimized = false
    vpc_security_group_ids = [
        "${aws_security_group.EC2SecurityGroup2.id}"
    ]
    source_dest_check = true
    root_block_device {
        volume_size = 8
        volume_type = "gp3"
        delete_on_termination = true
    }
    user_data = "IyEvYmluL2Jhc2gKZG5mIHVwZGF0ZSAteQpkbmYgaW5zdGFsbCAteSBtYXJpYWRiMTA1Cg=="
    tags = {
        Name = "AppBastion"
    }
}

resource "aws_lb" "ElasticLoadBalancingV2LoadBalancer" {
    name = "prodELB"
    internal = false
    load_balancer_type = "application"
    subnets = [
        "subnet-0002439b41edf1d9c",
        "subnet-0c9a74482ef54c66c"
    ]
    security_groups = [
        "${aws_security_group.EC2SecurityGroup3.id}"
    ]
    ip_address_type = "ipv4"
    access_logs {
        enabled = false
        bucket = ""
        prefix = ""
    }
    idle_timeout = "60"
    enable_deletion_protection = "false"
    enable_http2 = "true"
    enable_cross_zone_load_balancing = "true"
}

resource "aws_lb_listener" "ElasticLoadBalancingV2Listener" {
    load_balancer_arn = "arn:aws:elasticloadbalancing:eu-central-1:632397271958:loadbalancer/app/prodELB/7cfe70022f559dd4"
    port = 80
    protocol = "HTTP"
    default_action {
        redirect {
            host = "#{host}"
            path = "/#{path}"
            port = "443"
            protocol = "HTTPS"
            query = "#{query}"
            status_code = "HTTP_301"
        }
        type = "redirect"
    }
}

resource "aws_lb_listener" "ElasticLoadBalancingV2Listener2" {
    load_balancer_arn = "arn:aws:elasticloadbalancing:eu-central-1:632397271958:loadbalancer/app/prodELB/7cfe70022f559dd4"
    port = 443
    protocol = "HTTPS"
    ssl_policy = "ELBSecurityPolicy-2016-08"
    certificate_arn = "arn:aws:acm:eu-central-1:632397271958:certificate/cc31de15-ebf9-4272-a08d-dbf22bcafead"
    default_action {
        target_group_arn = "arn:aws:elasticloadbalancing:eu-central-1:632397271958:targetgroup/prodTargetGroup/ebd39bd2b3157594"
        type = "forward"
    }
}

resource "aws_autoscaling_group" "AutoScalingAutoScalingGroup" {
    name = "prodAutoScalingGroup"
    launch_template {
        id = "lt-0cdd1d09ee713ddbf"
        name = "prodAppLaunchTemplate"
        version = "1"
    }
    min_size = 1
    max_size = 3
    desired_capacity = 1
    default_cooldown = 300
    availability_zones = [
        "eu-central-1a",
        "eu-central-1b"
    ]
    target_group_arns = [
        "arn:aws:elasticloadbalancing:eu-central-1:632397271958:targetgroup/prodTargetGroup/ebd39bd2b3157594"
    ]
    health_check_type = "EC2"
    health_check_grace_period = 0
    vpc_zone_identifier = [
        "subnet-01bfbea3638066304",
        "subnet-0d78267ed6b30f68b"
    ]
    termination_policies = [
        "Default"
    ]
    service_linked_role_arn = "arn:aws:iam::632397271958:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
    tag {
        key = "Name"
        value = "prodApp"
        propagate_at_launch = true
    }
}

resource "aws_autoscaling_policy" "AutoScalingScalingPolicy" {
    autoscaling_group_name = "prodAutoScalingGroup"
    policy_type = "SimpleScaling"
    adjustment_type = "ChangeInCapacity"
    scaling_adjustment = -1
    cooldown = 60
}

resource "aws_autoscaling_policy" "AutoScalingScalingPolicy2" {
    autoscaling_group_name = "prodAutoScalingGroup"
    policy_type = "SimpleScaling"
    adjustment_type = "ChangeInCapacity"
    scaling_adjustment = 1
    cooldown = 60
}

resource "aws_security_group" "EC2SecurityGroup" {
    description = "Security Group for Private subnets"
    name = "web-PrivateSG-192JG8EC0PR1U"
    tags = {
        Name = "PrivateSG"
    }
    vpc_id = "${aws_vpc.EC2VPC.id}"
    ingress {
        security_groups = [
            "${aws_security_group.EC2SecurityGroup2.id}"
        ]
        from_port = 22
        protocol = "tcp"
        to_port = 22
    }
    egress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 0
        protocol = "-1"
        to_port = 0
    }
}

resource "aws_security_group" "EC2SecurityGroup2" {
    description = "Security Group for private network"
    name = "web-BastionSG-DMGBS883L7E9"
    tags = {
        Name = "BastionSG"
    }
    vpc_id = "${aws_vpc.EC2VPC.id}"
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 22
        protocol = "tcp"
        to_port = 22
    }
    egress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 0
        protocol = "-1"
        to_port = 0
    }
}

resource "aws_security_group" "EC2SecurityGroup3" {
    description = "Security group with HTTP ingress rule"
    name = "prodSecurityGroup"
    tags = {
        Name = "WebSG"
    }
    vpc_id = "${aws_vpc.EC2VPC.id}"
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        description = "Allow HTTP"
        from_port = 80
        protocol = "tcp"
        to_port = 80
    }
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        description = "Allow HTTPS"
        from_port = 443
        protocol = "tcp"
        to_port = 443
    }
    egress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 0
        protocol = "-1"
        to_port = 0
    }
}

resource "aws_security_group" "EC2SecurityGroup4" {
    description = "Allow inbound traffic"
    name = "sec-group-prod-devopskb}"
    tags = {
        Name = "prod prod-devopskb sec-group"
    }
    vpc_id = "vpc-01db4d4bd05d66d6a"
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 80
        protocol = "tcp"
        to_port = 80
    }
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 8080
        protocol = "tcp"
        to_port = 8080
    }
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 22
        protocol = "tcp"
        to_port = 22
    }
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 443
        protocol = "tcp"
        to_port = 443
    }
    egress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 0
        protocol = "-1"
        to_port = 0
    }
}

resource "aws_security_group" "EC2SecurityGroup5" {
    description = "This security group was generated by AWS Marketplace and is based on recommended settings for CentOS Stream 8 (x86_64) version CS8-20220920 provided by Amazon Web Services"
    name = "sec-group-web-server-01"
    tags = {
        Name = "WebDMZ"
    }
    vpc_id = "vpc-01db4d4bd05d66d6a"
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 80
        protocol = "tcp"
        to_port = 80
    }
    ingress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 22
        protocol = "tcp"
        to_port = 22
    }
    egress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 0
        protocol = "-1"
        to_port = 0
    }
}

resource "aws_security_group" "EC2SecurityGroup6" {
    description = "Database security group"
    name = "web-DatabaseSG-OH0433JT34K6"
    tags = {
        Name = "DatabaseSG"
    }
    vpc_id = "${aws_vpc.EC2VPC.id}"
    ingress {
        security_groups = [
            "${aws_security_group.EC2SecurityGroup3.id}"
        ]
        from_port = 3306
        protocol = "tcp"
        to_port = 3306
    }
    ingress {
        security_groups = [
            "${aws_security_group.EC2SecurityGroup2.id}"
        ]
        from_port = 3306
        protocol = "tcp"
        to_port = 3306
    }
    egress {
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        from_port = 0
        protocol = "-1"
        to_port = 0
    }
}

resource "aws_launch_template" "EC2LaunchTemplate" {
    name = "prodAppLaunchTemplate"
    user_data = "IyEvYmluL2Jhc2gKc2V0ICtlCmRuZiB1cGRhdGUgLXkKZG5mIC15IGluc3RhbGwgLXkgaHR0cGQgbW9kX3NzbCBwaHA4LjIgcGhwOC4yLW15c3FsbmQgbWFyaWFkYjEwNQpzeXN0ZW1jdGwgZW5hYmxlIC0tbm93IGh0dHBkCmNkIC92YXIvd3d3L2h0bWwKCmVjaG8gIjxoMT5IZWxsbyBmcm9tIFNlcnZlcjwvaDE+IiA+IGluZGV4Lmh0bWwKIyBlY2hvICI8aDE+SGVsbG8gZnJvbSBTZXJ2ZXI8L2gxPiIgPiBpbmRleC5waHAKIyBlY2hvICI8P3BocCAKIyAgIFwkcHVibGljSVAxID0gZ2V0SG9zdEJ5TmFtZShcJF9TRVJWRVJbJ1NFUlZFUl9BRERSJ10pOwojICAgXCRwdWJsaWNJUDIgPSBnZXRIb3N0QnlOYW1lKFwkX1NFUlZFUlsnU0VSVkVSX05BTUUnXSk7CiMgICBlY2hvICc8cD4nIC4gXCRwdWJsaWNJUDEgLiAnPC9wPic7IAojICAgZWNobyAnPHA+JyAuIFwkcHVibGljSVAyIC4gJzwvcD4nOyAKIyA/PiIgPj4gaW5kZXgucGhwCgojIENyZWF0ZSBTU0wgY2VydGlmaWNhdGUKbWtkaXIgL2V0Yy9odHRwZC9zc2wgICAgICAgICAgICAgIApvcGVuc3NsIHJlcSAteDUwOSAtbm9kZXMgLWRheXMgMzY1IC1uZXdrZXkgcnNhOjIwNDggXAogIC1rZXlvdXQgL2V0Yy9odHRwZC9zc2wvYXBhY2hlLmtleSAtb3V0IC9ldGMvaHR0cGQvc3NsL2FwYWNoZS5jcnQgXAogIC1zdWJqICIvQz1VUy9TVD1TdGF0ZS9MPUNpdHkvTz1Pcmdhbml6YXRpb24vT1U9VW5pdC9DTj1hcHAua29seWFkYS5saW5rIgoKIyBDb25maWd1cmUgU1NMIGZvciBodHRwZApjZCAvZXRjL2h0dHBkL2NvbmYuZC8KY3Agc3NsLmNvbmYgc3NsLmNvbmYub3JpZwpzZWQgLWkgJy9MaXN0ZW4vZCcgc3NsLmNvbmYKc2VkIC1pICcvU1NMRW5naW5lL2QnIHNzbC5jb25mCnNlZCAtaSAnL1NTTENlcnRpZmljYXRlRmlsZS9kJyBzc2wuY29uZgpzZWQgLWkgJy9TU0xDZXJ0aWZpY2F0ZUtleUZpbGUvZCcgc3NsLmNvbmYKc2VkIC1pICcxIGlcTGlzdGVuIDQ0MyBodHRwcycgc3NsLmNvbmYKc2VkIC1pICcvPFZpcnR1YWxIb3N0IF9kZWZhdWx0Xzo0NDM+L2EgXApTU0xFbmdpbmUgb24gXApTU0xDZXJ0aWZpY2F0ZUZpbGUgL2V0Yy9odHRwZC9zc2wvYXBhY2hlLmNydCBcClNTTENlcnRpZmljYXRlS2V5RmlsZSAvZXRjL2h0dHBkL3NzbC9hcGFjaGUua2V5JyBzc2wuY29uZgpzeXN0ZW1jdGwgcmVzdGFydCBodHRwZAoKIyBJbnN0YWxsIEFXUyBDTEkgICAgICAgICAgICAgIApkbmYgaW5zdGFsbCAteSBhd3MtY2xpCmNkIC92YXIvd3d3L2h0bWwKQVBQPXBocGFwcApBQ0NFU1NfS0VZPUFLSUFaR1BPUVNPTE1EUkJSUlJLClNFQ1JFVF9LRVk9cnhyWkFtZGlQdTNPYnpORUhVeXZERTBUNTI3VkhwQ3FJYXdZdGNEQgpSRUdJT049ZXUtY2VudHJhbC0xCmF3cyBjb25maWd1cmUgc2V0IGF3c19hY2Nlc3Nfa2V5X2lkICRBQ0NFU1NfS0VZCmF3cyBjb25maWd1cmUgc2V0IGF3c19zZWNyZXRfYWNjZXNzX2tleSAkU0VDUkVUX0tFWQphd3MgY29uZmlndXJlIHNldCByZWdpb24gJFJFR0lPTgphd3MgY29uZmlndXJlIHNldCBvdXRwdXQganNvbgoKIyAgVXNlIHRoZSBFbmRwb2ludCBmb3IgdGhlIG15c3FsIGNvbm5lY3Rpb24KIyBEQl9IT1NUPSQoYXdzIGNsb3VkZm9ybWF0aW9uIGRlc2NyaWJlLXN0YWNrcyAtLXN0YWNrLW5hbWUgJEFQUCAtLXF1ZXJ5ICJTdGFja3NbMF0uT3V0cHV0c1s/T3V0cHV0S2V5PT0nRGF0YWJhc2VFbmRwb2ludCddLk91dHB1dFZhbHVlIiAtLW91dHB1dCB0ZXh0KQphd3MgczMgY3AgczM6Ly95YS1jZi10ZW1wbGF0ZXMvcGhwYXBwLy5lbnYgfi8KZXhwb3J0ICQoZ3JlcCBEQl9IT1NUIH4vLmVudiB8IHhhcmdzKQplY2hvICJEQl9IT1NUOiAkREJfSE9TVCIKcm0gfi8uZW52CgojIENvcHkgQXBwbGljYXRpb24KY2QgL3Zhci93d3cvaHRtbAphd3MgczMgY3AgczM6Ly95YS1jZi10ZW1wbGF0ZXMvcGhwYXBwLyAuIC0tcmVjdXJzaXZlIC0tZXhjbHVkZSAqLnNxbApzZWQgLWkgInMvZGF0YWJhc2UuY2l1aHMzaHNmc3FqLmV1LWNlbnRyYWwtMS5yZHMuYW1hem9uYXdzLmNvbS8kREJfSE9TVC9nIiAuL2RiLnBocAoKIyBSZW1vdmUgaW5pdCBjb25maWd1cmF0aW9uCiMgcm0gLWYgfi8uYXdzLyoKIyBoaXN0b3J5IC1jCg=="
    key_name = "aws-web"
    network_interfaces {
        associate_public_ip_address = false
        device_index = 0
        security_groups = [
            "${aws_security_group.EC2SecurityGroup3.id}",
            "${aws_security_group.EC2SecurityGroup.id}"
        ]
    }
    image_id = "ami-024f768332f080c5e"
    instance_type = "t2.micro"
}

resource "aws_launch_template" "EC2LaunchTemplate2" {
    name = "httpd-1"
    tag_specifications {
        resource_type = "instance"
        tags {
            Name = "httpd-1"
        }
    }
    user_data = "IyEvYmluL2Jhc2gKZG5mIHVwZGF0ZSAteQpkbmYgLXkgaW5zdGFsbCBodHRwZApzeXN0ZW1jdGwgZW5hYmxlIC0tbm93IGh0dHBkCmNkIC92YXIvd3d3L2h0bWwKZWNobyAiPGgxPkhlbGxvIGZyb20gU2VydmVyIDE8L2gxPiIgPiBpbmRleC5odG1s"
    vpc_security_group_ids = [
        "${aws_security_group.EC2SecurityGroup5.id}"
    ]
    key_name = "aws-web"
    image_id = "ami-024f768332f080c5e"
    instance_type = "t2.micro"
}

resource "aws_launch_template" "EC2LaunchTemplate3" {
    name = "httpd-2"
    user_data = "IyEvYmluL2Jhc2gKZG5mIHVwZGF0ZSAteQpkbmYgLXkgaW5zdGFsbCBodHRwZApzeXN0ZW1jdGwgZW5hYmxlIC0tbm93IGh0dHBkCmNkIC92YXIvd3d3L2h0bWwKZWNobyAiPGgxPkhlbGxvIGZyb20gU2VydmVyIDI8L2gxPiIgPiBpbmRleC5odG1s"
    key_name = "aws-web"
    network_interfaces {
        device_index = 0
        security_groups = [
            "${aws_security_group.EC2SecurityGroup5.id}"
        ]
        subnet_id = "subnet-066ff6b6535c460dc"
    }
    image_id = "ami-024f768332f080c5e"
    instance_type = "t2.micro"
}

resource "aws_lb_target_group" "ElasticLoadBalancingV2TargetGroup" {
    health_check {
        interval = 30
        path = "/"
        port = "traffic-port"
        protocol = "HTTP"
        timeout = 5
        unhealthy_threshold = 2
        healthy_threshold = 5
        matcher = "200"
    }
    port = 80
    protocol = "HTTP"
    target_type = "instance"
    vpc_id = "${aws_vpc.EC2VPC.id}"
    name = "prodTargetGroup"
}

resource "aws_lb_target_group" "ElasticLoadBalancingV2TargetGroup2" {
    health_check {
        interval = 30
        path = "/index.html"
        port = "traffic-port"
        protocol = "HTTP"
        timeout = 5
        unhealthy_threshold = 2
        healthy_threshold = 5
        matcher = "200"
    }
    port = 80
    protocol = "HTTP"
    target_type = "instance"
    vpc_id = "vpc-01db4d4bd05d66d6a"
    name = "web-target-group"
}

resource "aws_lb_target_group" "ElasticLoadBalancingV2TargetGroup3" {
    health_check {
        interval = 30
        path = "/index.html"
        port = "traffic-port"
        protocol = "HTTP"
        timeout = 5
        unhealthy_threshold = 2
        healthy_threshold = 5
        matcher = "200"
    }
    port = 80
    protocol = "HTTP"
    target_type = "ip"
    vpc_id = "vpc-01db4d4bd05d66d6a"
    name = "web-target-group-ip"
}

resource "aws_ebs_volume" "EC2Volume" {
    availability_zone = "eu-central-1b"
    encrypted = false
    size = 8
    type = "gp3"
    snapshot_id = "snap-00099e2296a49daa3"
    tags = {}
}

resource "aws_ebs_volume" "EC2Volume2" {
    availability_zone = "eu-central-1a"
    encrypted = false
    size = 10
    type = "gp2"
    snapshot_id = "snap-0e88748fb950b8682"
    tags = {}
}

resource "aws_ebs_volume" "EC2Volume3" {
    availability_zone = "eu-central-1a"
    encrypted = false
    size = 8
    type = "gp3"
    snapshot_id = "snap-00099e2296a49daa3"
    tags = {}
}

resource "aws_ebs_volume" "EC2Volume4" {
    availability_zone = "eu-central-1a"
    encrypted = false
    size = 8
    type = "gp3"
    snapshot_id = "snap-00099e2296a49daa3"
    tags = {}
}

resource "aws_volume_attachment" "EC2VolumeAttachment" {
    volume_id = "vol-08c4dc7b804ad91fd"
    instance_id = "i-0734334730cb1aa09"
    device_name = "/dev/xvda"
}

resource "aws_volume_attachment" "EC2VolumeAttachment2" {
    volume_id = "vol-06a205209ffeec1a8"
    instance_id = "i-062c433be4b96b02d"
    device_name = "/dev/sda1"
}

resource "aws_volume_attachment" "EC2VolumeAttachment3" {
    volume_id = "vol-0becb225c6f47185e"
    instance_id = "i-04ed1c74350556750"
    device_name = "/dev/xvda"
}

resource "aws_volume_attachment" "EC2VolumeAttachment4" {
    volume_id = "vol-0889e326b58de964c"
    instance_id = "i-03a4ab4f026b3f4a6"
    device_name = "/dev/xvda"
}

resource "aws_network_interface" "EC2NetworkInterface" {
    description = ""
    private_ips = [
        "10.10.2.95"
    ]
    subnet_id = "subnet-01bfbea3638066304"
    source_dest_check = true
    security_groups = [
        "${aws_security_group.EC2SecurityGroup3.id}",
        "${aws_security_group.EC2SecurityGroup.id}"
    ]
}

resource "aws_network_interface" "EC2NetworkInterface2" {
    description = "ELB app/prodELB/7cfe70022f559dd4"
    private_ips = [
        "10.10.20.168"
    ]
    subnet_id = "subnet-0002439b41edf1d9c"
    source_dest_check = true
    security_groups = [
        "${aws_security_group.EC2SecurityGroup3.id}"
    ]
}

resource "aws_network_interface" "EC2NetworkInterface3" {
    description = ""
    private_ips = [
        "10.10.1.57"
    ]
    subnet_id = "subnet-0d78267ed6b30f68b"
    source_dest_check = true
    security_groups = [
        "${aws_security_group.EC2SecurityGroup.id}",
        "${aws_security_group.EC2SecurityGroup6.id}"
    ]
}

resource "aws_network_interface" "EC2NetworkInterface4" {
    description = ""
    private_ips = [
        "172.31.16.143"
    ]
    subnet_id = "subnet-01308adc595b1ccb8"
    source_dest_check = true
    security_groups = [
        "${aws_security_group.EC2SecurityGroup4.id}"
    ]
}

resource "aws_network_interface" "EC2NetworkInterface5" {
    description = "ELB app/prodELB/7cfe70022f559dd4"
    private_ips = [
        "10.10.10.57"
    ]
    subnet_id = "subnet-0c9a74482ef54c66c"
    source_dest_check = true
    security_groups = [
        "${aws_security_group.EC2SecurityGroup3.id}"
    ]
}

resource "aws_network_interface" "EC2NetworkInterface6" {
    description = ""
    private_ips = [
        "10.10.10.201"
    ]
    subnet_id = "subnet-0c9a74482ef54c66c"
    source_dest_check = true
    security_groups = [
        "${aws_security_group.EC2SecurityGroup2.id}"
    ]
}

resource "aws_network_interface" "EC2NetworkInterface7" {
    description = "Interface for NAT Gateway nat-0cb43891a35af2685"
    private_ips = [
        "10.10.10.110"
    ]
    subnet_id = "subnet-0c9a74482ef54c66c"
    source_dest_check = false
}

resource "aws_network_interface_attachment" "EC2NetworkInterfaceAttachment" {
    network_interface_id = "eni-0e9a7f5b44e584e6a"
    device_index = 0
    instance_id = "i-0734334730cb1aa09"
}

resource "aws_network_interface_attachment" "EC2NetworkInterfaceAttachment2" {
    network_interface_id = "eni-0deeb8992f49eec64"
    device_index = 0
    instance_id = "i-03a4ab4f026b3f4a6"
}

resource "aws_network_interface_attachment" "EC2NetworkInterfaceAttachment3" {
    network_interface_id = "eni-08a2c4fcb2026f1f7"
    device_index = 0
    instance_id = "i-062c433be4b96b02d"
}

resource "aws_network_interface_attachment" "EC2NetworkInterfaceAttachment4" {
    network_interface_id = "eni-0680714ea5724fae8"
    device_index = 0
    instance_id = "i-04ed1c74350556750"
}

resource "aws_key_pair" "EC2KeyPair" {
    public_key = "REPLACEME"
    key_name = "y-kolyada-key-pair-rsa"
}

resource "aws_key_pair" "EC2KeyPair2" {
    public_key = "REPLACEME"
    key_name = "aws-web"
}

resource "aws_lambda_function" "LambdaFunction" {
    description = ""
    function_name = "ya-lambda-function"
    handler = "index.handler"
    architectures = [
        "x86_64"
    ]
    s3_bucket = "awslambda-eu-cent-1-tasks"
    s3_key = "/snapshots/632397271958/ya-lambda-function-e4fb2528-9f09-4ac2-a353-d0b3cb49d3d8"
    s3_object_version = "4hMItdgYjidtbQPgB4ecFwxVEjU6XhDZ"
    memory_size = 128
    role = "${aws_iam_role.IAMRole4.arn}"
    runtime = "nodejs18.x"
    timeout = 3
    tracing_config {
        mode = "PassThrough"
    }
}

resource "aws_db_subnet_group" "RDSDBSubnetGroup" {
    description = "Created from the RDS Management Console"
    name = "rds-ec2-db-subnet-group-1"
    subnet_ids = [
        "subnet-01c58e41ceafec6a9",
        "subnet-0c52c2efaae6829c6",
        "subnet-08bd243dc5e3cf91f"
    ]
}

resource "aws_opsworks_user_profile" "OpsWorksUserProfile" {
    allow_self_management = false
    user_arn = "arn:aws:iam::632397271958:user/phpapp"
    ssh_username = "phpapp"
}

resource "aws_cloudwatch_metric_alarm" "CloudWatchAlarm" {
    alarm_name = "ALBRequestCountAlarmHigh"
    alarm_description = "Scale out when request count is high"
    actions_enabled = true
    alarm_actions = [
        "arn:aws:autoscaling:eu-central-1:632397271958:scalingPolicy:e01e4474-d509-447f-b9d3-dc80a83c1a5a:autoScalingGroupName/prodAutoScalingGroup:policyName/web-ScaleOutPolicy-nN8hviSV9wZc"
    ]
    metric_name = "RequestCount"
    namespace = "AWS/ApplicationELB"
    statistic = "SampleCount"
    dimensions {
        LoadBalancer = "app/prodELB/7cfe70022f559dd4"
    }
    period = 60
    evaluation_periods = 1
    threshold = 15
    comparison_operator = "GreaterThanOrEqualToThreshold"
}

resource "aws_cloudwatch_metric_alarm" "CloudWatchAlarm2" {
    alarm_name = "ALBRequestCountAlarmLow"
    alarm_description = "Scale in when request count is low"
    actions_enabled = true
    alarm_actions = [
        "arn:aws:autoscaling:eu-central-1:632397271958:scalingPolicy:6c83a251-e225-4a19-ab94-811690763a5f:autoScalingGroupName/prodAutoScalingGroup:policyName/web-ScaleInPolicy-SJ5s9ISSDI2K"
    ]
    metric_name = "RequestCount"
    namespace = "AWS/ApplicationELB"
    statistic = "SampleCount"
    dimensions {
        LoadBalancer = "app/prodELB/7cfe70022f559dd4"
    }
    period = 60
    evaluation_periods = 1
    threshold = 12
    comparison_operator = "LessThanOrEqualToThreshold"
}

resource "aws_cloudwatch_metric_alarm" "CloudWatchAlarm3" {
    alarm_name = "alarm4"
    actions_enabled = true
    metric_name = "RequestCount"
    namespace = "AWS/ApplicationELB"
    statistic = "SampleCount"
    dimensions {
        LoadBalancer = "app/AppALB/8800fddecc324a6d"
    }
    period = 60
    evaluation_periods = 1
    datapoints_to_alarm = 1
    threshold = 2
    comparison_operator = "GreaterThanThreshold"
    treat_missing_data = "missing"
}

resource "aws_cloudwatch_dashboard" "CloudWatchDashboard" {
    dashboard_name = "my-production-system"
    dashboard_body = "{\"widgets\":[{\"type\":\"metric\",\"x\":0,\"y\":0,\"width\":6,\"height\":6,\"properties\":{\"view\":\"timeSeries\",\"stacked\":false,\"metrics\":[[\"AWS/EC2\",\"CPUUtilization\",\"InstanceId\",\"i-0eb3596eafefd5862\"]],\"region\":\"eu-central-1\"}},{\"type\":\"metric\",\"x\":6,\"y\":0,\"width\":6,\"height\":6,\"properties\":{\"view\":\"timeSeries\",\"stacked\":false,\"metrics\":[[\"AWS/EC2\",\"NetworkPacketsIn\",\"InstanceId\",\"i-0eb3596eafefd5862\"]],\"region\":\"eu-central-1\"}},{\"type\":\"metric\",\"x\":12,\"y\":0,\"width\":6,\"height\":6,\"properties\":{\"view\":\"timeSeries\",\"stacked\":false,\"metrics\":[[\"AWS/EC2\",\"NetworkPacketsOut\",\"InstanceId\",\"i-0eb3596eafefd5862\"]],\"region\":\"eu-central-1\"}}]}"
}

resource "aws_cloudwatch_log_group" "LogsLogGroup" {
    name = "/aws/imagebuilder/ya-web-ami-recipe"
}

resource "aws_cloudwatch_log_group" "LogsLogGroup2" {
    name = "/aws/lambda/hello-world-python"
}

resource "aws_cloudwatch_log_group" "LogsLogGroup3" {
    name = "/aws/lambda/ya-lambda-function"
}

resource "aws_cloudwatch_log_group" "LogsLogGroup4" {
    name = "/ecs/first-run-task-definition"
}

resource "aws_cloudwatch_log_group" "LogsLogGroup5" {
    name = "messages"
}

resource "aws_cloudwatch_log_stream" "LogsLogStream" {
    log_group_name = "/aws/imagebuilder/ya-web-ami-recipe"
    name = "1.0.0/1"
}

resource "aws_cloudwatch_log_stream" "LogsLogStream2" {
    log_group_name = "/aws/lambda/hello-world-python"
    name = "2023/01/25/[$LATEST]6764667fdd534af2a23e49be2ba9752b"
}

resource "aws_cloudwatch_log_stream" "LogsLogStream3" {
    log_group_name = "/aws/lambda/ya-lambda-function"
    name = "2023/10/26/[$LATEST]3258db4b398f4c768c683d6fc6341d9f"
}

resource "aws_cloudwatch_log_stream" "LogsLogStream4" {
    log_group_name = "/aws/lambda/ya-lambda-function"
    name = "2023/10/26/[$LATEST]a429725acb6f4debbe9df6c3728d87d0"
}

resource "aws_cloudwatch_log_stream" "LogsLogStream5" {
    log_group_name = "/aws/lambda/ya-lambda-function"
    name = "2023/10/26/[$LATEST]d9c82bb4ca79481c95d9c151fbfc0fd5"
}

resource "aws_cloudwatch_log_stream" "LogsLogStream6" {
    log_group_name = "/ecs/first-run-task-definition"
    name = "ecs/sample-app/e5bd8cf448e94a9ca4a753ce2c9b36dd"
}

resource "aws_cloudwatch_log_stream" "LogsLogStream7" {
    log_group_name = "messages"
    name = "i-0c5a76e32f83bb6a7"
}

resource "aws_cloudwatch_log_metric_filter" "LogsMetricFilter" {
    pattern = "error"
    log_group_name = "/aws/lambda/ya-lambda-function"
    metric_transformation {
        name = "lambda-log-errors"
        namespace = "ya-lambda-errors-namespace"
        value = "1"
        default_value = 0
    }
}

resource "aws_cloudtrail" "CloudTrailTrail" {
    name = "management-events"
    s3_bucket_name = "aws-cloudtrail-logs-632397271958-99b8a708"
    include_global_service_events = true
    is_multi_region_trail = true
    enable_log_file_validation = false
    enable_logging = true
}

resource "aws_ecs_task_definition" "ECSTaskDefinition" {
    container_definitions = "[{\"name\":\"sample-app\",\"image\":\"httpd:2.4\",\"cpu\":256,\"memoryReservation\":512,\"links\":[],\"portMappings\":[{\"containerPort\":80,\"hostPort\":80,\"protocol\":\"tcp\"}],\"essential\":true,\"entryPoint\":[\"sh\",\"-c\"],\"command\":[\"/bin/sh -c \\\"echo '<html> <head> <title>Amazon ECS Sample App</title> <style>body {margin-top: 40px; background-color: #333;} </style> </head><body> <div style=color:white;text-align:center> <h1>Amazon ECS Sample App</h1> <h2>Congratulations!</h2> <p>Your application is now running on a container in Amazon ECS.</p> </div></body></html>' >  /usr/local/apache2/htdocs/index.html && httpd-foreground\\\"\"],\"environment\":[],\"mountPoints\":[],\"volumesFrom\":[],\"logConfiguration\":{\"logDriver\":\"awslogs\",\"options\":{\"awslogs-group\":\"/ecs/first-run-task-definition\",\"awslogs-region\":\"eu-central-1\",\"awslogs-stream-prefix\":\"ecs\"}}}]"
    family = "first-run-task-definition"
    execution_role_arn = "${aws_iam_role.IAMRole2.arn}"
    network_mode = "awsvpc"
    requires_compatibilities = [
        "FARGATE"
    ]
    cpu = "256"
    memory = "512"
}

resource "aws_neptune_subnet_group" "NeptuneDBSubnetGroup" {
    name = "rds-ec2-db-subnet-group-1"
    description = "Created from the RDS Management Console"
    subnet_ids = [
        "subnet-01c58e41ceafec6a9",
        "subnet-0c52c2efaae6829c6",
        "subnet-08bd243dc5e3cf91f"
    ]
}

resource "aws_docdb_subnet_group" "DocDBDBSubnetGroup" {
    name = "rds-ec2-db-subnet-group-1"
    description = "Created from the RDS Management Console"
    subnet_ids = [
        "subnet-01c58e41ceafec6a9",
        "subnet-0c52c2efaae6829c6",
        "subnet-08bd243dc5e3cf91f"
    ]
}

resource "aws_ses_configuration_set" "SESConfigurationSet" {
    name = "my-first-configuration-set"
}

resource "aws_budgets_budget" "BudgetsBudget" {
    limit_amount = "1.0"
    limit_unit = "USD"
    time_unit = "MONTHLY"
    cost_filters {}
    name = "My Zero-Spend Budget"
    cost_types {
        include_support = true
        include_other_subscription = true
        include_tax = true
        include_subscription = true
        use_blended = false
        include_upfront = true
        include_discount = true
        include_credit = false
        include_recurring = true
        use_amortized = false
        include_refund = false
    }
    budget_type = "COST"
}

resource "aws_acm_certificate" "CertificateManagerCertificate" {
    domain_name = "app.kolyada.link"
    subject_alternative_names = [
        "app.kolyada.link"
    ]
    validation_method = "DNS"
}
