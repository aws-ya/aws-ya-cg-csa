| ID | Description |
|:-|:-|
| gp2 | general purpose SSD 2nd gereration, up to 1TB, up to 16000 IOPS, 3 IOPS per GiB, up to 99.9% durability |
| gp3 | general purpose SSD 3nd gereration, 1G - 16TB up to 16000 IOPS, 3 IOPS per GiB, up to 99.9% durability |
| io1 | Provisioned IOPS SSD, up to 64000 IOPS per volume, 50 IOPS per GiB, 99.9% durability |
| io2 | Provisioned IOPS SSD, up to 64000 IOPS per volume, 500 IOPS per GiB, 99.999% durability, 20% cheaper then gp2 |
| io2 Block Express | Provisioned IOPS SSD io2 Block Express, up to 64 TB, up to 256000 IOPS per volume, 99.999% durability, for SAP HANA, Oracle, DB2, MS SQL, SAN in the cloud, up to 99.9% durability |
| io2 | Provisioned IOPS SSD, up to 64000 IOPS per volume, 500 IOPS per GiB, 99.999% durability, price of i02 is equil to i01 |
| sc1 | Cold HDD, 12 - 80 per TB, up to 250 MB/s per volume, lowest cost, can not be a boot, up to 99.9% durability |
| st1 | Throughput Optimized HDD, 40 - 250 per TB, up to 500 MB/s per volume, bit data, data warehouse, can not be a boot, up to 99.9% durability |
| AMI | Amazon Machine Image |
| AZ | Availability Zone |
| DPA | Deployment, Provisioning, and Automations |
| EBS | Elastic Block Store |
| EC2 | Elastic Compute Cloud |
[ ELB ] Elastic Load Balancer |
| ETL | Extract, Transform and Load |
| IaC | Infrastructure as Code |
