# AI Prompt EngX

## Understanding of Large Language Models

- Grasp the main ideas of artificial intelligence
- Follow the growth of language tech from early voice systems to today's advanced models
- Understand the basics of how these language models work
- Discover the ways to use LLMs in various fields

### L1: Introduction to Generative AI

The main concepts of artificial intelligence and how they relate to one another:

- Artificial intelligence
- Machine learning
- Deep learning
- Large language models
- Generative artificial intelligence

![AI-ML-Deep-Learning](imgs/AI-ML-Deep-Learning.png)

> Artificial Intelligence

AI is a science that helps computers and machines act “smart,” think like humans, and complete tasks on their own. Its main goal is to create systems that understand the world, adapt to changes, and interact well with people. AI aims to boost our abilities, increase efficiency, and transform our lives by learning, reasoning, solving problems, and making decisions.

> Machine Learning

AI focuses on giving computers human-like abilities, while ML trains them to do these tasks. Unlike traditional programming, ML doesn't use explicit logic coding. Instead of manually setting search engine rules, computer models now learn user preferences and provide personalized content for each person.

How does it work? Additional nodes and hidden layers help train the model. The process begins with an input layer, or "features". Then each node changes the input and sends results to the next node in the chain. As you can see in the graphic, solving the problem takes just two layers with four and two nodes, respectively, leading to the correct output.

Pay attention to the thickness of the lines joining the nodes. They show each node's impact on the final result. As a prompt engineer, one of your goals is to understand and use these connections effectively. We’ll focus on doing just that throughout this course.

#### Large Language Models and Generative Artificial Intelligence

Large Language Models (LLMs) and Generative Artificial Intelligence (GenAI) are two important pieces of deep learning. How do they fit into AI? Explore the graphic below to learn more.

![LLM_GenAI](./imgs/LLM_GenAI.png)

Generative AI systems and Large Language Models are both subsets of Deep Learning. Language models like GPT-3, GPT-4, and Google Bard focus on generating text, while generative AI systems such as DALL-E and Midjourney produce images. GitHub Copilot produces code, and so on. Most LLMs create their output in response to prompts.

> Prompt

LLMs and Gen AI systems typically use “prompts” as text input to guide the model.Prompts provide instructions and context for a specific task to achieve a desired out

### L2: History of LLMs

Where did LLMs start? Their progression goes back over 70 years.

> Audrey

![LLM_Audrey](./imgs/LLM_Audrey.png)

In the 1950s, Bell Labs conducted pioneering experiments in human speech recognition, giving birth to the first system called "Audrey". Audrey could recognize all ten digits, showcasing the early potential of speech recognition technology and laying the foundation for future advancements. However, despite its remarkable capabilities, Audrey's slow performance led to its eventual abandonment.

> Harpy

![LLM_Harpy](./imgs/LLM_Harpy.png)

In 1976, Carnegie Mellon University and DARPA partnered to develop Harpy, a speech recognition system that could recognize over 1000 words.

> T9

![LLM_T9](./imgs/LLM_T9.png)

In the 1990s, the rise of mobile phones and new communication options led to the creation of T9 ("Text on nine keys"). T9 is considered the precursor to modern LLMs. It allowed users to form words with a single key for each letter, significantly improving the time-consuming multi-tap approach used on conventional mobile phones. It also shares the fundamental concept of predicting the next sequence of characters based on the preceding text.

> GPT

![LLM_GPT](./imgs/LLM_GPT.png)

The greatest breakthrough for AI came in 2017 with the introduction of the Transformer architecture by Google researchers. This architecture, utilized in Generative Pre-trained Transformers (GPT), revolutionized generative AI models. Transformer internally manipulates input sequences by employing self-attention mechanisms to generate meaningful output sequences, taking this technology a step further than anything that has come before.

#### The Evolution of GPT

> GPT-1

The release of GPT-1 by OpenAI in 2018 showcased the power and promise of these models. Unlike previous specialized models that required large, annotated datasets and lacked generalization, GPT-1 could efficiently process unlabeled raw data. This increased its scalability and overall effectiveness by allowing for increased model size (number of parameters) and the utilization of larger training datasets.

> GPT-2

GPT-2 demonstrated even greater capabilities with its impressive 1.5 billion parameters and training on 40 GB of text. GPT-2 became proficient at solving tasks it was not explicitly trained for, such as arithmetic problems and common-sense reasoning, and it could easily generate essays on topics like climate change.

> GPT-3

AI capabilities grew with the increase in parameter count. LLMs learned code completion, translation, pattern recognition, and other, often unexpected skills, resulting in the launch of GPT-3 in 2020. GPT-3 was trained on an extensive dataset of over 570 GB and boasted 175 billion parameters, 116 times more  than GPT-2.

> GPT-4

In early 2023, GPT-4 was released. Although the exact number of parameters and dataset size remains undisclosed, its enhanced capabilities suggest further increases. GPT-4 has achieved remarkable feats, such as passing exams designed for humans without additional training and providing detailed explanations of image content.

> 8B

![AI_8B](./imgs/AI_8B.png)

> 62B

![AI_62B](./imgs/AI_62B.png)

> 540B

![AI_540B](./imgs/AI_540B.png)

### L3: How LLMs Work

#### Transformer Architecture

Transformers have a straightforward architecture consisting of two main components—encoder and decoder—which provides a framework for understanding how LLMs process and generate language.

![GPT_architecture](./imgs/GPT_architecture.png)

> Encoder

Encoder takes an input sequence (prompt), scans it by traversing through multiple internal layers, and identifies relevant blocks. It is then passed to the decoder utilizing a self-attention mechanism.

> Self-Attention Mechanism

Self-attention mechanism helps the system comprehend and process the relationships between words in a sentence or a paragraph. It lives within the “context window”, a dynamic memory for your conversation.

> Decoder

Decoder receives relevant blocks from the encoder and generates output using the provided context.

A self-attention mechanism is a core component that makes transformers so powerful. In early AI models, the input was processed word-by-word by Recurrent Neural Networks (RNN), which struggled to focus and remain in context. Unlike RNNs, transformers holistically analyze a prompt and identify related blocks. It happens within a **context window** — a dynamic memory of your conversation. The larger the context window, the more relations LLM can "see" at once. This is what differentiates, for instance, GPT-4 and GPT-4-32K - the latter can process four times larger inputs at a time but is slower and more expensive, so use it only if you absolutely need it. Transformers can process multiple text sequences in parallel, enhancing their effectiveness as language processing tools.

#### Language Modeling

Language modeling is fundamental to LLMs and involves predicting the next word (token) given the context of the preceding words, using mathematical calculations and probability. With its self-attention mechanism, the transformer architecture enables LLMs to effectively learn and generate language by capturing dependencies and patterns within the input data.

Take a look at this example. Given its training on multiple text examples, the model might predict with a 50% probability that the following word in the sentence would be "pizza". While words like "pasta," "chicken," and "salad" have lower probabilities.

![GPT_modeling](./imgs/GPT_modeling.png)

### L4: LLMs Capabilities

LLMs are a powerful technology that can accomplish a variety of tasks. As this technology continues to develop, even more possibilities will become available. It's up to you to explore and discover potential use cases for LLMs. Generative AI offers a range of capabilities that can help you enhance your productivity, simplify complex tasks, and streamline your workflows. By using AI tools like ChatGPT, you can save time and effort on mundane tasks, allowing you to focus on more creative, strategic, and complex problem-solving roles.

#### LLMs for Productivity Boost

Estimates of potential improvements vary widely, but early trials and research suggest that integrating generative AI and complementary technologies into core processes may result in a 25+% increase in productivity across a range of tasks and activities. This means that using LLMs can have a big impact on functional areas like Finance, Consulting Services, Sales, R&D, Supply Chain, Marketing, HR, IT & Operations, Customer Service, and more.

To help you identify if LLMs are the right tool for your needs, let's take a closer look at some potential use cases where they may fit well.

#### Natural Language Processing (NLP)

LLMs are great at natural language processing (NLP), which is all about teaching computers to understand, analyze, and generate human language. If you're new to this field, there are four key areas where you can use LLMs to your advantage. These include text classification, named entity recognition, text summarization, and text generation.

Language is a huge part of our lives, so the potential for LLMs in NLP is immense. They can be used for a variety of tasks, such as:

- Text Generation: Assist with writing articles, blog posts, stories, and more.
- Sentiment Analysis: Analyze text to determine the general sentiment it conveys positive, negative, or neutral.
- Summarizing Content: Condense large pieces of text into key points for easier understanding.
- Creative Writing: Generate ideas for creative writing, produce drafts, and suggest plot development.
- Conversational AI: Chatbots, Virtual assistants, text-to-speech software, etc.
- Language translation: Translate text from one language to another, with the highest proficiency in English but also capable with many other languages.
- Product and service guides: Creation and curation of product descriptions and service guidelines.
- Email Scanners: For example, LLMs are used to scan emails incoming to large distribution lists. LLMs extract entities, recognize intent, and pass them to the underlying back office solution, which routes the email to specific recipients' groups.
- GPT Chatbots: Another example is Vivi.n—a chatbot created at 'E' that encompasses a GPT-based conversation interface with voice generation and an Unreal engine-based human avatar. Or GPT-augmented customer support chatbot—to name but a few.

#### Other Potential Areas for LLMs Usage

- Code Generation & Optimization
  - Boilerplate code generation
  - Code refactoring
  - Automated generation and testing
- Platform, Product, and Service Integration
  - LLM plug-ins
  - LLM APIs
  - Multi-LLM integration
- Personalization and Recommendations
  - Content recommendations
  - Product recommendations
- Content Generation, Training and Education
  - Product documentation
  - Customized learning materials
  - Interactive simulations
  - Assessment generation
  - Blog post generation
  - Social media management
  - Email marketing campaigns
  - Content summarization
- Data Analysis and Visualization
  - Automated data analysis
  - Predictive analytics
  - Data visualization
  - Data enrichment
- Natural Language Processing & Chatbots
  - Conversational Al
  - Sentiment analysis
  - Language translation
  - Product and service guides
- User Interface (UI) and User Experience (UX) Design
  - UI component generation
  - UX evaluation
  - Wireframe generation

#### Conclusion

By understanding the limitations and risks associated with LLMs, you can make informed decisions and harness their transformative potential to drive innovation and create impactful solutions. The world of Generative AI is yours to explore, and your journey will contribute to the ever-expanding landscape of possibilities and use cases.

## Limitations of Generative AI and Mitigation Techniques

- Understand Generative AI's limitations and workarounds
- Use AI responsibly
- Apply the guardrail framework for safe AI deployment

### L1: Understanding Limitations of LLMs

> Cognitive Limitations

LLMs don't think like humans, and they don’t know if their responses are good or bad. They lack introspection, and they can't understand everything. They might make mistakes, produce fake facts (“hallucinate”) or be biased.

> Output Quality and Transparency Limitations

LLMs generate answers based on their training, which might be outdated. The quality of their responses also depends on the questions you ask them, and it's hard to know the reasoning behind their answers.

> Technical Limitations

LLMs are new technology, so they can be tricked or attacked. Double-check their outputs before accepting an answer.

> Privacy, Security, and Regulatory Limitations

LLMs may store data and raise privacy concerns, so using them for sensitive information could lead to legal issues. Be mindful of data regulations and potential copyright issues when using LLMs.

### L2: Cognitive Limitations

> LLMs do not know if they have produced good or bad output.

If you're using text generation models, like GPT-3.5 or GPT-4, you might not know if the output is good or bad. While the latest models are less likely to make mistakes, they don't check for them while generating text. You also have to consider cost: latest models are usually more expensive, but tend to produce better output, while older ones may be cheaper but not able to meet your needs as effectively.

To avoid mistakes, always check the quality of the output. Try using a more advanced model like GPT-4 to start, and then downgrade if possible. You can prompt the model to validate the output and correct mistakes using self-consistency check technique.

> LLMs do not have an "inner monologue stream in the head" as humans do

Language models identify a cloud of tokens that can be used to complete a prompt. Then, they more or less randomly choose which token to use based on patterns they have learned. They don’t think about their own thoughts or feelings, and they can't tell you any personal perspectives or experiences to help explain their responses.

Understand the prompt structure, and use proper prompt techniques and parameters as a result. This tactic may not fully overcome this limitation, but it will help mitigate it.

> LLMs do not know everything

LLMs are trained on a very large, fact-based knowledge base that spans number of areas, but they don't know everything. Picture LLMs like a jpeg snapshot of internet: When you look at it from far away, it looks good. But when you zoom in, it gets blurry, and you start to see the flaws. LLMs are good at giving general advice or boilerplate code, but they might struggle once you go in-depth into a subject or ask them to complete specific tasks like complex calculations or very specialized code generation. Different models also have their own limitations: some LLMs, like Open AI GPT, don't have all the information yet because the data was cut off in September 2021.

Access plugins and models with an internet connection to help with the data cutoff. This works well for specialized tasks, like calculations.

> LLMs may be biased

LLMs may be biased as they can inadvertently learn and reproduce existing biases present in the data they were trained on, potentially leading to unfair or discriminatory outcomes.
IMG

The reason for this has been mentioned and later you will find a separate section that explains how to deal with potential bias in models.

> LLMs may "hallucinate"

”Hallucinations” are common with LLMs; they can generate non-existent or fabricated information.
IMG

Use LLMs for non-mission-critical use cases first. Always validate any information provided by an LLM. If you’re still worried about hallucinations, try some different fine-tuning techniques to see what works best.

> Conclusion

While LLMs have revolutionized the field of natural language processing, they do come with certain limitations. It is important to understand these limitations and try to mitigate them the best you can. By using advanced models, understanding prompt techniques, accessing plugins and models with an internet connection, and being aware of potential biases and hallucinations, you can maximize their capabilities and enhance your work.

### L3: Output Quality and Transparency Limitations

LLMs provide answers based on how they were trained, so it occasionally happens that the responses you receive are outdated. Plug-ins and APIs integrate current information to augment the underlying foundational models, helping to mitigate this problem. But it's also possible that LLMs can’t understand the full context of the query, leading to incorrect responses. Successful LLM integration depends on data availability, data quality, knowledge management, data access, and operationalization. This course explores data-related limitations and how to best mitigate them.

> Dependency on Prompt Quality

The quality of the response you receive from an LLM depends on your prompt: the quality, clarity, and completeness of what you ask the LLM. Making even slight changes to how you phrase your input can lead to better responses.
IMG

Prompt Engineering is the key to quality responses. We’ll cover these techniques in this course, and you’ll be able to apply them to get the best results.

> Lack of Training Data and Decision Logic Transparency

Unfortunately, users cannot access the documents or datasets that GPT-4 was trained on. But, even if you know the dataset, you may not understand the logic behind the LLM’s output.
IMG

To make it easier, use techniques like self-consistency check. Create a specialized dataset tailored to your industry or use case, which you can fully control. Then, use the LLM to serve data from this dataset. E.* has already explored this use case: Please refer to the GenAI-X Innovation Hub team for examples.

> Conclusion

While LLMs provide valuable answers based on their training, they may suffer from outdated information and limitations in understanding contextual queries. To help mitigate these issues, integrate plug-ins and APIs to enhance the models with current information.

It is important to acknowledge the limitations of LLMs, such as their dependency on prompt quality and the lack of transparency in training data and decision logic, so you can utilize proper prompt engineering techniques and explore methods like self-consistency and creating specialized datasets.

### L4: Technical Limitations

> Prompt Injection

LLMs can be vulnerable to prompt injection attacks. These happen when users manipulate the input prompts to generate unwanted or harmful outputs, leading to misinformation or offensive responses.

Implement measures such as input sanitization, validation, and filtering. It can also be helpful to have human reviewers validate and filter the generated outputs before they’re shared to prevent the spread of misinformation or offensive content.

> "Jailbreak" Attacks

"Jailbreak" attacks refer to attempts by users to exploit the model's behavior beyond its intended use to generate outputs that may violate ethical guidelines or be offensive, inappropriate, or harmful.

Actively monitor and review the outputs generated by LLMs, especially in interactive or public-facing applications. Implement downstream validation and post-processing mechanisms to help filter out undesirable content.

> Data Poisoning Attacks

LLMs learn from the data they are trained on, and if the training data has biased or harmful information, the generated outputs may also be biased or harmful. Data poisoning attacks happen when biased or malicious data is intentionally included in the training data to influence the model's behavior.

Follow robust data collection and curation practices, including thorough data validation and quality control. Regular monitoring and retraining of the model can help identify and mitigate data poisoning attacks.

> Emerging Vulnerabilities

As LLM technology is still relatively new, vulnerabilities and weaknesses may emerge over time, especially as the technology grows and changes.

Stay updated with the latest research and developments in the field and address any newly-discovered vulnerabilities. Regular security audits, code reviews, and collaboration with the AI community can help identify and address potential vulnerabilities in your LLM implementations.

> Conclusion

Recognizing generative AI’s current limitations and potential risks is important, especially as it is changing and evolving. By acknowledging the vulnerabilities associated with LLMs, such as prompt injection, “jailbreak” attacks, data poisoning, and emerging vulnerabilities, we can take proactive steps to mitigate risks and ensure security. Be vigilant and adaptable to unlock generative AI’s transformative potential while safeguarding against potential risks.

### L5: Privacy, Security, and Regulatory Limitations

Generative AI learns from the data or prompts you give them, saving the input data to improve the model. This can be a problem if the model generates content based on private or sensitive information. And, foundational LLMs use training content from many sources, so generated content might accidentally infringe on copyrights, trademarks, or patents by generating text that closely resembles existing work or utilizes previously registered names, logos, or designs.

As you’re working with generative AI, it's important to comply with GDPR and CCPA regulations, as well as privacy laws and regulations.

> Data Privacy & Security Concerns

As with any system that processes data, generative AI has a risk of data breaches or misuse of information.

Do not include sensitive data in prompts if you use public Model-as-a-Service options like OpenAI GPT-x. At E.*, we use an Azure GPT service available via a VPN connection that should only be used for experimenting. If you plan to discuss a GenAI-enabled solution with a customer, engage the GenAI-X Innovation Hub team to find a solution that keeps privacy and security strategies in mind.

> Compliance with Existing Data Regulations

While generative AI seems to align with ethical guidelines, it may not comply with all existing data regulations, like GDPR, CCPA, NDA, or PCI DSS.

Engage the Legal group of the GenAI-X Innovation Hub to ensure the solution you offer the customer complies with all regulations. This is especially important at this time because many governments are still in the early stages of developing specific regulations for AI, and updates are published regularly.

> Copyright Concerns

While the output of generative AI tools is not typically considered eligible for copyright protection because it is not created by a human author, this is an evolving area of law. For instance, the U.S. Copyright Office recently announced that works created with the assistance of AI may be copyrightable, provided they involve sufficient human authorship. Even with this evolving stance, the definition of "sufficient human authorship" remains unclear, introducing uncertainty and potential risk.

Reach out to the GenAI-X Innovation Hub Legal group for guidance if you have any copyright concerns.

> Ethical Considerations

Generative AI also has questions about its ethics – including fairness, attribution, and accountability in using AI-generated content.

We cover ethical aspects of prompt engineering in this training, but if you have further questions, you should reach out to GenAI-X Innovation Hub experts.

> Conclusion

When using generative AI, it is crucial to ensure data privacy and security, comply with existing data regulations, navigate copyright issues, and consider ethical considerations. To mitigate potential risks, avoid including sensitive data in prompts when using public Model-as-a-Service options.

### L6: Guardrail Framework

It's important to use generative AI safely. One way to do this is by using a guardrail framework, either E.*-created or otherwise. This framework was designed to create a controlled environment for generative AI and help avoid any negative user experiences, help mitigate potential risks, maximize the benefits, and ensure compliance with ethical guidelines. It is important to emphasize the value of these frameworks to our clients and provide solutions that align with their specific needs and requirements, ensuring that their generative AI projects are successful, ethical, and reliable.

In this course, you'll be guided through an example guardrail framework, exploring common risks, examples, and purposes detailed in the framework.

> Example Guardrail framework

| Categories | Overview | Examples | Purpose |
| :- | :- | :- | :- |
| Legal Risk | Our Legal team has outlined specific rules that we must adhere to when deploying this product. | - Discriminatory content <br> - Violent/Abusive Profanity <br> - Pricing Misrepresentation <br> | - Filtering responses <br> - Outlining when GPT will refuse to respond <br> - Shifting accountability to OpenAI <br> |
| Technological Risk | We are applying learnings from multiple companies that have stumbled after launching GPT-powered chatbots. | - GPT forgetting its training model GPT <br> -     Hallucinations  <br> - Response accuracy <br> | - Protecting our training model <br> - Preventing the model from responding with outlandish content |
| Competitive Risk | We will not promote the businesses of other companies. Also, we will not belittle, misrepresent, or compare competitors. | - Linking to a competitor's site <br> - Comparing to direct competition  <br> | - Filtering responses <br> - Outlining when GPT will refuse to respond |
| Virtual Assistant Limitations | ChatGPT is a front-end tool that supports multiple response formats, but our Beta launch will be confined to a VA, which limits how users can receive responses. | - Tables will not fit into the VA code <br> - Code responses will not fit/not likely relevant | - Retaining proper UX/Design within the VA <br> - Limiting queries GPT will answer |
| “Example Industry” - Related Topics | “Example Industry” has an endless amount of potential topics. We will do our best to accommodate all aspects of it. But we realize that some topics may violate the " Example Industry-Related" guardrails despite a user's earnest intentions. | - Industry example topic 1 (i.e. Text Translation) <br> - Industry example topic 2 (i.e. Taxation Rules) | - Fine-tuning the model to be inclusive of all topics rationally related to “Example Industry” |
|||||

## Understanding Prompts

- Explain the basics of Prompt Engineering
- Spot and study key parts of great prompts
- Use expert prompting tricks
- Alter advanced parameters to tailor LLM outcomes

### L1: Introduction to Prompt Engineering

Prompt Engineering is a new discipline that investigates how to develop and optimize prompts to efficiently use language models for various applications.

Working with LLMs may sound simple—you ask a question and get an answer. But, in reality, it can be more of an art than a science to design a prompt that delivers the best results. Researchers and the global community alike are studying, brainstorming, and sharing techniques to create the most effective prompts and strategies. In this course, we’ll take a look at these discoveries to help you become a skilled Prompt Engineer.

To better understand what a Prompt Engineer does, review the figure below.

![prompt_engineer_interaction](./imgs/prompt_engineer_interaction.png)

Prompt Engineers primarily interact with LLMs by writing effective prompts. The LLM interacts with the data by querying, collecting, and summarizing it before returning it to the Prompt Engineer. Data is more like a concept here and can be stored in LLM itself or come from the external sources, but it’s your job to validate it, ensuring accuracy and minimizing risks.

### NEW! Where to Write Prompts: E.* Dial

[https://chat.lab.e*com/](https://chat.lab.e*.com/)

> GPT-3.5

GPT-3.5 is an advanced language model developed by OpenAI, known for its powerful natural language processing, understanding, and context awareness. Enabling human-like conversations, it can carry out various tasks, including summarizing, translation, content creation, and answering queries.

> GPT-4

GPT-4 is the latest iteration of the GTP series by OpenAI, offering more powerful natural language processing capabilities than its previous versions. It harnesses extensive training and massive datasets to deliver lifelike conversations, unsurpassed context awareness, and high-quality content generation across multiple languages and domains.

> GPT-4-32K

GPT-4-32K is the extension for GTP-4 that can possess a larger context window size (32,000 tokens). It offers several advantages, including a better understanding of context, improved performance in long-form tasks, enhanced reasoning and knowledge handling, and increased creativity in content generation.

> PaLM2 (Bison)

PaLM2 (Bison) is an LLM developed by Google that leverages an innovative hierarchical architecture to enhance its linguistic competence and computational efficiency. By employing parallel and multitask learning mechanisms, PaLM2 (Bison) provides exceptional performance in diverse language tasks, like question-answering, summarization, and translation, while also being highly scalable and adaptive to new environments.

> Assistants

You can use an Add-on together with a System Prompt to achieve specific behavior in the LLM, which allows for greater flexibility and customization in LLM's responses. In the AI DIAL framework, such Add-ons and System Prompts combinations are called Assistants.

Assistants give you more control over LLM behavior, resulting in tailored and accurate responses that match specific needs. The flexibility of this combination lets you create custom Assistants within the AI DIAL framework. These Assistants can range from simple tasks, like asking the LLM to provide answers in a specific tone or style (e.g., like a pirate), to more complex tasks, such as restricting the LLM's data to a specific geographical area (e.g., providing weather forecasts for Chicago only). Combining Add-ons and System Prompts enables better customization and adaptability to various situations, leading to more versatile AI responses.

In a chat application, you can include custom Assistant(s) that users can choose when using EPAM Dial.

> Applications

In the AI DIAL framework, Applications are ready-to-use solutions made up of predefined configurations of Add-ons and other services or any custom logic. They help you customize LLM behavior by integrating Add-ons with any desired logic. Applications can be any component that meets API requirements and implements non-LLM-related logic, such as repeating user messages, creating a fully deterministic chatbot, or managing complex multi-LLM interactions. This versatility in Applications allows for tailored handling of unique scenarios and requirements.

Examples of Applications include guided conversations and hierarchical searches. Chat applications can have custom Application(s) that you can select to enable specific behaviors.

> System Prompt

The system prompt in EPAM Dial refers to the initial instructions given to the AI. It differs from other prompt types because it sets the context and tone for the conversation, helping the AI understand its purpose and respond accordingly.

The system prompt in EPAM Dial is like a compass for a traveler, pointing the AI in the right direction to navigate the conversational landscape, ensuring it stays on course and aligned with the overall purpose and desired outcome. The system prompt does not change throughout the entire conversation, and the model will consider it when generating responses.

> Temperature

The temperature parameter controls the creativity and randomness of the model's output. A higher temperature (e.g., 1.0) makes the output more diverse and creative, while a lower temperature (e.g., 0.1) makes the output more focused and deterministic. Adjusting the temperature allows you to balance creativity and consistency in the generated text.

#### Conclusion

In summary, E.* AI Dial is an easy-to-use web application that allows users to engage with various Large Language Models. To access it, use a corporate VPN. You can also create and use custom prompts, add-ons, and applications. However, remember that conversations and prompts are currently stored in your browser and will be lost if the cache is cleared, so don't forget to back them up by exporting.

#### Eight components

> Role

The Role component defines the perspective or identity that the LLM must assume to complete the given task, whether an individual or group. It helps set the context and parameters for the rest of the prompt and tells the LLM how to limit the scope and set the tone for the response. The role is usually set at the beginning of the prompt by saying something like “Act as business analyst,”  or “You are a creative and inspirational technical expert at EPAM, with an extensive technical vocabulary.”

> Context

The Context component defines the context in which the prompt is being given. It’s a key component because it includes information about the topic at hand or any relevant background information that the LLM should consider when generating a response. Be mindful, though, that the more context you provide in your prompt, the more expensive your communication with the LLM becomes. Try to find the right balance that fits your needs: too much context may not be required and may be excessive to both you and the LLM. Instead, think about Context like a message to attendees at the start of a meeting: tell it what it needs to know, and don’t overwhelm it with details.

> Instruction

The Instruction (Task or Question) component defines the specific task that LLM should perform; put simply, this is your ask of the LLM. The Instruction could be anything from summarizing text to generating a response to a specific question, but the more precise and succinct you define the ask, the better the chances are that you will receive the desired result. Remember, it is more of an art than a science to create a prompt, and it may take several attempts with different Instructions before you get what you need.

> Data

The Data component includes any relevant data or information that the LLM should consider when generating a response. This could include specific keywords or phrases, documents, or other relevant information. As with the Context component, providing too much Data to process may become expensive. You’ll learn more about how to input external data into LLM in the advanced training.

> Output

The Output component defines the format and structure of the desired output, guiding how the LLM will structure its response. This could include specific formatting requirements or any other relevant information outlining how you want the LLM to present the results, such as a bulletpointed list.

> Example

The Example component provides an example of the type of response the LLM should generate. This helps ensure that the model understands what is being asked and generates the desired response.

> Constraints

The Constraints component defines any limitations that the LLM should work within when generating a response. This could include time constraints, computational resources, or any other relevant limitations. This is where you fine-tune the model to get your desired response.

> Evaluation Criteria

The Evaluation Criteria component defines how the output generated by the LLM will be evaluated. This may include criteria for accuracy, specificity, or any other relevant factors.

#### Example of each Prompt Component

> Role:

Act as a data analyst responsible for improving the product with insights from customer feedback.

> Context:

Our company has received a large amount of customer feedback regarding our latest product release. [… omitted for brevity…].

> Task:

Using the data provided, perform a sentiment analysis of the customer feedback to determine the overall sentiment towards our product. Additionally, identify the top three features that customers mention in their feedback and provide recommendations for improving these features.

> Data:

The provided CSV file includes customer feedback in text format, as well as metadata such as the date and time the feedback was submitted and the customer's geographic location.

> Output:

Generate a report summarizing the sentiment analysis of customer feedback, including the overall sentiment towards our product and the sentiment towards each of the key features identified. Additionally, provide specific recommendations for improving each of these features.

> Example:

Summary report of customer feedback
Overall product feedback
The overall sentiment towards our product.
Key features feedback
Specific feature feedback | Recommendations for improving

> Constraints:

Proceed feedback records only for the last month. Be specific and do not provide any additional recommendations that do not refer to what is mentioned in the provided data.

> Evaluation Criteria:

The report should include information on the accuracy of the sentiment analysis and the thoroughness and clarity of the recommendations provided.

### L2: Prompt Components

- Identify and describe the eight components of a prompt.
- Define the purpose and importance of each prompt component.
- Apply each component to create a comprehensive prompt that provides clarity, conciseness, and effectiveness.
- Analyze prompts that guide LLMs to accomplish specific tasks effectively.

#### Conclusion

Except for Task component, all other components may be optional, but the more you include into your prompt the better LLM will understand the task. Additionally, clear separation between components may increase the efficiency of LLM, how they understand what you are asking and the data that should be considered.

### Scenario: Prompt Components

> "Task" and "Output"

Task: Explain the concept of authentication and its importance in software development. Provide at least three examples of the best practices for secure authentication implementation.

Output: The report should be two-paragraph long.

> "Task" and "Role" components

Task: Explain the concept of authentication and its importance in software development. Provide at least three examples of the best practices for secure authentication implementation.

Role: You are a cybersecurity consultant, and you have been asked to explain to developers how to implement a secure authentication system in their software.

> "Task" and "Constraints"

Task: Explain the concept of authentication and its importance in software development. Provide at least three examples of the best practices for secure authentication implementation.

Constraints: Avoid examples of cloud architecture.

**The best choise is**: "Task" and "Role"

The given prompt is effective, as the Role component describes how to act, and the Task component is clear and well-defined, leaving no room for confusion.

### L3: Prompting Parameters

Prompt parameters are configurable settings that determine how a language model interacts with your prompt. By adjusting these parameters, you can optimize the performance of an LLM, influence the content it generates, and manage its behavior according to your requirements. Mastering advanced prompt parameters is essential for prompt engineers as it allows them to effectively customize the AI's output, making it more relevant, accurate, and valuable.

#### Principal Parameters

Employing advanced prompt parameters enables prompt engineers to achieve, among others, several objectives:

- Control response length and stop sequence
- Define the underlying model
- Manage the Creativity Level
- Control frequency and presence penalties
- Inject start and restart text

These parameters provide a more refined approach to AI interaction, ensuring that the AI-generated output is of the highest quality possible.

Moving forward, you will delve into a more detailed examination of the parameters focusing on the OpenAI GPT family of models. You may find that these parameters are comparable across other LLM systems. The lesson will concentrate on the most significant ones to ensure a strong foundation and understanding of their application.

> Temperature

The "temperature [0 to 1]" is a parameter that controls the creativity and randomness of the model's output. A higher temperature (e.g., 1.0) makes the output more diverse and creative, while a lower temperature (e.g., 0.1) makes the output more focused and deterministic. Adjusting the temperature allows you to balance creativity and consistency in the generated text. This parameter is the first to adjust if you are looking for predictable output each time you call the model.

Temperature is like the spice level in a dish. A low temperature (mild spice) produces a more predictable and consistent flavor, while a high temperature (extra spicy) leads to a more diverse and adventurous taste experience.

> Top P

The "top_p [0-1]" (also known as nucleus sampling) dictates the scope of randomness for the language model. It determines how many random results the model should consider based on the temperature setting. A higher Top P value means that a larger set of tokens will be considered, introducing more randomness and variety. A lower Top P value results in a smaller set of tokens, generating more focused and narrow text.

This parameter is like the variety of ingredients available when making a meal. A higher Top P means you have more items to choose from, leading to more possible recipes and unexpected combinations. A lower Top P narrows your choices, producing more focused and specific dishes.

```console
Top P and Temperature are correlated, and both have a significant effect on output. OpenAI suggests changing only one at a time, keeping another one at 1.
```

> Stop Sequences

The "stop_sequences [list of strings]" is a list of strings or tokens that, when encountered by the model, will cause it to stop generating further text. This helps control the length and structure of the generated content, preventing the model from producing unwanted text beyond the specified stopping point.

Stop sequences are like traffic signs that tell you when to stop. When driving, you stop at a red light or a stop sign. Similarly, when the model encounters a stop sequence, it stops generating text.

> Frequency Penalty

The "frequency_penalty [-2 to 2]" parameter reduces the likelihood of the model repeating the same line verbatim by assigning a penalty to more frequent tokens. A positive frequency penalty (e.g., 1.0) discourages the model from repeating tokens that appear frequently in the input, while a negative frequency penalty (e.g., -1.0) encourages the model to repeat such tokens. You will want to adjust this parameter to prevent the exact completion text from being repeated across multiple requests.

Think of telling stories to friends. If you constantly repeat the same anecdotes, the frequency penalty would remind you to share new, less frequently told stories to keep your listeners engaged.

> Presence Penalty

The "presence_penalty [-2 to 2]" parameter increases the chances of the model discussing new topics by penalizing tokens already present in the input. A positive presence penalty (e.g., 1.0) discourages the model from using tokens already appearing in the input. In contrast, a negative presence penalty (e.g., -1.0) encourages the model to reuse tokens from the input. This parameter is very similar to Frequency, but the presence penalty is applied to the whole text, not just the previous output.

The presence penalty is like choosing topics for a conversation based on what has already been discussed. A positive presence penalty encourages discussing new topics, while a negative presence penalty promotes revisiting previous topics.

> Best of

The "best_of [positive integer]" allows you to specify the number of completions (n) that the model should generate, and it returns the best completion according to the model's internal evaluation. This is useful when obtaining the highest quality completion from different possible results. (n) can be an integer in the range from 1 to 20.

Consider a sports coach who runs several tryouts to choose the best player for a particular position. The best of multiple attempts or performances is selected to achieve the most optimal outcome.

This can be useful if you would like to invoke the model via API and then compare the results. You will have to use just a single API call instead of multiple roundtrips.

```console
Please keep in mind that you will be charged n times the number of tokens present in the prompt. Therefore, setting n = 2 will double the cost of the API call.
```

#### How to Use Parameters

Advanced prompt parameters are generally accessible only through the LLM API. As a developer you would typically create an object containing parameter name/value pairs and pass it to API. Alternatively you can leverage OpenAI Playground UI. Here you would have access to the same parameter set as in API.

In ChatGPT interface, however, there are little to none parameter settings. There is a technique though how to work with the entire range of parameter. You can simply add a JSON settings object to the prompt to adjust the advanced parameters. For instance, if you want to encourage the model to repeat tokens that appear frequently and set the temperature to 0.8, you can alter the message as follows:

```json
[
{"role": "user", "content": "Write a user story for the login process.", "settings": {"temperature": 0.8, " frequency_penalty": -1}}
]
```

Please consider that this feature is not thoroughfully document yet, therefore be caucious when using it in your projects.

#### Conclusion

In conclusion, mastering advanced prompt parameters is crucial for prompt engineers to effectively customize the AI's output, ensuring higher quality, relevance, accuracy, and value. Key parameters include temperature, stop sequences, top P, frequency, and presence penalty, each controlling different aspects of the generated text. Understanding and effectively using these parameters allow engineers to fine-tune AI responses, meeting both creative and practical requirements while achieving optimal results for their specific objectives.

### L4: Prompting Techniques

- Define the concept of a "shot" in the context of Prompt Engineering
- Differentiate between zero-shot and few-shot prompting techniques
- Apply zero-shot and few-shot prompting
- Describe and implement the Chain-of-Thoughts prompting technique
- Explain and apply implement the Tree-of-Thoughts prompting technique

Prompting techniques are a set of methods used to guide Large Language Models (LLMs) to generate text that meets certain criteria. LLMs are trained on massive amounts of text data and can generate high-quality text in response to prompts. However, without proper guidance, the text generated by an LLM may not be relevant or accurate to a specific task. Prompting techniques help to improve the accuracy of the text generated by LLMs by providing a structured framework or guidelines.

> The Concept of a “Shot”

Before moving forward, it is crucial to understand the concept of a "shot" in the context of Prompt Engineering. This concept forms the foundation of various Prompting Techniques that you'll soon explore.

Picture a smart AI system that needs to learn, adapt, and solve various tasks. To achieve this, it must be trained with various examples called "training data".

In Prompt Engineering, the term "shot" refers to the number of examples provided to the model.

This video will explore what "shot" means and its role in crafting effective prompts for large language models.

Imagine you're training your AI system without giving it any examples. That's right - zero examples! This approach is called the "zero-shot" technique. It relies on the model's built-in understanding to tackle a problem.

While it might seem impossible, models are often pre-trained with vast amounts of data, so their innate knowledge can still help solve tasks.

Now, let's say you decide to give your AI system a little bit of help by providing one or more examples. This strategy is known as the "few-shot" methodology.

The few-shot approach aims to fine-tune the model's understanding and performance on specific tasks while maintaining efficiency in the learning process.

To sum it all up, "shot" in Prompt Engineering is all about the number of examples given to a machine learning model. It can range from zero to a few, depending on the approach chosen: zero-shot or few-shot.

#### Prompting Techniques

After understanding the concept of a "shot" in the context of Prompt Engineering, it is time to delve into the finer details of each prompting technique. As you explore each method, you'll master how to craft prompts, keeping in mind that each approach serves a unique purpose in eliciting desired responses.

> Zero-Shot

Zero-shot prompting is a technique that involves generating text from an LLM without additional training or fine-tuning. Instead, pre-trained models are used to generate text in response to a human-generated prompt. Zero-shot prompting minimizes the need for additional training, as models have already learned the underlying structure of the language.

For example, if you want to generate text about cloud computing without training a model specifically on cloud computing. You could give a prompt sentence like:

What are the benefits and challenges of migrating to cloud computing for businesses?

Using the general understanding of language, combined with the context from vast training data, LLMs can generate a response that features relevant facts and insights on cloud computing, despite not having had specific training data on this topic.

In most situations, it is recommended to focus on "zero-shot" prompts. This approach reduces the number of tokens you use and, subsequently, the cost of using the model, as well as prevents overfitting.

```console
Always start with a zero-shot strategy before exposing the model to additional training data.
```

If the model does not produce accurate responses, rethink your prompt and check the techniques for handling unsatisfactory responses.

> Few-Shot

Few-shot prompting involves training LLMs on a few training examples to perform specific tasks. Unlike zero-shot prompt generation, few-shot prompting allows for the customization of LLMs to suit specific task requirements. Few-shot models use a few examples to adapt to a specific type of task, making it a useful method for customizing text generation pipelines.

Prompt:

Please analyze the customer sentiments in the following product reviews

...

"I absolutely love Product A! It has exceeded my expectations and has become an essential part of my daily routine. I highly recommend it to anyone looking for a high-quality and reliable product."

sentiment: positive

...

"I've been using Product A for a few weeks now, and it's been okay. It does what it's supposed to do, but I haven't noticed any significant improvements or changes. It's just an average product in my opinion."

sentiment: neutral

...

"I was really disappointed with Product A. It didn't work as advertised, and I ended up wasting my money on something that didn't deliver results. I wouldn't recommend it to anyone."

sentiment: negative

...

"Product A was a complete waste of money. Not only did it not work, but it also caused some negative side effects. I regret buying it and wish I had done more research before making the purchase."

sentiment:

Response: negative

The few-shot approach might be necessary when dealing with low-quality data or addressing biases in the dataset. It can also be beneficial if you need to establish a context in a conversation with LLM, allowing the model to consider prior messages for generating more accurate responses.

```console
However, be aware that including each shot in the prompt consumes additional tokens, which may become costly when scaled up.
```

> Zero-Shot vs. Few-Shot: Deeper Insight

The practice of few-shot learning often leads to successful prompts, where one or more examples of the desired behavior are included to condition the model to respond as desired for only the current inference. In contrast, prompts without examples are referred to as zero-shot learning.

| **Zerro-Shot** |
| :- |
**Prompt**:
Headline: Tesla acquires DeepScale to develop AI for self-driving cars <br> Topic: |
**Response**:
Tesla has recently acquired DeepScale, a company that specializes in developing AI technology for self-driving cars. With this acquisition, Tesla aims to enhance its autonomous driving capabilities and expand on its already existing Autopilot system.
||

| **Few-Shot** |
| :- |
**Prompt**:
Headline: Apple announces new M1 Pro and M1 Max chips for MacBook Pro lineup
Topic: Computer Hardware
Headline: Google unveils Pixel 6 and Pixel 6 Pro with custom-built Tensor chip
Topic: Mobile Devices
Headline: Microsoft introduces new Azure AI tools for developers
Topic: Cloud Computing
Headline: Tesla acquires DeepScale to develop AI for self-driving cars
Topic:
**Response**:
Autonomous Vehicles
||

The example demonstrates the usefulness of few-shot learning, as the model appears to be guessing the desired behavior without the examples. However, the examples clearly demonstrate how the model should operate and also show the model's ability to infer the desired category label, even without the "Autonomous Vehicles" label in the examples.

> Chain-of-Thought

Chain-of-Thought prompting is an extension of few-shot prompting. It involves training models to learn the type of relationships that exist between different pieces of text. Models are taught to recognize how different pieces of text are connected and use that knowledge to generate text in response to a prompt. This technique is useful in applications such as question-answering and summarization, which require a deep understanding of the relationships between different pieces of text.

| **Standard Prompting** |
| :- |
**Prompt**:
Roger has 5 tennis balls. He buys 2 more cans of tennis balls. Each can have 3 tennis balls. How many tennis balls does he have now?
**Response**:  The answer is 11.
**Prompt**:
The cafeteria had 23 apples. If they used 20 to make lunch and bought 6 more, how many apples do they have?
**Response**: The answer is 27.
||

| **Chain-of-Thought Prompting** |
| :- |
**Prompt**:
**Prompt**: Roger has 5 tennis balls. He buys 2 more cans of tennis balls. Each can have 3 tennis balls. How many tennis balls does he have now?
**Response**: Roger started with 5 balls. 2 cans of 3 tennis balls each is 6 tennis balls. 5+ 6 = 11. The answer is 11.
**Prompt**: The cafeteria had 23 apples. If they used 20 to make lunch and bought 6 more, how many apples do they have?
**Response**: The cafeteria had 23 apples originally. They used 20 to make lunch. So they had 23-20 = 3. They bought 6 more apples, so they have 3+6=9. The answer is 9.
||

You can also attempt to activate the Chain-of-Thought mode in LLM by including the instruction "let's think step-by-step" into your prompt. The model will output the logic and sequence of steps to reach its conclusion. This approach may help "debugging" complex cases in responses.

> Tree of Thoughts

An even more intriguing method for prompting, known as the Tree of Thoughts (ToT), has been proposed by a team of researchers aiming to enhance the problem-solving abilities of LLMs. They introduced a framework that can potentially improve the planning and decision-making processes carried out by models, building upon the previously mentioned techniques.

The concept of ToT involves requesting the model to present various alternatives instead of an immediate solution. Once you obtain these alternatives, you assess them and encourage LLM to expand on the most favorable ones while disregarding the less desirable ones. You continue this process until you achieve the best outcome for your requirements.

Let's compare the Chain of Thought method versus the Tree of Thoughts Prompting approach.

The **Chain of Thoughts** technique follows a linear progression, where one thought leads to another in a sequential manner. It is like a chain where each link is connected to the previous and the next one.

![chain_of_fhoughts](./imgs/chain_of_fhoughts.png)

On the other hand, the **Tree of Thoughts** technique is a non-linear approach to thought generation. It starts with a central concept and branches out into multiple directions, like the branches of a tree.

![tree_of_thoughts](./imgs/tree_of_thoughts.png)

Below is a simplified example of ToT prompting, where different branches of the thought tree are explored within a single chat session. In real-life scenarios, however, you would typically use API calls to navigate the branches programmatically, validating, canceling, or accepting them.

##### Step 1

Provide instruction, asking for options.

- Prompt:

You are the corporate content editor. You need to create a Midjourney prompt to generate image that will illustrate an article with the heading "Agile Transformation in Software Development: Best Practices". Please generate list 3 of distinct options.

- Response:

Option 1: An image of a group of software developers collaborating around a large whiteboard, with agile methodologies like Scrum or Kanban boards being displayed, symbolizing effective communication and teamwork during an agile transformation.

Option 2: A side-by-side comparison image showing the transformation from the traditional waterfall model to Agile methodology - with progress bars, checklists, and timelines illustrating how development phases are streamlined and adapted to the new approach.

Option 3: A visually appealing, modern infographic-style image that showcases the key components of Agile transformation: such as user stories, scrum teams, ceremonies, continuous integration, and more, illustrating the process and best practices of Agile transformation in software development.

##### Step 2

Verify the output and adjust your prompt as needed. In real life, this step is usually repeated several times.

For the example provided, take note of the markup used and how the "sort by" instruction is highlighted. Numbers should be checked.

- Prompt:

20 For each option: evaluate potential, summarize pros and cons, and assign a probability of being clicked by a user. Assign a score based on these factors. Provide output as a list. <important>Sort by score</important>. Use the template.

…
Option 1:
[Score]
[Score justification, 100 characters maximum]
[Pros/Cons]
…

- Response:

Option 3: Score: 9/10 Highly engaging, modern design with key info Pros: Eye-catching, informative, concise representation of agile transformation Cons: Requires more design effort Probability of being clicked: 75%

Option 1: Score: 7.5/10 Illustrates teamwork and agile methodologies Pros: Easy to understand, relatable, showcases practical implementation Cons: May be less unique as it's often used in similar articles Probability of being clicked: 55%

Option 2: Score: 6.5/10 Visual comparison of methodologies Pros: Useful for highlighting the differences between traditional and agile approaches Cons: Less engaging, may require more time to comprehend Probability of being clicked: 45%

##### Step 3

Choose the best option.

- Prompt:

Discard options 1 and 2. For option 3, output the final Midjourney prompt text from previous messages.

- Response:

Option 3: A visually appealing, modern infographic-style image that showcases the key components of Agile transformation: such as user stories, scrum teams, ceremonies, continuous integration, and more, illustrating the process and best practices of Agile transformation in software development.

#### Conclusion

In conclusion, prompting techniques play a crucial role in guiding Large Language Models to generate accurate and relevant text. Understanding the concept of a "shot" and the various prompting techniques, such as zero-shot, few-shot, chain-of-thought, and tree-of-thoughts allows users to craft effective prompts that elicit desired responses from LLMs. While zero-shot prompting is often the recommended starting point, few-shot and chain-of-thought prompting can be useful in specific situations or when dealing with complex tasks. By mastering these techniques, users can harness the full potential of LLMs and improve the overall quality and accuracy of generated text.

## Fine-Tuning Prompts

- Adjust prompts to work better
- Evaluate ideas for great prompts
- Make prompts clear and easy to understand
- Evaluate LLM output

### L1: Fine-Tuning Process Overview

- Define the purpose and importance of fine-tuning prompts in working with language models
- Identify the three main steps involved in the fine-tuning process

As you progress in mastering effective prompts, it becomes increasingly important to fine-tune them to achieve optimal results. The development of effective prompts serves as the foundation for working with language models, while fine-tuning these prompts guarantees that the model addresses your unique requirements and generates the desired output.

```console
Creating effective prompts is the foundation of working with language models, but fine-tuning these prompts ensures that the model caters to your specific needs and produces the desired output.

Mastering and fine-tuning prompts can be compared to baking and decorating a cake perfectly.

Creating effective prompts is like baking the cake itself. You need the right ingredients – your ideas and objectives – and a recipe – the prompt structure and wording. Then, you must set the correct temperature and baking time – the parameters – to achieve a well-baked cake or desired output. Mastering this process creates a solid foundation to build upon.

Fine-tuning prompts, on the other hand, is like decorating a cake. Once you have a well-baked cake, refine its appearance to make it more appealing and customized. This means adjusting the icing – prompt phrasing – balancing decorations – the context – and ensuring the overall design – model behavior – aligns with your vision. Fine-tuning allows you to achieve the perfect balance between aesthetics and taste.

In both cases, practice and experimentation are key to mastering the art of crafting and fine-tuning prompts, just as they are in baking and decorating a cake. Users often face problems like well-crafted prompts not yielding expected results, leading to inconsistencies or irrelevant outputs. This is where fine-tuning comes into play.
```

The purpose of fine-tuning prompts is to refine and adjust them to better align with your objectives, ultimately enhancing the performance of the language model.

The fine-tuning process can be broken down into three main steps. Let's briefly discuss each step:

![fine-tuned_prompts](./imgs/fine-tuned_prompts.png)

> (Re)Validate an Idea

Before starting to work with a language model, it's crucial for you to have a clear and well-defined idea of what you want to achieve. This involves understanding the problem you're trying to solve and determining if a language model is the right tool for the job. When framing your idea, make sure it is specific, well-articulated, and leaves little room for interpretation.

> Improve Prompt or Data

Once you have a clear idea, the next step is to create or adjust your prompt and (or) data. This involves experimenting with different prompt structures, wording, and parameters to find the most effective combination. Keep in mind that prompt engineering is an evolving field, and staying up-to-date with the latest techniques and best practices can help you get the most out of your language models.

> Evaluate Results

After adjusting your prompt or data, it's essential for you to evaluate the results to determine if they meet your expectations or require further adjustments. This involves comparing outputs, checking for consistency, assessing relevance, evaluating clarity, and testing with multiple examples. Based on your evaluation, you may need to iterate and refine your prompt until you achieve the desired results.

#### Conclusion

In conclusion, prompt fine-tuning is an iterative process that requires experimentation and validation. By understanding and following these steps, you can improve the quality of your prompts and achieve better results from language models.

### L2: (Re)Validate an Idea

The first step of fine-tuning prompts is validating (or revalidating) an idea of your prompt. Without a strong foundation, your prompt may fall apart or fail to get the desired results, so it’s imperative to look at your idea from every angle and make sure it fits your needs before you enter it into an LLM. In this course, we’ll walk you through this first step and share some examples of revalidating ideas.

> Ensure Language Model Suitability

Language models are powerful and versatile, but they may not always be the best solution for your idea. Consider alternative approaches before committing to an LLM. If it seems like the wrong fit, don’t try to force it into working with the model – instead, evaluate your options and move on.

For example, LLMs would be an excellent choice for automatically summarizing extensive documents as they excel in these tasks. However, LLMs aren't quite as effective when it comes to tasks that involve spreadsheet calculations, data manipulations, or organizing structured data. In such cases, using Microsoft Excel or Python-based tools would be a more appropriate choice.

> Frame Your Ideas Clearly

If an LLM is the right tool for the job, you must form your idea. A well-articulated and specific idea is more likely to result in what you want, so ensure your idea is clear and concise, leaving little room for interpretation. This will help the model understand your requirements and generate a more accurate output.

For instance, look at examples of clear and vague idea framing:

- **Clear Idea Framing**: Write an essay discussing the causes and effects of climate change, focusing on the role of human activities and the consequences on the global environment.
- **Vague**: Write an essay about the world and problems.

> Give Specific Instructions

Once your idea is formed, how do you get your desired result? Narrow down your ask to restrict how the LLM can operate. By providing specific instructions and limiting the scope of the problem, you can help the model focus on the most relevant information and generate a more precise output.

For instance, look at examples of specific and vague instructions idea framing:

- **Specific Instructions**: Write an analysis of William Shakespeare's play 'Romeo and Juliet', focusing on the theme of love and conflict and discussing three major scenes that demonstrate the relationship between these themes.
- **Vague Instructions**: Write something about a Shakespeare play and its themes.

> Create Simple Prompts

Finally, simplicity is essential when creating prompts. While it may be tempting to include additional details to guide the model, this can lead to overfitting, confusion, and extra costs. Keep your prompts as simple as possible, and only include the information essential to achieving the desired results.

With this process in mind, here are some tips for validating an idea:

- Brainstorm and outline your idea before creating a prompt. Don’t draft the prompt until you’re clear on what you want from the LLM.
- Clearly define the problem you're trying to solve.
- Consider alternative approaches and evaluate if an LLM is the best solution.
- Keep your idea specific, well-articulated, and concise.
- Narrow down your ask and limit the scope of the problem.

#### Conclusion

Validating an idea is a crucial first step in the fine-tuning process of prompts, but it doesn’t stop there.

### L3: Improve Prompt and Data

- Apply rephrasing techniques to enhance prompt clarity and context
- Adjust model parameters to influence output creativity, verbosity, and relevance
- Implement strategies to prevent false or hallucinations responses and increase precision
- Analyze the impact of prompt components on model output

After critically examining your idea, you can begin validating your input to the model. Keep in mind that Prompt Engineering is a young and emerging field, with models constantly being trained and adjusted. New approaches, ideas, and patterns emerge, while some existing techniques become outdated. Consider this guide as a snapshot of knowledge at the time of writing. If your experience differs from our findings, feel free to share your suggestions. Your fellow learners will appreciate it.

Adjusting prompts and data are both vast areas of exploration. However, there are some techniques and strategies to improve prompt effectiveness.

1. Rephrase Your Prompt

The wording of your prompt is crucial for obtaining the desired output from a language model. The following techniques enable you to clarify the context, emphasize important elements, and ensure that the language model understands your requirements, ultimately leading to more accurate and useful results.

> Use a Native language

If English is not your native language, pay attention to how you frame your question. If you're not receiving satisfactory results, try asking the question in your native language first and then prompt the language model to answer in English. Compare the output. Sometimes asking in native languages produced better outputs.

> Try Analogies

Language models are good at processing language and identifying similarities and patterns. Using analogies is a technique that can help rephrase your prompt, leading to better output. By incorporating analogies, you can help the model understand the context and concept you are trying to convey.

> Repeat Words

Don't be afraid to repeat yourself to the model if necessary. Repetitions increase the weight of a particular word or sequence of characters (recency bias), which can be a tool to emphasize something in your prompt. By repeating important keywords or phrases, you can guide the model toward generating the desired output.

> Train a Model

You should generally target zero-shot prompting, but in case you have a complex request you can try to train LLM additionally but providing extra context or examples.

2. Adjust Parameters

Adjusting parameters is a vital aspect of working with language models, as it allows you to fine-tune the model's behavior and output.

> Importance of Parameters

Parameters play a significant role in controlling the behavior of language models. By tweaking these settings, you can influence the model's output in terms of creativity, verbosity, and relevance. Understanding how to adjust parameters effectively can help you obtain the desired output and enhance the overall performance of the model.

> Experimenting with Parameters

When adjusting parameters, it's essential to experiment and iterate to find the optimal settings for your specific use case. You may need to try different combinations of parameter values to achieve the desired balance between creativity, relevance, and conciseness in the model's output.

3. Provide More Room for the Model

It is recommended to send precise, succinct, and scoped instructions to language models, but "providing more room" for the model is not the same.

> Escape Option

Provide the model with an "escape option"—the one that will be returned if nothing else fits.

In conventional programming, this resembles the notion of a default clause in a switch or a finally clause in a try/catch block. It will prevent the model from generating false responses and hallucinations.

||
| :- |
**Prompt (bad)**:
Which color is a pineapple? Choose from the options below:
a) blue
b) pink
c) white
**Response**:
c) white
**Prompt (good)**: Which color is a pineapple? Choose from the options below:
a) blue
b) pink
c) white
d) none of the above
**Response**:
c) none of the above
||

> Qustion Splitting

Another option is to provide more space by splitting your questions into smaller parts. By doing this, you give the model more room for the answer, potentially increasing precision.

||
| :- |
**Prompt (bad)**:
What is the sum of 3,000 + 4,000 and the product of 5 * 6?
**Response**:
The sum is 7,000 and the product is 30
**Prompt (good)**:
What is the sum of 3,000 + 4,000?
**Response**:
The sum of 3,000 + 4,000 is 7,000
**Prompt (good)**:
What is the product of 5 * 6?
**Response**:
The product of 5 * 6 is 30
||

> Step-by-step Debugging

If you're unsatisfied with the results, try to use the "explain step-by-step" command in your prompts and chain-of-thoughts technique. It will put language models into a "debugging mode," allowing you to see how the model arrives at the answer.

||
| :- |
**Prompt (bad)**:
What is 9,000*1,000+1,000,000?
**Response**:
10,001,000
**Prompt (good)**:
What is 9,000*1,000+1,000,000? Let’s think step by step.
**Response**:
Step 1: 9,000*1,000 = 9,000,000
Step 2: 9,000,000 + 1,000,000 = 10,000,000
Therefore, 9,000*1,000+1,000,000 = 10,000,000
||

4. Experiment with Prompt Components

In the previous sections, the role and importance of each prompt component were already described. In this section, more emphasis is placed on some aspects that may be helpful for you when tuning prompts for better output.

> Order Metter

The order in which information is provided to the model can directly impact the output. Even the order of your examples (shots) is also important, as more recent examples tend to have more weight in the output ("recency bias"). Consider experimenting with your prompt by reorganizing the blocks.

When setting limits for a variable in your prompt, it is a good idea to first introduce the variable to the model and then provide additional inputs, similar to the approach used in standard programming.

> Markup and Separators

The use of markup and separators is a critical aspect when crafting prompts. OpenAI emphasizes the impact of inserting extra lines between paragraphs on the output quality. Thus, ensure the creation of well-defined divisions between content blocks by using appropriate delimiters, such as triple backticks, triple quotes, and others, to facilitate the model's ability to distinguish block structures.

For a comprehensive understanding of this principle, consult the in-depth chapter dedicated specifically to this topic within the training materials.

#### Conclusion

In general, experimenting with order in prompts can reveal interesting insights. Put yourself in the shoes of a language model and check if it would be easy for you or another human to grasp the logic and internal connections within your prompt.

In conclusion, effectively working with language models requires a combination of rephrasing prompts, adjusting parameters, providing more room for the model, and experimenting with prompt components. By mastering these techniques, you can enhance the performance of the model and obtain more accurate, relevant, and useful results. Remember that prompt engineering is an evolving field, and it is essential to stay updated with new approaches and ideas. Continuously experimenting and iterating will not only improve your understanding of language models but also contribute to the growth of this emerging field. Sharing your experiences and findings with fellow learners will foster a collaborative learning environment and drive advancements in prompt engineering.

### L4: Evaluate Results

Evaluating the results of LLMs is a crucial aspect of the fine-tuning process, as it allows you to identify areas of improvement and ensure that models are providing the desired output.

```console
Remember, language models are trained to be useful to you, not necessarily accurate.
```

Evaluating GPT results is like examining a blurry picture taken with a low-resolution camera.

- At first glance, the image appears to capture the essence of the scene, providing a general understanding of the subject matter.
- However, as you zoom in to inspect the details, the clarity diminishes, and the accuracy of the finer elements becomes questionable.

Just as you would examine a blurry picture to understand the true nature of the scene, you should approach LLMs results with a critical eye, verifying the information and ensuring the details align with reality. Hallucination is a known issue. Therefore, validate thoroughly and check the output, especially the numbers, meticulously.

1. Handling Hallucinations in AI Models

When using conversational AI models, particularly OpenAI's Language Models, be prepared for hallucinations and unexpected results, especially during long conversations. Even when given clear examples and a strict output format, the model might generate outputs different from what you've asked for. After answering a few questions correctly, it could start adding undesired information to the output, like extra comments to code, imaginary nodes to JSON, or wrapping JSON outputs with unnecessary text.

To address this issue, consider cutting the conversation history and leaving only one or two most important messages for context. An example could be a summary like, "Here's the current state [summary] Please continue from here." This strategy not only helps prevent unexpected outputs but also reduces the cost of API chains calls.

2. Other Approaches for Evaluating Results

Explore other techniques and major known issues when working with LLM and evaluating their outputs.

> Be Cautious with Results

It's also important to be cautious about the validity of the AI-generated results. Don't blindly trust the AI as it can fabricate data, such as statistics or forecasts. Always remember to double-check the provided information to ensure its accuracy and reliability. Validate links, references and numbers provided by the model.

> Handle Unexpected Stops

At times, the ChatGPT model might stop generating text unexpectedly. This could be due to internal API issues or because it has reached the token limit in the response. To address this, you can either increase the token limit or simply prompt the model with "please continue" to resume generating the desired output.

> Choose the Right Model

There are numerous Language Models available, including a range of GPT models from OpenAI, such as davinci, curie, babbage, and ada. These models vary in their purpose, quality, and response time. When designing a prompt for a task and evaluating the results, be mindful of the differences between these models, as well as the associated costs.

#### Conclusion

In conclusion, effectively evaluating the results of LLMs is essential for harnessing their full potential. As these models may not always be accurate, it's crucial to approach their outputs with a critical mindset, validating necessary information and paying special attention to the details. Be prepared for hallucinations and unexpected outputs, and explore various strategies for mitigating these issues, such as trimming conversation history or selecting the most appropriate model for the task at hand. Always exercise caution when relying on AI-generated results, and double-check the provided information to ensure accuracy. By being vigilant and adaptive in your evaluation of LLM outputs, you can significantly improve the fine-tuning process, optimizing your language model to better serve your needs and enhancing its overall performance.

## Formatting Prompts and Results

- Apply Markdown and Wiki Syntax to format input prompts and output results.
- Apply table formatting methods to format tables for both input prompts and output results.
- Differentiate between HTML, XML, JSON, and YAML formatting.

### L1: Formatting Overview

- Explain the importance of formatting prompts and results
- List the main goals of prompt and result formatting

1. Formatting Goals

Clear and well-structured prompts are essential for effective communication with the language model. Even an additional blank line between paragraphs in a prompt can substantially influence output. When the prompt is well-structured, the model comprehends your request more precisely, and you can enjoy an accurate and relevant response.

The same relates to formatting the result. A well-shaped outcome improves readability and comprehension, making extracting relevant information easier. That, in turn, enhances the overall user experience and enables efficient decision-making or further analysis.

Explore the main goals of formatting prompts and results.

> Prompt Formatting Goals

Choosing the right prompt format aims to achieve the following objectives:

- **Clarity**: A well-structured prompt provides clear instructions and reduces misinterpretation by LLM.
- **Context**: A prompt serves as a framework for the model to comprehend and address the given question or problem.
- **Specificity**: Well-structured prompts guide the model to generate focused and targeted responses.
- **Consistency**: Providing prompts in a specific format guides the model to respond in the same format. However, if required by your flow, you can request an explicit response format.
- **Efficiency**: A properly formatted prompt can increase cost efficiency and performance, as API users typically incur charges based on the number of prompt tokens used.

> Output Formatiing Goals

Choosing the right result format aims to achieve the following objectives:

- **Readability**: A formatted result makes the generated output easy to read and understand.
- **Presentation**: A well-structured output makes the presentation visually appealing.
- **Accessibility**: Formatted results can be seamlessly integrated and utilized across different platforms and applications.
- **Relevance**: A well-structured output allows for the easy extraction of relevant information and supports efficient analysis and decision-making.
- **Alignment**: Well-shaped results align with the user's purpose and specific requirements.

The major goal of formatting both the prompt and the result boils down to explaining the structure and the logic of those to the language model. The better you structure your prompt, the more accurate result you will get. The better you structure the result, the easier it will be to extract and process the desired information.

There is a variety of formatting methods to help you with that. They distinguish by their formatting capabilities, but overall, their main goal is to help you format the content (prompt and result) by adding delimiters, e.g., triple backticks ("`""`""`"), triple quotes (“””), triple dashes (---), angle brackets (< >), XML tags (<tag> </tag>), etc. Different formatting methods use different delimiters.

In the upcoming lessons, you will explore formatting methods and their specific delimiters in more detail. For ease of use, all methods are divided into three main groups: text formatting methods, tabular data formatting methods, and other methods.

#### Conclusion

The proper structuring and formatting of both prompts and results are vital for achieving desired outcomes when working with LLMs. Well-structured prompts improve communication with the model and lead to accurate and relevant responses, while well-formatted results enhance readability and facilitate extracting relevant information.

### L2: Formatting Text Data

- Identify the key differences between formatting text in the prompt and formatting text in the result
- Describe the methods for text formatting using Markdown and Wiki Syntax
Apply these methods to format text for both input prompts and output results

When interacting with the LLM, the text serves as the primary communication format. Properly structured and formatted prompts provide clear instructions and context to the model and enable accurate and relevant responses. Similarly, formatting the generated output enhances readability, comprehension, and the visual presentation of the information.

> Prompt Formatiing

This refers to the process of structuring the text in a prompt by applying various techniques, e.g., adding headers, lists, and emphasis (bold and italics), etc., to enhance the clarity and organization of the prompt.

The goal is to provide clear instructions, convey context, and guide the language model in understanding the task to facilitate effective communication and obtain an accurate and relevant response.

> Result Formatting

This refers to structuring the generated text output from the language model by adding headers, lists, emphasis (bold and italics), etc., to improve the output's readability, presentation, and usefulness.

The goal is to make the generated output more accessible, understandable, and visually appealing to the user. Effective formatting of the result text enables more accessible extraction and interpretation of relevant information and is essential for integrating language models into applications or generating reports.

1. Markdown

Markdown is a lightweight markup language designed for easy readability and simplicity in both its source code and rendered output. It supports basic formatting elements like headers, lists, emphasis (bold and italics), links, images, blockquotes, and code blocks. With its minimal syntax and ease of use, Markdown has become popular among developers and non-developers.

Explore how to format content with Markdown.

> Mardown Formatting

In Markdown, common symbols for formatting include the pound sign # for headers, asterisks * and underscores _ for emphasis (italics and bold), and backticks ` for inline code.

Headers:

```markdown
# H1
## H2
### H3 
#### H4 
##### H5
###### H6
```

Emphasis:

```markdown
*Italic text*
_Italic text_ 
      
**Bold text** 
__Bold text__ 

***Bold and italic text*** 
___Bold and italic text___ 
```

Lists:

```maskdown
- Unordered list item 1 
- Unordered list item 2 
  - Nested unordered list item 
  
1. Ordered list item 1 
2. Ordered list item 2 
   1. Nested ordered list item   
```

Blockquotes:

```markdown
> Blockquote text    
```

Code:

```markdown
`Inline code` 
```

> Unformatted

- **Prompt**: Write 1 sentence explanation on 3 most common project management approaches

- **Response**: Certainly! Here's a one-sentence explanation of the three most common project management approaches:
  - Waterfall Approach: A linear project management approach where each phase is completed sequentially before moving on to the next, suitable for projects with well-defined requirements.
  - Agile Approach: An iterative and adaptive project management approach that emphasizes collaboration, flexibility, and delivering value incrementally.
  - Hybrid Approach: A customized project management approach that combines elements from different methodologies to suit the unique needs of a project, providing flexibility while maintaining structure.

> Formatted

- **Prompt**: Write 1 sentence explanation on 3 most common project management approaches. The generated text should be formatted with markdown. The main key phrases should be in bold.

- **Response**: Certainly! Here's a one-sentence explanation of the three most common project management approaches formatted with markdown:
  - Waterfall Approach: A linear project management approach where each phase is completed sequentially before moving on to the next, suitable for projects with well-defined requirements.
  - Agile Approach: An iterative and adaptive project management approach that emphasizes collaboration, flexibility, and delivering value incrementally.
  - Hybrid Approach: A customized project management approach that combines elements from different methodologies to suit the unique needs of a project, providing flexibility while maintaining structure.

3. Wiki Syntax

Wiki Syntax, also known as Wikitext or Wiki Markup, is a lightweight markup language primarily used for editing and creating content on wiki-based platforms like MediaWiki, which powers Wikipedia. It is designed to be easy to learn and use, focusing on using single or double curly braces, brackets, and other punctuation marks. Wiki Syntax supports a similar set of formatting elements as Markdown.

Explore how to format content with Wiki Syntax.

> Wiki Syntax Formatting

In Wiki Syntax, common symbols for formatting include equal signs = for headers, single quotes '' for emphasis, and angle brackets < > for specific tags like blockquotes and code.

Headers:

```wiki
= H1 = 
== H2 == 
=== H3 === 
==== H4 ==== 
===== H5 ===== 
====== H6 ====== 
```

Emphasis:

```wiki
''Italic text'' 

'''Bold text''' 

'''''Bold and italic text''''' 
```

Lists:

```wiki
* Unordered list item 1 
* Unordered list item 2 
** Nested unordered list item 

# Ordered list item 1 
# Ordered list item 2 
## Nested ordered list item 
```

Blockquotes:

```wiki
<blockquote>Blockquote text</blockquote> 
```

Code:

```wiki
<code>Inline code</code> 
        
<pre> 
Block of code 
</pre> 
```

> Formatted

- **Prompt**: Write short paragraph on what are the key roles in Scrum using wiki syntax to format response
- **Response**: In Scrum, there are three key roles that contribute to the effective implementation of the framework:

  - ==Product Owner==
  The '''Product Owner''' represents the stakeholders and ensures their requirements are effectively communicated to the development team. They prioritize the product backlog, make decisions about what features to include, and continuously refine and adjust the product vision.
  - ==Scrum Master==
  The '''Scrum Master''' acts as a facilitator and coach, ensuring that the Scrum framework is followed and supporting the team in overcoming any obstacles. They promote self-organization, facilitate meetings, and foster a collaborative and productive environment.

  - ==Development Team==
  The '''Development Team''' consists of professionals responsible for delivering a potentially shippable product increment in each sprint. They are cross-functional and self-organizing, deciding how to best accomplish the work and collaborating closely to achieve the sprint goals.

  > These three roles work together to enable effective communication, transparency, and agility within the Scrum framework.

#### Conclusion

Formatting text in the prompt and response is crucial for effective interactions with language models and obtaining accurate results. Lightweight markup languages like Markdown and Wiki Syntax offer simple yet effective text formatting capabilities. Understanding and leveraging these formatting methods helps you effectively structure prompts, communicate with the language model, and present the generated output in a way that aligns with your desired outcomes.

### L3: Formatting Tabular Data

- Identify the key differences between prompt and result formatting
- Describe the simple methods for formatting tables
- Apply these methods to format tables for both input prompts and output results

> Prompt Formatting

This refers to the process of converting tabular data into a plain text format that is easily understandable by LLMs.

The goal is to make the data more readable for the model, ensuring that it can accurately interpret the information and generate relevant responses. Proper input formatting is crucial for obtaining accurate and meaningful responses from language models.

> Result Formatting

This refers to the process of converting the generated text from LLMs into a table format for better presentation and understanding by users.

The goal is to make the output data more organized, visually appealing, and easier to analyze for users. Proper output formatting enhances the user experience and facilitates decision-making based on the generated information.

```console
Prompt formatting focuses on preparing table data in a way that is easily understandable by LLMs, while result formatting aims to present the generated information in a table format that is more accessible and actionable for users.
```

Discover these simple methods to format tables using plain text.

> Mardown Formatting

Markdown tables use pipes | and hyphens - to define columns and headers. The alignment can be specified using colons :.

```mardown
| Header 1 | Header 2 | Header 3 | 
| -------- |:-------:| --------:| 
| Row 1, Column 1 | Row 1, Column 2 | Row 1, Column 3 | 
| Row 2, Column 1 | Row 2, Column 2 | Row 2, Column 3 | 
| Row 3, Column 1 | Row 3, Column 2 | Row 3, Column 3 | 
```

> Wiki Syntax Formatting

Wiki Syntax tables use curly braces { and } along with pipes | to define table structure, rows, and cells.

```wiki
{| class="wikitable" 
|+ Table Caption 
|- 
! Header 1 !! Header 2 !! Header 3 
|- 
| Row 1, Column 1 || Row 1, Column 2 || Row 1, Column 3 
|- 
| Row 2, Column 1 || Row 2, Column 2 || Row 2, Column 3 
|- 
| Row 3, Column 1 || Row 3, Column 2 || Row 3, Column 3 
|} 
```

> CSV Formatting

CSV (Comma Separated Values) formatting is a simple file format used to store tabular data, such as a spreadsheet or database. Each line of the file is a data record, and each record consists of one or more fields separated by commas ,.

```console
Header 1,Header 2,Header 3 
Row 1 Column 1,Row 1 Column 2,Row 1 Column 3 
Row 2 Column 1,Row 2 Column 2,Row 2 Column 3 
Row 3 Column 1,Row 3 Column 2,Row 3 Column 3 
```

#### Conclusion

In conclusion, understanding and effectively utilizing table formatting techniques is essential when working with lightweight markup languages and language models. By mastering prompt and result formatting, users can ensure that the language models accurately interpret and generate relevant responses. Familiarizing oneself with simple table formatting methods, such as Markdown, Wiki Syntax, and CSV, can greatly enhance the user experience and facilitate decision-making based on the generated information. By leveraging these techniques, users can efficiently exchange tabular data with language models, enabling a wide range of tasks, including data manipulation, summarization, and analysis.

### L4: Other Formats

- Describe the difference between HTML and XML formatting and explain how to format prompts for LLMs using HTML or XML
- Identify the key differences between JSON and YAML formatting and explain how to format prompts for LLMs using JSON or YAML

There is a variety of other formats, such as JSON, YAML, XML, HTML, and code in different programming languages, that can be used for both the input and output of the LLM, depending on the specific context. You have the freedom to experiment by mixing these formats in your prompt.

Large language models are trained on a massive dataset of text and code. As such, they can understand a variety of formatting conventions, including HTML and XML. However, it is important to note that LLMs could be better, and they may sometimes make mistakes when interpreting formatting. Therefore, it is always a good idea to double-check the output of language models to ensure it is correct.

1. HTML and XML Formatting

HTML and XML offer advanced capabilities for formatting and visual elements, enhancing text presentation and structure. HTML- or XML-formatted text can be rendered within a web browser or seamlessly integrated into an application.

When formatting prompts for LLMs with XML and HTML, the primary difference lies in the markup language structure.

> HTML

HTML (Hypertext Markup Language) is specifically designed for web content display, with predefined tags to structure content, apply styles, and embed media. The main focus is on appearance and presentation.

> XML

XML (eXtensible Markup Language) is designed for data storage, transportation, and system exchange. XML allows you to create custom tags, making it more flexible than HTML. The main focus is on data structure and semantic meaning.

- **Prompt**:

```console
<!-- Prompt --> <h2>Prompt:</h2> <p>Create a webpage layout for an online store.</p>
```

- **Response**:

```console
<!-- Response -->
<h2>Response:</h2>
<div class="container">
  <header>
    <h1>Welcome to our Online Store!</h1>
  </header>
  <nav>
  <ul> 
...
```

You can emphasize a text block, drawing the model's attention with tags. For example, in an XML structure, you can create custom tags such as <important>:

```console
<prompt>
   <question>What are the <important>key differences</important> between cloud computing and traditional on-premises infrastructure?</question>
</prompt> 
```

You can also use XML-like tagging in plain text prompts.

```console
What are the key differences between cloud computing and traditional on-premises infrastructure? Assign a score to the key factors and <important>sort by score</important> the resulting list.
```

2. JSON and YAML Formatting

LLMs can process and understand prompts formatted in JSON or YAML. The main difference between formatting prompts with JSON and YAML lies in the structure and syntax of the two data formats. Both JSON and YAML are used to represent structured data, but they have different syntax rules and readability.

> JSON

JSON (JavaScript Object Notation):

- Uses curly braces {} for objects and square brackets [] for arrays.
- Requires double quotes around keys and string values.
- Is more verbose and less human-readable compared to YAML.
- Is widely used in web applications and APIs.

> YAML

YAML (YAML Ain't Markup Language):

- Uses indentation to represent the structure (similar to Python).
- Does not require quotes around keys or string values unless necessary for disambiguation.
- Is more concise and human-readable compared to JSON.
- Is often used for configuration files and data exchange between languages with different data structures.

You can provide JSON input to the model with instructions, parameters, and role settings. For example:

```json
[
  {"role": "user", "content": "Write a user story for the login process.", "settings": {"temperature": 0.8, " frequency_penalty": -1}}
]
```

You can also mix plain text with formatted input, but in all cases, the key is to provide clear and well-defined guidance to LLM, conveying the structure of your prompt as clearly as possible.

```console
Be mindful at this point: if you want to obtain the output in the desired format (HTML, JSON, CSV, etc.), you should explicitly request it.
```

HTML is a more advanced formatting option that provides precise control over styling and enhances prompts and responses' visual presentation and interactivity.

#### Conclusion

In conclusion, LLMs can process and understand a variety of formats such as JSON, YAML, XML, HTML, and code in different programming languages. This flexibility allows you to experiment with different formats in your prompts, depending on the specific context. While LLMs are trained on massive datasets and can understand various formatting conventions, it is essential to double-check their output for accuracy.

HTML and XML provide advanced formatting capabilities, with HTML focusing on appearance and presentation, while XML emphasizes data structure and semantic meaning. JSON and YAML are used for structured data representation, with JSON being more verbose and less human-readable compared to YAML's concise and human-readable syntax.

When using these formats with LLMs, it is crucial to provide clear and well-defined guidance, conveying the structure of your prompt as clearly as possible. Additionally, if you want the output in a specific format, you should explicitly request it. By leveraging these advanced formatting options, you can enhance the visual presentation and interactivity of prompts and responses, ultimately improving the overall user experience.

## The Ethics of Prompt Engineering

- Grasp the main ideas of ethical rules and biases in AI
- Use the fair prompt engineering methods to tackle biases

### L1: Ethical Principles and Bias in AI

- Describe the fundamental principles of AI ethics
- Describe the common areas and industries potentially prone to bias

As AI advances and shapes various aspects of people's lives, it becomes important to understand the ethical considerations surrounding its development and use. As a prompt engineer, you are responsible for harnessing AI's power and recognizing and addressing the ethical implications. In this lesson, you will explore the fundamental principles that guide AI's safe and responsible application and discover areas within AI systems prone to bias.

1. Principles of AI Ethics

AI ethics encompass a set of principles and practices to ensure AI's safe and responsible use. These guidelines serve as a moral framework, ensuring that the development and deployment of AI align with widely accepted notions of morality.

Although there is currently no official "code" of AI ethics or government regulation in place, various groups and organizations have taken the initiative to develop and implement their own ethical frameworks. An exemplary case to consider is the Rome Call for AI Ethics, a document endorsed by prominent companies such as Microsoft, IBM, and other major organizations. The Rome Call is a valuable example of promoting an ethical approach to AI, highlighting the importance of responsible practices. The document outlines six fundamental principles for ethical AI. 

- **Transparency**: AI systems should be transparent and explainable, ensuring that individuals can comprehend their functionalities, reasoning processes, and how they reach their decisions and predictions.

- **Inclusion**: AI should operate without discrimination, upholding the value and dignity of every human life, irrespective of race, color, gender, language, religion, or political affiliation.

- **Accountability**: Responsible individuals must take accountability for the actions of AI systems.

- **Impartiality**: Bias must be eliminated from AI's operations, ensuring equal and unbiased service to all members of society.

- **Impartiality**: Bias must be eliminated from AI's operations, ensuring equal and unbiased service to all members of society.

- **Security**: AI systems and their creators must act securely, respect user privacy, and responsibly handle data collection, management, and utilization.

While these principles may appear straightforward in theory, putting them into practice and achieving ethical AI poses numerous challenges. As a prompt engineer, you will frequently encounter one of the most prevalent challenges: **bias**.

2. Bias in AI

LLMs like ChatGPT are trained on extensive datasets that may contain biases, and if not carefully examined and managed, they can perpetuate harmful biases. Bias refers to a predisposition or prejudice towards or against something or someone, influencing people's thinking, decision-making, and perception of information. It involves the favoritism or discrimination of certain ideas, individuals, groups, or outcomes based on preconceived notions or personal beliefs.

#### Potentially Biased Areas

Before you delve into strategies for addressing biases in prompt engineering, review common areas and industries potentially prone to bias.

> Insurance Quates

AI models used in the insurance industry may inadvertently produce discriminatory quotes based on age, gender, or race, resulting in unfair pricing and limited access to coverage for certain groups.

> Job Recruitment

AI-powered recruitment tools may generate biased candidate shortlists by disproportionately favoring individuals based on gender, ethnicity, or educational background rather than solely considering their skills, experience, and qualifications.

> Newsfeed Content

AI algorithms used to curate personalized newsfeeds can contribute to the formation of echo chambers by prioritizing content that reinforces users' existing beliefs and biases, thus limiting exposure to diverse perspectives.

> Customer Service

AI chatbots and virtual assistants may inadvertently treat customers differently based on their names, speech patterns, or other factors, leading to unequal service experiences for specific groups.

> Loan Approvals

AI models used in credit scoring and loan decision-making may exhibit discrimination against minority borrowers due to historical biases in the training data, resulting in unfair lending practices.

While this lesson focuses primarily on ethical biases, it's important to note that biases can also exist in other domains. For instance, if the model is trained on code examples of lower quality, it can impact the output. Additionally, when you include specific learning examples or shots in your prompt, the LLM may give them priority in generating the response. Developers of LLMs continually work on improving them and implementing measures to address bias. However, it's important not to blindly trust the output of LLMs and instead consider additional verification, correctness, and fairness checks to ensure the desired outcomes.

Bias in AI is a real-world phenomenon that has significant implications. The following use cases highlight instances where major entities encountered AI bias. 

#### Conclusion

Any decision-making process involving humans or human evaluation should be approached with extra caution and subject to thorough scrutiny. While AI systems hold immense potential for transformative impact, they are not immune to biases that can perpetuate unfairness and discrimination. Understanding and addressing bias in AI is paramount to ensure equitable and just outcomes.

In the next section you will explore the implications of bias in your daily life as a prompt engineer and strategies and techniques to help you combat it.

### L2: Ethical Prompt Engineering Process

Ethical considerations and aspects within prompt engineering are not static; they evolve alongside society. Engaging in ethical prompt engineering is paramount for fostering a fair and inclusive environment and shaping the future of ethical learning and language modeling. In this lesson, you will explore the step-by-step process of ethical prompt engineering, equipping you with the necessary knowledge to excel in this important endeavor.

#### Ethical Prompt Engineering Process 

- Step 1. Understand and Recognize Biases in LLM

Common biases include gender, racial and cultural biases. As a prompt engineer, it is vital for you to understand these biases and cases where they can occur to address and mitigate them. This awareness will enable you to navigate your day-to-day work with a focus on creating inclusive and unbiased prompts.

- Step 2. Apply Debiasing Techniques

Debiasing techniques encompass various methods to mitigate biases in the outputs. One effective approach is to provide counterexamples, which are examples that contradict the biased patterns in the data. The model can learn to avoid biased responses by including diverse and balanced examples. Additionally, you can use neutral language to minimize the influence of biased language or stereotypes in the outputs.

- Step 3. Monitor and Measure Biases

To enhance fairness, monitoring and reviewing your prompts' outputs is essential. Incorporating a dedicated review step, especially when a designated person holds it, will significantly improve the prompt engineering process. Additionally, you can refer to E.* Code of Ethical Conduct and assess your prompt against the values outlined by the company. If you require further guidance or support, don't hesitate to contact your manager, HR, or E.* Compliance Sponsors and Compliance Managers.

- Step 4. Design Prompts in Collaboration with Diverse Teams

Incorporating diverse perspectives and backgrounds in the prompt design process is essential. By involving individuals from different cultural, social, and experiential backgrounds, you can challenge biases, identify blind spots, and ensure that the generated outputs are fair and representative. This collaborative approach fosters a more inclusive and equitable AI ecosystem.

- Step 5. Learn

To effectively contribute to an ethical LLM future, embracing a continuous learning mindset is essential. Staying informed about emerging ethical and bias concerns in prompt engineering and keeping up with new techniques and methods to address them is paramount. This field is dynamic and responsive to societal changes, requiring prompt engineers to adapt their practices accordingly.

#### Conclusion

As a prompt engineer, you can ensure uphold the highest standards of fairness, inclusivity, and ethical responsibility by prioritizing ongoing education and staying attuned to evolving ethical considerations. Embracing this commitment to continuous learning and growth enables you to shape a more equitable and inclusive learning landscape that aligns with society's evolving needs and values.
