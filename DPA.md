# Deployment, Provisioning, and Automations (DPA)

- [Create key pair](#create-key-pair)
- [Create security group](#create-security-group)
- [Deploying an EC2 Instance](#deploying-an-ec2-instance)
- [Secure an EC2 Instance](#secure-an-ec2-instance)
- [EBS Volumes](#ebs-volumes)
- [Bastion Host](#bastion-host)
- [Elastic Load Balancer](#elastic-load-balancer)
- [Elastic Load Balancer Error messsges](#elastic-load-balancer-error-messsges)
- [Deploying Elastic Load Balancer based on instances](#deploying-elastic-load-balancer-based-on-instances)
- [Deploying Elastic Load Balancer based on IP Address](#deploying-elastic-load-balancer-based-on-ip-address)
- [View CloudWatch metrics for ELB](#view-cloudwatch-metrics-for-elb)
- [Create S3 Bucket](#create-s3-bucket)
- [View Access logs for ELB](#view-access-logs-for-elb)
- [Sticky Sessions for ELB](#sticky-sessions-for-elb)
- [Create IAM Role for AMI](#create-iam-role-for-ami)
- [EC2 Image Builder features](#ec2-image-builder-features)
- [Create Amazon Machine Image](#create-amazon-machine-image)
- [CloudFormations Benefits](#cloudformations-benefits)
- [CloudFormations Features](#cloudformations-features)
- [Provisioning EC2 using CloudFormation](#provisioning-ec2-using-cloudformation)

## Create key pair

**aws console > ec2 > Network & Security menu > Key Pairs menu > Create key pair**

1. In the 'Create key pair' panel, set the values of the fields:

- Name (eg. ya-cg-key-1)
- Key pair type (.pem)
- Private key file format (pem)

2. Click the 'Create key pair' button
3. Check that the Key pair was created successfully
4. Check that the Key file in yout Download directory

## Create security group

**aws console > ec2 > Network & Security menu > Security Groups menu > Create security group**

1. In the 'Create security group' panel, set the values of the fields:

- Name and tags
- Basic details (eg. ya-cg-sg-1)
- Description
- Inbound rules
  - Type (SSH)
  - Source type (My IP)

2. Click the 'Create security group' button
3. Check that the Security was created successfully

## Deploying an EC2 Instance

### Launch an EC2 Instance

**Launch an instance**

**aws console > ec2 > Launch instance button > Launch an instance**

1. In the 'Launch an instance' panel, set the values of the fields:

- Name and tags
- Application and OS Images (eg. Amazon Linux)
- Instance type (eg. t2.micro)
- Key pair (eg. Create new key pair or select and existing key pair)
- Network settings
  - click 'Edit' button
  - Auto-assign public IP: Enabled
  - Firewall (security groups) (Create security group or Select existing security group)
- Configure storage (eg. 1x8 gp3)
- Advanced details
- Bootstrap script

```bash
#!/bin/bash
dnf update -y
dnf install httpd -y
echo "<html><body><h1>Hello EC2!</h1></body></html>" >/var/www/html/index.html
systemctl start httpd
systemctl enable httpd
```

2. Click the 'Launch instance' button
3. Check the Instance state and Status check in the Instances panel
4. Check the website in the Internet browser at http:\\instance_ip_address

### Secure an EC2 Instance

1. [Create security group](#create-security-group) 'ya-cg-sg-1'
2. [Create key pair](#create-key-pair)
3. Create an EC2 Instance using the [Launch an EC2 Instance](#launch-an-ec2-instance) procedure and the created Key pair and Security group
4. Connect to your EC2 instance using Key pair file and public IP address of the instance

```bash
chmod 400 ./ya-cg-key-1.pem
ssh i ya-cg-key-1.pem ec2-user@instance_ip_address
```

## EBS Volumes

- [Amazon EBS volume types](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volume-types.html)
- [Introducing new Amazon EBS general purpose volumes, gp3](https://aws.amazon.com/about-aws/whats-new/2020/12/introducing-new-amazon-ebs-general-purpose-volumes-gp3/)

## Bastion Host

- [Linux Bastion Hosts on AWS](https://aws.amazon.com/solutions/implementations/linux-bastion/)

## Elastic Load Balancer

- [Load balancer types](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/load-balancer-types.html)
- [Elastic Load Balancing features](https://aws.amazon.com/elasticloadbalancing/features/)
- [HTTP headers and Application Load Balancers](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/x-forwarded-headers.html)
- [Gateway Load Balancer](https://aws.amazon.com/elasticloadbalancing/gateway-load-balancer/)

## Elastic Load Balancer Error messsges

- [Troubleshoot your Application Load Balancers](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-troubleshooting.html)

## Deploying Elastic Load Balancer based on instances

**Prerequisites**

Create an EC2 Instance using the [Launch an EC2 Instance](#launch-an-ec2-instance) procedure and the created Key pair and Security group.

1. [Create Security group](#create-security-group) 'web-dmz'
    - select 'Inbound security groups rules > type': HTTP
2. [Create ec2 instanse](#deploying-an-ec2-instance) 'web-server-01':
    - select 'Key pair'
        - 'Proceed  without a key pair'
    - select 'Network settings' > 'Edit'
        - select 'Subnet' in AZ 'eu-central-1a'        -
        - 'Select existing securty group': web-dmz
    - select 'Advances details'
        - User data:

```bash
#!/bin/bash
dnf update -y
dnf install httpd -y
echo "<html><body><h1>Hello EC2!</h1></body></html>" >/var/www/html/index.html
systemctl start httpd
systemctl enable httpd
```

3. [Create ec2 instanse](#deploying-an-ec2-instance) 'web-server-02':
    - select 'Key pair'
        - 'Proceed  without a key pair'
    - select 'Network settings' > 'Edit'
        - select 'Subnet' in AZ 'eu-central-1b'
        - 'Select existing securty group': web-dmz
    - select 'Advances details'
        - User data:

```bash
#!/bin/bash
dnf update -y
dnf install httpd -y
echo "<html><body><h1>Hello EC2!</h1></body></html>" >/var/www/html/index.html
systemctl start httpd
systemctl enable httpd
```

**aws console > ec2 > Load balancing menu > Load Balancer menu > Create load balancer**

1. In the 'Create load balancer' panel select Create 'Application Load Balancer', and in 'Create Applicaion Load Balancer' panael set the values of the fields:

- Basic configuration
  - Load balancer name: web-load-balancer
  - Check Internet-facing
  - Check IPv4
- Network mapping:
  - check 'eu-central-1a' and 'eu-central-1b' and select subnets
- Security groups
  - select security group 'web-dmz'
- Listening and routing
  - Listener HTTP:80
    - Protocol: HTTP
    - Default action: click Create target group
      - Basic configuration
        - Choose a target type: Instances
        - Target group name: web-target-group
      - Health checks
        - Health check path: /index.html
    - click 'Next' button
    - Available instances
      - check instances in 'eu-central-1a' and 'eu-central-1b'
      - Ports for the selected instances: 80
      - click 'Include as pending below' button
    - click 'Create target group' button
    - click 'Refresh target group' on a previus page 'Create load balancer' and select 'web-target-group'
- Summary
  - check summary info

2. Click 'Create load balancer' button
3. Click 'View load balancer' button
4. In the 'Load balancers' panel check that 'State' is 'Active' click on the balancer in the 'Name' column
5. In the Details section copy 'DNS name' and open it in the Internet browser

## Deploying Elastic Load Balancer based on IP Address

1. [Create Application Load Balancer](#deploying-elastic-load-balancer) 'web-load-balancer-ip'
    - Create target group
        - Basic configuration
            - Choose a target type: IP addresses
            - Target group name: web-target-group-ip
        - Health checks
            - Health check path: /index.html
        - click 'Next' button
        - IP addresses
            - 'add IPv4 address' of web-server-01: 172.31.16.22
            - 'add IPv4 address' of web-server-02: 172.31.47.138
            - Ports for the selected instances: 80
            - click 'Include as pending below' button
        - click 'Create target group' button
        - click 'Refresh target group' on a previus page 'Create load balancer' and select 'web-target-group'

- Summary
  - check summary info

2. Click 'Create load balancer' button
3. Click 'View load balancer' button
4. In the 'Load balancers' panel check that 'State' is 'Active' click on the balancer in the 'Name' column
5. In the Details section copy 'DNS name' and open it in the Internet browser

**Use Cases**

- EC2 instances that have multiple IP addresses, and resources that are accessed using an IP (ed. databases)

## View CloudWatch metrics for ELB

Create an EC2 Instance using the [Launch an EC2 Instance](#launch-an-ec2-instance) procedure and the created Key pair and Security group.

**aws console > CloudWatch > Metrics menu > All Metricss menu > Browse tab > ApplicationELB > Per APPELB, per AZ, per TG Metrics**

Main metrics for ELB:

- HealthyHostCount
- UnHealthHostCount
- RequestCount
- TargetResponseTime
- HTTPStatusCodes

## Create S3 Bucket

**aws console > S3 > Create bucket button > Create bucket**

1. In the 'Create bucket panel, set the values of the fields:

- General configuration
  - Bucket name (eg. ya-elb-access-log, must be unique)
  - AWS Region

2. Click 'Create bucket'

## View Access logs for ELB

**aws console > Load balancers feature > Check <load-balancer-name> > Actions button > Edit load balancer attributes**

1. In the 'Edit load balancer attributes' panel, set the values of the fields:

- Enable 'Monitoring > Access logs'
- Click 'View' button
- Select an existing or create new Bucket
  - [Create S3 Bucket](#create-s3-bucket)
  - Click 'Browse S3' and select the bucker (eg. ya-elb-access-log)

2. Change Permissions for created bucket in

- Open 'Permisions' tab for created bucket
- Click 'Bucket policy > 'Edit' button
- Add Bucket policy*

```json
{
 "Version": "2012-10-17",
 "Statement": [
  {
   "Effect": "Allow",
   "Principal": {
    "AWS": "arn:aws:iam::054676820928:root"
   },
   "Action": "s3:PutObject",
   "Resource": "arn:aws:s3:::ya-elb-access-log/*"
  }
 ]
}
See*:
- [How do I troubleshoot S3 related errors while setting up ELB access logging?](https://repost.aws/knowledge-center/elb-troubleshoot-access-logs)
- [Enable access logs for your Application Load Balancer - Regions available before August 2022](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/enable-access-logging.html)
```

3. Click 'Save changes' button
4. Open created Bucket and select 'Objects' tab
5. Navigate into 'AWSLogs/' subfolders
6. Check and 'Download' gz'iped log file
    - Amazon S3 > Buckets > ya-elb-access-log > AWSLogs/ > 63...58/ > elasticloadbalancing/ > eu-central-1/ > 2023/ > 10/ > 25/

## Sticky Sessions for ELB

- Sticky Sessions in Load Balancing - use cookies to identify a session, and send requests that are parh of the same session to the same target (VM)
- Use Stikcy Session then an application caches session information locallly on the web server (online forms, shopping carts, training websites)

## Create IAM Role for AMI

**aws console > IAM > Access management menu > Roles menu > Create role**

1. In the 'Select trusted entity' panel, set the values of the fields:

- Check 'Trusted entity type > AWS service'
- Select 'Use case > Service or use case': EC2
- Click 'Next' button

2. In the 'Add permissions' panel, set the values of the fields:

- In 'Permissions policies' filter enter: image
- Check 'EC2InstanceProfileForImageBuilder'
- Clear filter
- In 'Permissions policies' type: SSM
- Check 'AmazonSSMManagedInstanceCore'
- Click 'Next' button

3. In the 'Name, review, and create' panel, set the values of the fields:

- 'Role details > Role name': yaImageBuilderRole
- Check 'Trust policy'

```bash
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "sts:AssumeRole"
            ],
            "Principal": {
                "Service": [
                    "ec2.amazonaws.com"
                ]
            }
        }
    ]
}
```

- Click 'Create role' button

## EC2 Image Builder features

How does it work:

- Choose Base OS (eg., Amazon Linux)
- Define software to Install (eg., Python)
- Run test
- Distribute the image

## Create Amazon Machine Image

**aws console > EC2 Image Builder > Image pipelines button > Roles menu > Create role**

Go through the workflow:

1. In the 'Specify pipeline details' panel, set the values of the fields:

- General
    - 'Pipeline name': ya-web-ami-pipeline
    - Deselect 'Enable enhanced metadata collection'
- Build schedule
    - Check 'Schedule options > Manual'
- Click 'Next' button

2. In the 'Choose recipe' panel, set the values of the fields:

- 'Recipe > Configuration options'
    - Select 'Create new recipe'
- 'Image type > Output type'
    - Select 'Amazon Machine Image (AMI)'
- General
    - Name: ya-web-ami-recipe
    - Version: 1.0.0
- Base Image
    - Select 'Select image > Select managed images'
    - Select 'Image Operating System (OS) > Amazon Linux'
    - Select 'Image origin > Quick start (Amazon-managed)'
    - Select 'Image name > Amazon Linux 2023 x86'
    - Select 'Auto-versioning options > Use latest available OS version'
- Instance configuration 
    - Check 'SSM agent > Remove SSM agent after pipeline execution'
- Components
    - In Filter 'Build components - Amazon Linux' enter: update
    - Check 'update-linux'
    - In Filter 'Test components - Amazon Linux' enter: simple
    - Check 'simple-boot-test-linux'
- Click 'Next' button

3. In the 'Define infrastructure configuration - optional' panel, set the values of the fields:

- 'Infrastructure configuration > Configuration options'
    - Select 'Create a new infrastructure configuration'
- General
    - Name: ya-web-ami-pipeline-config
    - IAM role: yaImageBuilderRole (created before)
- AWS infrastructure
    - Select 'Instance type': t2.micro
- Click 'Next' button

4. In the 'Define distribution settings - optional' panel, set the values of the fields:

- 'Distribution settings > Configuration options'
    - Select 'Create distribution settings using service defaults'
- Click 'Next' button

5. In the 'Review' panel, review all setting values:

- Click 'Create pipeline' button

6. In the 'Image pipelines ' panel, set the values of the fields:

- Check pipeline (eg., ya-web-ami-pipeline)
- Click 'Action' button and select 'Run pipeline'
- Click 'View detail' button
- It can take 20 min or more...
- Check 'EC2 > Instances' to view the creation of 'Build instance for ya-web-ami-recipe' instance

## CloudFormation Benefits

- **Quck and efficient** - less time and efforts then do configuring manually
- **Consistent** - fewer mistakes and reuse developed configurations again
- **Version control** - IaC, templates are stored in the repository
- **Manage updates** - can be used to manage updates and dependencies
- **Rollback** - can be used to rallback to the previous state or to remove the infrastructure
- **Free to use** - you are charged only for used resources

## CloudFormation Features

How does it work:

- **Resources** - is the only mandatory section of the CloudFormation template
- **Transform**- is used for referencing additional code stored in S3
- **Mapping** - allows to create custom mappings like Region-AMI
- **IaS** - YAML or JSON is used for templates
- **Parameters** - is used to input custom values
- **Conditions** - is used to provision resources based on environment

- [Template snippets](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/CHAP_TemplateQuickRef.html)
- [AWS::Include transform](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/create-reusable-transform-function-snippets-and-add-to-your-template-with-aws-include-transform.html)

## Provisioning EC2 using CloudFormation

**Prerequisites**

1. [Create key pair](#create-key-pair) 'aws-web.pem'
    - select 'Inbound security groups rules > type': HTTP

**Create a CloudFormation stack**

**aws console > CloudFormation > Create stack button > Roles menu > Create role**

Go through the workflow:

1. In the 'Create stack' panel, set the values of the fields:

- 'Prerequisite - Prepare template > Prepare template'
    - Select 'Template is ready'    
```yaml
AWSTemplateFormatVersion: 2010-09-09

Description: Template to create an EC2 instance and enable SSH

Parameters: 
  KeyName:
    Description: Name of SSH KeyPair
    Type: 'AWS::EC2::KeyPair::KeyName'
    ConstraintDescription: Provide the name of an existing SSH key pair

Resources:
  cloudFronIinstance:
    Type: 'AWS::EC2::Instance'
    Properties:
      InstanceType: t2.micro
      ImageId: ami-04376654933b081a7
      KeyName: !Ref KeyName
      SecurityGroups:
        - !Ref InstanceSecurityGroup
      Tags:
        - Key: Name
          Value: cf-instance
  InstanceSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupName: floudfront-web-dmz
      GroupDescription: Enable SSH access via port 22
      SecurityGroupIngress:
        IpProtocol: tcp
        FromPort: 22
        ToPort: 22
        CidrIp: 0.0.0.0/0

Outputs: 
  InstanceID:
    Description: The CF Instance ID
    Value: !Ref cloudFronIinstance
```

- 'Specify template > Template source'
    - Select 'Upload a template file' (eg., cloudfront-ec2-template.yaml)
    - Click 'View in Designer' button to check the template

```console
S3 URL: https://s3.eu-central-1.amazonaws.com/cf-templates-gkyuh5cnfhe7-eu-central-1/2023-10-26T093349.688Zz2i-cloudfront-ec2-template.yaml
```

- Click 'Next' button

2. In the 'Specify stack details' panel, set the values of the fields:

- 'Stack name > Stack name': cloudfront-ec2-stack
- 'Parameters > KeyName': aws-web
- Click 'Next' button

3. In the 'Configure stack options' panel, set the values of the fields:

- Use default values
- Click 'Next' button

4. In the 'Review' panel, review all setting values:

- Review cloudfront-ec2-stack
    - Step 1: Specify template
        - Prerequisite - Prepare template
        - Template
    - Step 2: Specify stack details
        - Stack name
        - Parameters
    - Step 3: Configure stack options
        - Tags
        - Permissions
        - Stack failure options
        - Stack policy
        - Rollback configuration
        - Notification options
        - Stack creation options
- Click 'Submit' button
- In  'cloudfront-ec2-stack' panal select 'Events' tab and check the 'Status' of the Steck creation

4. In the 'cloudfront-ec2-stack' panel:
- Select 'Events' tab and check the 'Status' of the Steck creation
- Click 'Delete' button, deleting the stack will delete all stack resources

