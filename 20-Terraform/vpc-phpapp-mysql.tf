variable "app" {
  type    = string
  default = "phpapp"
}

variable "keypair_name" {
  type    = string
  default = "aws-web"
}

variable "ssl_certificate_arn" {
  type    = string
  default = "arn:aws:acm:eu-central-1:632397271958:certificate/cc31de15-ebf9-4272-a08d-dbf22bcafead"
}

variable "access_key_id" {
  type    = string
  default = "AK..RK"
}

variable "secret_access_key" {
  type    = string
  default = "rx...DB"
}

variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "env_name" {
  type    = string
  default = "prod"
}

variable "vpc_cidr" {
  type    = string
  default = "10.10.0.0/16"
}

variable "private_subnet1_cidr" {
  type    = string
  default = "10.10.1.0/24"
}

variable "private_subnet2_cidr" {
  type    = string
  default = "10.10.2.0/24"
}

variable "public_subnet1_cidr" {
  type    = string
  default = "10.10.10.0/24"
}

variable "public_subnet2_cidr" {
  type    = string
  default = "10.10.20.0/24"
}

variable "db_name" {
  type    = string
  default = "phpapp"
}

variable "db_username" {
  type    = string
  default = "root"
}

variable "db_root_password" {
  type    = string
  default = "Password123#"
}

variable "db_password" {
  type    = string
  default = "Password123#"
}

# =====================================
# Data Source
data "aws_region" "current" {}

data "aws_availability_zones" "available" {}

data "aws_ami" "selected" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = [lookup({ eu-central-1 = "amzn2-ami-hvm-*-x86_64-gp2" }, data.aws_region.current.name)]
  }
}

# Add additional resource configurations for ASG, ELB, and EC2 here

# Resources
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "${var.env_name}VPC"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.env_name}InternetGateway"
  }
}

resource "aws_vpc_attachment" "main" {
  vpc_id             = aws_vpc.main.id
  internet_gateway_id = aws_internet_gateway.main.id
}

# Private Subnets
resource "aws_subnet" "private_subnet1" {
  vpc_id                  = aws_vpc.main.id
  availability_zone       = element(tolist(data.aws_availability_zones.available.names), 0)
  cidr_block              = var.private_subnet1_cidr
  map_public_ip_on_launch = false
  tags = {
    Name = "${var.env_name}PrivateSubnetAZ1"
  }
}

resource "aws_subnet" "private_subnet2" {
  vpc_id                  = aws_vpc.main.id
  availability_zone       = element(tolist(data.aws_availability_zones.available.names), 1)
  cidr_block              = var.private_subnet2_cidr
  map_public_ip_on_launch = false
  tags = {
    Name = "${var.env_name}PrivateSubnetAZ2"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.env_name}PrivateRouteTable"
  }
}

resource "aws_route" "default_private_route" {
  route_table_id         = aws_route_table.private_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id          = aws_nat_gateway.main.id
}

resource "aws_subnet_route_table_association" "private_subnet1_association" {
  subnet_id      = aws_subnet.private_subnet1.id
  route_table_id = aws_route_table.private_route_table.id
}

resource "aws_subnet_route_table_association" "private_subnet2_association" {
  subnet_id      = aws_subnet.private_subnet2.id
  route_table_id = aws_route_table.private_route_table.id
}

# Public Subnets
resource "aws_subnet" "public_subnet1" {
  vpc_id                  = aws_vpc.main.id
  availability_zone       = element(tolist(data.aws_availability_zones.available.names), 0)
  cidr_block              = var.public_subnet1_cidr
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.env_name}PublicSubnetAZ1"
  }
}

resource "aws_subnet" "public_subnet2" {
  vpc_id                  = aws_vpc.main.id
  availability_zone       = element(tolist(data.aws_availability_zones.available.names), 1)
  cidr_block              = var.public_subnet2_cidr
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.env_name}PublicSubnetAZ2"
  }
}

resource "aws_eip" "main" {
  domain = "vpc"
  tags = {
    Name = "${var.env_name}ElasticIP"
  }
}

resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.main.id
  subnet_id     = aws_subnet.public_subnet1.id
  tags = {
    Name = "${var.env_name}NatGateway"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.env_name}PublicRouteTable"
  }
}

resource "aws_route" "default_public_route" {
  route_table_id         = aws_route_table.public_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_subnet_route_table_association" "public_subnet1_association" {
  subnet_id      = aws_subnet.public_subnet1.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_subnet_route_table_association" "public_subnet2_association" {
  subnet_id      = aws_subnet.public_subnet2.id
  route_table_id = aws_route_table.public_route_table.id
}

# =====================================

# Variables
variable "env_name" {
  type    = string
  default = "prod"
}

# Public Security Groups
resource "aws_security_group" "bastion_sg" {
  name        = "BastionSG"
  description = "Security Group for private network"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = ["194.29.60.39/32"]
  }

  tags = {
    Name = "BastionSG"
  }
}

# Private Security Groups
resource "aws_security_group" "private_sg" {
  name        = "PrivateSG"
  description = "Security Group for Private subnets"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port              = 22
    to_port                = 22
    protocol               = "tcp"
    security_group_source = aws_security_group.bastion_sg.id
  }

  tags = {
    Name = "PrivateSG"
  }
}

resource "aws_security_group" "database_sg" {
  name        = "DatabaseSG"
  description = "Database security group"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port                = 3306
    to_port                  = 3306
    protocol                 = "tcp"
    security_group_source   = aws_security_group.bastion_sg.id
  }

  ingress {
    from_port                = 3306
    to_port                  = 3306
    protocol                 = "tcp"
    security_group_source   = aws_security_group.web_sg.id
  }

  tags = {
    Name = "DatabaseSG"
  }
}

# Web DMZ Security Group
resource "aws_security_group" "web_sg" {
  name        = "${var.env_name}SecurityGroup"
  description = "Security group with HTTP ingress rule"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "WebSG"
  }
}

# =====================================

# EC2 Instances
resource "aws_instance" "app_bastion" {
  ami             = data.aws_ami.selected.id
  instance_type   = "t2.micro"
  key_name        = var.keypair_name
  subnet_id       = aws_subnet.public_subnet1.id
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.bastion_sg.id]
  tags = {
    Name = "AppBastion"
  }

  user_data = <<-EOF
#!/bin/bash
dnf update -y
dnf install -y mariadb105
EOF
}

resource "aws_instance" "app_db" {
  ami             = data.aws_ami.selected.id
  instance_type   = "t2.micro"
  key_name        = var.keypair_name
  subnet_id       = aws_subnet.private_subnet1.id
  vpc_security_group_ids = [aws_security_group.private_sg.id, aws_security_group.database_sg.id]
  tags = {
    Name = "${var.env_name}AppDB"
  }

  user_data = <<-EOF
#!/bin/bash
set +e
date
dnf update -y

# Install AWS CLI
dnf install -y aws-cli
APP=phpapp
ACCESS_KEY=AK...RK
SECRET_KEY=rx...DB
REGION=eu-central-1
aws configure set aws_access_key_id $ACCESS_KEY
aws configure set aws_secret_access_key $SECRET_KEY
aws configure set region $REGION
aws configure set output json
aws configure list
aws sts get-caller-identity

echo "APP: $APP"
aws s3 ls
aws s3 cp s3://ya-cf-templates/phpapp/.env ~/

export $(grep -v '^#' ~/.env | xargs)
DB_HOST=$(ip -br a | grep -v lo | xargs | cut -d' ' -f3 | cut -d'/' -f1)
sed -i '/DB_HOST/d' ~/.env
echo "DB_HOST=$DB_HOST" >> ~/.env
aws s3 cp ~/.env s3://ya-cf-templates/phpapp/
rm ~/.env

echo "DB_NAME: $DB_NAME"
echo "DB_ROOT_PASSWORD: $DB_ROOT_PASSWORD"
echo "DB_HOST: $DB_HOST"

wget https://dev.mysql.com/get/mysql80-community-release-el9-5.noarch.rpm
dnf install mysql80-community-release-el9-5.noarch.rpm -y
dnf install mysql-community-server -y

systemctl enable --now mysqld
systemctl is-enabled httpd && echo "mysqld is enabled" || echo "mysqld is not disabled"
systemctl status mysqld
systemctl stop mysqld
systemctl status mysqld

sudo -u mysql /usr/sbin/mysqld --skip-grant-tables --skip_networking &
sleep 5
mysql -u root --skip-password -e "FLUSH PRIVILEGES; ALTER USER 'root'@'localhost' IDENTIFIED BY '$DB_ROOT_PASSWORD';"
kill $(ps xa | grep "sudo -u mysql" | head -n 1 | xargs | cut -d' ' -f1)
sleep 10
systemctl start mysqld
systemctl is-active mysqld && echo "mysqld is active" || echo "mysqld is not active"
mysql -u root -p$DB_ROOT_PASSWORD -e "CREATE USER 'root'@'%' IDENTIFIED BY '$DB_ROOT_PASSWORD'; FLUSH PRIVILEGES;"
mysql -u root -p$DB_ROOT_PASSWORD -e "GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;"
mysql -u root -p$DB_ROOT_PASSWORD -D mysql -e "SELECT Host, User FROM user WHERE user = 'root';" > ~/db_users
cat ~/db_users

aws s3 cp s3://ya-cf-templates/phpapp/init.sql ~/
mysql -u root -p$DB_ROOT_PASSWORD < ~/init.sql
date

rm -f ~/.aws/
history -c
EOF
}

resource "aws_launch_template" "app_launch_template" {
  name = "${var.env_name}AppLaunchTemplate"

  image_id        = data.aws_ami.selected.id
  instance_type   = "t2.micro"
  key_name        = var.keypair_name

  network_interfaces {
    device_index          = 0
    associate_public_ip_address = false
    security_groups = [aws_security_group.web_sg.id, aws_security_group.private_sg.id]
    subnet_id       = aws_subnet.public_subnet1.id
  }

  user_data = <<-EOF
#!/bin/bash
set +e
dnf update -y
dnf -y install -y httpd mod_ssl php8.2 php8.2-mysqlnd mariadb105
systemctl enable --now httpd
cd /var/www/html

echo "<h1>Hello from Server</h1>" > index.html

# Create SSL certificate
mkdir /etc/httpd/ssl              
openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
  -keyout /etc/httpd/ssl/apache.key -out /etc/httpd/ssl/apache.crt \
  -subj "/C=US/ST=State/L=City/O=Organization/OU=Unit/CN=app.kolyada.link"

# Configure SSL for httpd
cd /etc/httpd/conf.d/
cp ssl.conf ssl.conf.orig
sed -i '/Listen/d' ssl.conf
sed -i '/SSLEngine/d' ssl.conf
sed -i '/SSLCertificateFile/d' ssl.conf
sed -i '/SSLCertificateKeyFile/d' ssl.conf
sed -i '1 i\Listen 443 https' ssl.conf
sed -i '/<VirtualHost _default_:443>/a \
SSLEngine on \
SSLCertificateFile /etc/httpd/ssl/apache.crt \
SSLCertificateKeyFile /etc/httpd/ssl/apache.key' ssl.conf
systemctl restart httpd

# Install AWS CLI              
dnf install -y aws-cli
cd /var/www/html
APP=phpapp
ACCESS_KEY=AK...RK
SECRET_KEY=rx...DB
REGION=eu-central-1
aws configure set aws_access_key_id $ACCESS_KEY
aws configure set aws_secret_access_key $SECRET_KEY
aws configure set region $REGION
aws configure set output json

#  Use the Endpoint for the mysql connection
aws s3 cp s3://ya-cf-templates/phpapp/.env ~/
export $(grep DB_HOST ~/.env | xargs)
echo "DB_HOST: $DB_HOST"
rm ~/.env

# Copy Application
cd /var/www/html
aws s3 cp s3://ya-cf-templates/phpapp/ . --recursive --exclude *.sql
sed -i "s/database.ciuhs3hsfsqj.eu-central-1.rds.amazonaws.com/$DB_HOST/g" ./db.php

# Remove init configuration
rm -f ~/.aws/
history -c

# =====================================

# Load Balancer for internet traffic
resource "aws_lb" "application_load_balancer" {
  name               = "${var.env_name}ELB"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.web_sg.id]
  subnets            = [aws_subnet.public_subnet1.id, aws_subnet.public_subnet2.id]
}

# Target Group
resource "aws_lb_target_group" "target_group" {
  name     = "${var.env_name}TargetGroup"
  port     = 80
  protocol = "HTTP"

  health_check {
    interval          = 30
    path              = "/"
    port              = "traffic-port"
    healthy_threshold = 5
    unhealthy_threshold = 2
    matcher           = "200"
  }

  vpc_id = aws_vpc.main.id
}

# Auto Scaling
resource "aws_autoscaling_group" "auto_scaling_group" {
  desired_capacity     = 1
  min_size             = 1
  max_size             = 3
  vpc_zone_identifier = [aws_subnet.private_subnet1.id, aws_subnet.private_subnet2.id]

  launch_template {
    id      = aws_launch_template.app_launch_template.id
    version = aws_launch_template.app_launch_template.latest_version
  }

  target_group_arns = [aws_lb_target_group.target_group.arn]

  tags = [
    {
      key                 = "Name"
      value               = "${var.env_name}App"
      propagate_at_launch = true
    },
  ]
}

# =====================================

# Redirect HTTP traffic to HTTPS
resource "aws_lb_listener" "alb_http_listener" {
  load_balancer_arn = aws_lb.application_load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect_action {
      host        = "#{host}"
      path        = "/#{path}"
      port        = "443"
      protocol    = "HTTPS"
      query       = "#{query}"
      status_code = "HTTP_301"
    }
  }
}

# Forward HTTPS traffic to Load Balancer
resource "aws_lb_listener" "alb_https_listener" {
  load_balancer_arn = aws_lb.application_load_balancer.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"

  default_action {
    type = "forward"

    forward_action {
      target_group_arn = aws_lb_target_group.target_group.arn
    }
  }

  certificate_arn = var.ssl_certificate_arn
}

# Route53 record for Elastic Load Balancing
resource "aws_route53_record" "app_alias_record" {
  depends_on = [aws_lb_listener.alb_https_listener]

  zone_id = var.route53_zone_id
  name    = "app.kolyada.link"
  type    = "A"

  alias {
    name                   = aws_lb.application_load_balancer.dns_name
    zone_id                = aws_lb.application_load_balancer.zone_id
    evaluate_target_health = false
  }
}

# Scaling. Checking High traffic every minute
resource "aws_cloudwatch_metric_alarm" "alb_request_count_alarm_high" {
  alarm_name          = "ALBRequestCountAlarmHigh"
  alarm_description   = "Scale out when request count is high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "RequestCount"
  namespace           = "AWS/ApplicationELB"
  period              = 60
  statistic           = "SampleCount"
  threshold           = 15

  dimensions = {
    LoadBalancer = aws_lb.application_load_balancer.load_balancer_full_name
  }

  alarm_actions = [aws_autoscaling_policy.scale_out_policy.arn]
}

# Scaling Policy for Scale Out
resource "aws_autoscaling_policy" "scale_out_policy" {
  name                   = "ScaleOutPolicy"
  scaling_adjustment    = 1
  adjustment_type       = "ChangeInCapacity"
  cooldown              = 60
  cooldown_action       = "Default"
  estimated_instance_warmup = 0
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 70
  }

  autoscaling_group_name = aws_autoscaling_group.auto_scaling_group.name
}

# Scaling. Checking Low traffic every minute
resource "aws_cloudwatch_metric_alarm" "alb_request_count_alarm_low" {
  alarm_name          = "ALBRequestCountAlarmLow"
  alarm_description   = "Scale in when request count is low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "RequestCount"
  namespace           = "AWS/ApplicationELB"
  period              = 60
  statistic           = "SampleCount"
  threshold           = 12

  dimensions = {
    LoadBalancer = aws_lb.application_load_balancer.load_balancer_full_name
  }

  alarm_actions = [aws_autoscaling_policy.scale_in_policy.arn]
}

# Scaling Policy for Scale In
resource "aws_autoscaling_policy" "scale_in_policy" {
  name                   = "ScaleInPolicy"
  scaling_adjustment    = -1
  adjustment_type       = "ChangeInCapacity"
  cooldown              = 60
  cooldown_action       = "Default"
  estimated_instance_warmup = 0
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 30
  }

  autoscaling_group_name = aws_autoscaling_group.auto_scaling_group.name
}

output "AccessKeyId" {
  description = "AWS Access Key ID"
  value       = aws_iam_access_key.iam_user_access_key.id
}

output "SecretAccessKey" {
  description = "AWS Secret Access Key"
  value       = aws_iam_access_key.iam_user_access_key.secret
}

output "DatabaseEndpoint" {
  description = "The Private IP of the MySQL database"
  value       = aws_instance.app_db.private_ip
}

output "DatabaseName" {
  description = "The Database Name"
  value       = var.DBName
}

output "DBUsername" {
  description = "The Database Username"
  value       = var.DBUsername
}

output "DBRootPassword" {
  description = "The Database Password for root"
  value       = var.DBRootPassword
}

output "DBPassword" {
  description = "The Database Password for user"
  value       = var.DBPassword
}

output "Region" {
  description = "AWS Region"
  value       = var.Region
}

output "VPC" {
  description = "Reference to the created VPC"
  value       = aws_vpc.vpc.id
}

output "PrivateSubnet1" {
  description = "Reference to the private subnet in the 1st AZ"
  value       = aws_subnet.private_subnet_1.id
}

output "PrivateSubnet2" {
  description = "Reference to the private subnet in the 2nd AZ"
  value       = aws_subnet.private_subnet_2.id
}

output "PublicSubnet1" {
  description = "Reference to the public subnet in the 1st AZ"
  value       = aws_subnet.public_subnet_1.id
}

output "PublicSubnet2" {
  description = "Reference to the public subnet in the 2nd AZ"
  value       = aws_subnet.public_subnet_2.id
}

output "WebSG" {
  description = "Reference to the Security Group allowing HTTP only inbound"
  value       = aws_security_group.web_sg.id
}

output "AppLaunchTemplate" {
  description = "Reference to the Launch Template"
  value       = aws_launch_template.app_launch_template.id
}

output "AutoScalingGroup" {
  description = "Reference to the Auto Scaling Group"
  value       = aws_autoscaling_group.auto_scaling_group.name
}

output "ApplicationLoadBalancer" {
  description = "Reference to the Application Load Balancer"
  value       = aws_lb.application_load_balancer.arn
}

output "LoadBalancerURL" {
  description = "URL of the Application Load Balancer"
  value       = aws_lb.application_load_balancer.dns_name
}
